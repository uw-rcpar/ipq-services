<?php
	$conn = new mysqli('localhost', 'root','NClUYZQ2s0zT', 'testcenter');
	ini_set('memory_limit', '2000M'); 
	$students = $conn->query('SELECT id FROM students')->fetch_all();
	$sessions = array();
	$starting_sid = 1;
	$stopping_sid = 9000;
		
	foreach($students as $student) {
			$correctQuestions = array();
			$incorrectQuestions = array();
			$ccount = array();
			$sid = intval($student[0]);
			if ($sid >= $starting_sid && $sid <= $stopping_sid) {
				$sessions[$sid] = $conn->query('select summary, id from quizlet_sessions where student_id=' . $sid)->fetch_all();
//var_dump(count($sessions[$sid]));
				$json = array();
				if (count($sessions[$sid]) > 0) {
					foreach($sessions[$sid] as $session) {
						$summary = $session[0];
						$session_id = $session[1];
//var_dump($summary);
						$summary = json_decode($summary, true);
						$breakdowns = $summary['session']['summary']['breakdown'];

						$average_time = "";
						if (isset($summary['session']['summary']['timeAverage'])) {
							$average_time = $summary['session']['summary']['timeAverage'];
						}
						else { var_dump('Missing time average'); }
						
						if (isset($summary['session']['answers'])) {
							$answers = $summary['session']['answers'];
							foreach($answers as $answer) {
								if (isset($answer['question_id'])) { 
								//var_dump($answer['type']);
									if ($answer['type'] == 'multiple-choice') {
										$result = $conn->query('select chapter_id as cid from multiple_choice_questions where id=' . $answer['question_id'])->fetch_assoc();
										$type = 'mcq';
									}
									else {
										$result = $conn->query('select chapter_id as cid from task_based_questions where id=' . $answer['question_id'])->fetch_assoc();
										$type = 'tbs';
									}
//var_dump($answer['type']);
									$qid = $answer['question_id'];
if (isset($answer['flagged'])) {
}
									if (!is_null($result['cid'])) {
										$chapter_id = $result['cid'];
										if (!isset($correctQuestions[$chapter_id])) {
											$correctQuestions[$chapter_id]['mcq'] = array();
											$incorrectQuestions[$chapter_id]['mcq'] = array();
											$notedQuestions[$chapter_id]['mcq'] = array();
											$flaggedQuestions[$chapter_id]['mcq'] = array();

											$correctQuestions[$chapter_id]['tbs'] = array();
											$incorrectQuestions[$chapter_id]['tbs'] = array();
											$notedQuestions[$chapter_id]['tbs'] = array();
											$flaggedQuestions[$chapter_id]['tbs'] = array();
										}
										
										if($type == "tbs"){
//											var_dump($answer);
										}
										
										$score = $answer['score'];
/*
										if(is_string($score)){
											$score = (int)($answer['stat'] === 'correct');
										}
*/
//										var_dump($score);
										$validScore = $type === 'tbs' ? 0.75 : 0;
										if ($score > $validScore) {
											$status = 'correct';	
											$loc = array_search( $answer['question_id'], $correctQuestions[$chapter_id][$type]);
											if ($loc === false) {
												array_push($correctQuestions[$chapter_id][$type], $answer['question_id']);
											}
										}
										else {
											$loc = array_search( $answer['question_id'], $incorrectQuestions[$chapter_id][$type]);
											$status = 'incorrect';
											if ($loc === false) {
												array_push($incorrectQuestions[$chapter_id][$type], $answer['question_id']);
											}
										}
		//var_dump($correctQuestions[$chapter_id][$type]);
		//var_dump($incorrectQuestions[$chapter_id][$type]);
		
										if (isset($answer['answers'])) {
											if (count($answer['answers']) > 0) {
												$answer_id = end($answer['answers']);
											}
											else {
												$status = 'skipped';
												$answer_id = null;
											}
										}
										else {
											$status = 'skipped';
											$answer_id = null;
										}
										if($type=="tbs"){
											echo "Status: ".$status." score: ".$score;
											
										}
										$session_name = $summary['session']['name'];
										if (!isset($answer['time'])) {
											$time = $average_time;
										}
										else {
											$time = $answer['time'];
										}
										$attempts = json_encode($answer['attempts']);
										$score = $answer['score'];
										$answer_type = $type;
										$quizlet_num = preg_replace('/\D/', '', $answer['quizlet']);

										if (!is_string($answer_id)) {
											$answer_id = json_encode($answer_id);
										}

										$stmt = $conn->prepare("INSERT INTO question_scores(id, session_id, student_id, quizlet_num, type, status, answer_id, score, chapter_id, attempts, time) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update status=?, answer_id=?, score=?, chapter_id=?, attempts=?, time=?");
										$result = $stmt->bind_param('iiiissssisssssiss', 
											$qid,
											$session_id,
											$sid,
											$quizlet_num,
											$answer_type,
											$status,
											$answer_id,
											$score,
											$chapter_id,
											$attempts,
											$time,
											$status,
											$answer_id,
											$score,
											$chapter_id,
											$attempts,
											$time
										);
										$result = $stmt->execute();

									}
								}
								if (!isset($answer['question_id'])) {
									var_dump('Answer with NULL question_id field');
								}
							}
						}
					} // end session loop
				} // end if
							
var_dump($sid);
		}
	}
	$conn->close();


?> 
