<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * A collection of helpers for Course
 */
 
 
 /**
 * log into the Drupal site
 */
 
function rest_connect($protocol, $domain, $service, $username, $password){
	
	
	//dev server locked down with basic authentication
	$request_url = server_request_url($protocol, $domain, $service, '/user/login');
	
	$user_data = array(
	'username' => $username,
	'password' => $password,
	);
	
	$user_data = http_build_query($user_data);
	
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json')); // Accept JSON response
	curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
	curl_setopt($curl, CURLOPT_POSTFIELDS, $user_data); // Set POST data
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	curl_setopt($curl, CURLOPT_COOKIESESSION, true);
	$response = curl_exec($curl);
	
	//return $response;
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ($http_code == 200) {
	$logged_user = json_decode($response);
		return  $logged_user ;
	}
	else {
		$http_message = curl_error($curl);
		$result = (object) ['message' => $http_message];
		return $result;
	}
	curl_close($curl);
}

/**
 * build the request url
 */
function server_request_url($protocol, $domain, $service, $path = ''){
  //$request_url = 'http://rcpar:CPAdev2013@'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/' . $path;
  //$request_url = 'http://localhost/dingo_drupal/' . $path;
  $request_url = $protocol.'rcpar:CPAdev2013@'.$domain.':'.$_SERVER['SERVER_PORT'] . '/' .$service.$path;
  return $request_url;
}

function get_session_data($login_user=NULL)
{

	$headers = apache_request_headers();
	
	
	
	//if login data is not in the headers check for cookies
	if (isset($headers['sessid'])){
		
		$session_data=array(
	
		'sessid'=>$headers['sessid'],
		'session_name'=>$headers['session_name'],
		'token'=>$headers['token'],	
	
	);
		
	} elseif (isset($login_user->session_name)) {
		
		$session_data=array(
	
		'sessid'=>$login_user->sessid,
		'session_name'=>$login_user->session_name,
		'token'=>$login_user->token,
	
		);
		
	} else {
		
	$session_data=array(
	
		'sessid'=>$_COOKIE['sessid'],
		'session_name'=>$_COOKIE['session_name'],
		'token'=>$_COOKIE['token'],	
	
		);
	
	
	
	
	}
	
	return $session_data;
}

/**
 * get the sections and chapters from drupal
 * 
 * @param requst_url is the full service url like http://dingo.rogercpareviewdev.com/course_services/cr
 */
function rest_get_data($request_url,$login_user = NULL){
	
	
	
	//get the session cookie, but pass the login object incase the cook is not set yet
	$session_data=get_session_data($login_user);
	
		
	// Define cookie session
	$cookie_session = $session_data['session_name'] . '=' . $session_data['sessid'];
	
	
	
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $session_data['token'])); // Accept JSON response
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// return json response
		//return $response;
 		// Convert json response as array
 		$node = json_decode($response,true);
		return $node;
		
	}
	else {
		// Get error msg
		
		$http_message = curl_error($curl);
		
		return $http_message;
	}
	
}

/**
 * post data to the drupal server
 * @param $request_type values: POST, PUT, DELETE
 */
function rest_post_data($request_url, $node_data, $request_type='POST') {
	
	//get the session from the cookie
	$session_data=get_session_data();
	
	$node_data = http_build_query($node_data);

	// Define cookie session
	$cookie_session = $session_data['session_name'] . '=' . $session_data['sessid'];
	
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $session_data['token'])); // Accept JSON response
	//determine the post, put or delete
	switch ($request_type) {
	  case 'POST':
	    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
	    break;
	  case 'PUT':
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
	    break;
	  case 'DELETE':
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
	    break;
	  }
		
	curl_setopt($curl, CURLOPT_POSTFIELDS, $node_data); // Set POST data
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// Convert json response as array
		$node = json_decode($response,true);
		return $node;
	}
	else {
		// Get error msg
		$http_message = curl_error($curl);
		return $http_message;
	}
} 
/**
 * Called by course_menu_get() to provide the proper format
 * @param $course: course data returned by drupal
 * @param $section array 'far aud reg bec'
 * @param $type string 'online' 'cram'
 */
function format_coruse_menu($courses, $section, $type='online') {
	
	//reformat the feed to suit online course angularJS needs
		foreach ($courses as $course ) {
			$chapt_no=1;
			//filter by section
			if (in_array($course['section'],$section)) {
			
			//for crams add suffix _cram
			if ($type=='online') {
				$sections['section']=$course['section'];
			} else {
				$sections['section']=$course['section'].'_cram';
				
			}
			
			$chapters=$course['chapters'];
			foreach ($chapters as $chapter) {
				$topic_no=1;
				$chapter_data['title']=$chapter['title'];
				$chapter_data['prefix']=$chapter['prefix'];
				
				$topics=$chapter['topics'];
				foreach ($topics as $topic) {
					//not included in drupal data feed
					//$topic['prefix']=$chapt_no.'.'.$topic_no;
					$prefix = $chapt_no.'.'.$topic_no;
					if($topic_no < 10) {
						$prefix = $chapt_no.'.0'.$topic_no;
					}
					$chapter_data['topics'][]= array( 
													'id'=>$topic['id'],
													'topic'=>$topic['title'],
													'percentviewed'=>$topic['percentviewed'],
													'prefix'=> $prefix
													);
					$topic_no++;
				}
				$topic_no=1;
				
				$sections['chapters'][]=$chapter_data;
				$chapter_data=array();
				$chapt_no++;
				
				
			}
			
			$menu[]=$sections;
			$sections=array();
		
		 	
			} 
		
		}
	return $menu;
}
/**
 * parse the entitlements from login data
 * @param login object from drupal login service 
 */
function get_user_entitlements($login){
	
	//pull out the entitlements
	$courses=array();
	$sections=array('FAR','AUD','REG','BEC');
	//if not entitlements passed return empty
	if (!isset($login->user->user_entitlements->products)) {

		return $courses;		
		
	}
	$entitlements=$login->user->user_entitlements->products;
	
	//var_dump($entitlements);
	//exit;
	
		foreach ($entitlements as $key => $value) {
			
			
			if(in_array($key, $sections)) {
			
			$course['section']=$key;
			
			$section_data=$value;
			
			foreach ($section_data as $section_key => $section_value) {
				
				
				
				if ($section_key=='course_type') {
					$course['course_type']=$section_value;
				}
				
			}
			
			
			
			$courses[]=$course;
			$course=array();
		}
	
	
	}
	
	return $courses;
}


