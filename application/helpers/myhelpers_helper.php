<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * A collection of helpers for restserver
 */
 
 
 /**
 * called by chapters_get, returns number of questions based topic_id
 */
function get_no_mcq($no_multiple_choice, $topic_id)
{
  $count=0;
  $id_array=array();
  
  foreach ($no_multiple_choice as $mc) {
  	
	if ($mc['topic_id'] == $topic_id &&  !array_search($mc['topic_id'], $id_array)) {
		$count=$count+$mc['records'];
		$id_array[]=$mc['topic_id'];
	//if the topic_id is a list
	} elseif (strpos($mc['topic_id'], ',')!=FALSE) {
		$ids = explode(',', $mc['topic_id']);
		foreach ($ids as $id) {
			if ($id == $topic_id && !array_search($mc['topic_id'], $id_array)) {
			$count=$count+$mc['records'];
			$id_array[]=$mc['topic_id'];
			}
			
		}
	}
	  
  }
  
  return $count;
}

//get the word doc content called by manage_docs/search
function read_file_docx($filename){

        $striped_content = '';
        $content = '';

        if(!$filename || !file_exists($filename)) return false;

        $zip = zip_open($filename);

        if (!$zip || is_numeric($zip)) return false;


        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        //echo $content;
        //echo "<hr>";
        //file_put_contents('1.xml', $content);     

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);

        return $striped_content;
    }

function get_document_directory($doctype)
	{
		switch ($doctype)
		{
		case "mc":
		  $dir='/testmodule-admin/docs';
		  return $dir;
		case "tbs-journal":
		  $dir='/testmodule-admin/docs-widget-tbs2';
		  return $dir;
		case "tbs-wc":
		  $dir='/testmodule-admin/docs-wc';
		  return $dir;
		default:
		  $dir='not found';
		  return $dir;
		}


}
	
//check if a student is authorized to see a particular video		
	function is_authorized($student, $topic_id)
	{
		
		$access = json_decode($student[0]['access']);
		
		$access = (array) $access;
		
		$now = new DateTime();
		
		foreach ($access['TOPICS'] as $id => $exp) {
			
			//remove commas
		   $exp = str_replace(',','',$exp);
		   $exp_date = new DateTime($exp);
		   	
			//check the topicid and exp date
		   if ($id == $topic_id && $exp_date > $now) {
		   	
			   return array('access'=>TRUE);
		   }
		}
		return array('access'=>FALSE);
		
	}
	
	/*
	"6":"far course active",
	"7":"aud course active",
	"8":"reg course active",
	"9":"bec course active"
	 */
	function get_sections($student)
	{
			
		$roles=array();
		$access = json_decode($student[0]['access'], TRUE);
		
		//var_dump($access['USER']['ROLES']);
		//exit;
		
		foreach ($access['USER']['ROLES'] as $section) {
			
			
			if ($section == "far course active" && !in_array('far', $roles)) {
				$roles[]='far';
			}
			
			if ($section == "aud course active" && !in_array('aud', $roles)) {
				$roles[]='aud';
			}
			if ($section == "reg course active" && !in_array('reg', $roles)) {
				$roles[]='reg';
			}
			if ($section == "bec course active" && !in_array('bec', $roles)) {
				$roles[]='bec';
			}						
				
			
		}
		
		return $roles;		
		
	}
	
	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}
	//get student id from json cookie
	function get_sid()
	{
		$sid=0;
		if (isset($_COOKIE['data'])) {
			//student id and name are in a json string inside a cookie
			$cookie_obj=json_decode($_COOKIE['data']);
			$cookie_data = (array) $cookie_obj;
			$sid=$cookie_data['studentID'];
		}
		return $sid;
	}

//get CF student id from json cookie
	function get_cf_student_id()
	{
		$cf_student_id=0;
		if (isset($_COOKIE['data'])) {
			//student id and name are in a json string inside a cookie
			$cookie_obj=json_decode($_COOKIE['data']);
			$cookie_data = (array) $cookie_obj;
			$cf_student_id=$cookie_data['cf_student_id'];
		}
		return $cf_student_id;
	}

function get_course_url($drupal_topic_id)
{
	$course_url = 'https://www.rogercpareview.com/courseware-reference?topicID='.$drupal_topic_id;
	
	return $course_url;
}
//get the quesiton ids out of the summary json field from the saved sessions
function get_saved_question_ids($saved_sessions)
{
	$ids=array();
	foreach ($saved_sessions as $saved_session) {
		
		$summary=json_decode($saved_session['summary'], true);
		
		$answered_questions = $summary['session']['answers'];
		
		foreach ($answered_questions as $answered_question) {
			
					
			if ($answered_question['type']=='multiple-choice') {
				
				$ids['mcq'][]=$answered_question['question_id'];
				
			} else {
				
				$ids['tbs'][]=$answered_question['question_id'];
			}
		}	
		
	}
	
	return $ids;
}

function encode_tbs_search_string($search)
{
	$search=rawurlencode($search);
	$search=str_replace('%','|%',$search);
	return $search;
}
//multi dimentional array search
function search_for_qid($qid, $type, $array) {
   foreach ($array as $key => $val) {
       if ($val['question_id'] === $qid && $val['type']==$type) {
           return $key;
       }
   }
   return FALSE;
}
//return the number of research_unanswered to get the number of research questions unanswered
function is_research($question_id, $research_questions) {
    
    $is_research = (in_array($question_id, $research_questions)) ? TRUE : FALSE ;
    return $is_research;
   
}

function is_journal($question_id, $journal_questions) {
    
    $is_journal = (in_array($question_id, $journal_questions)) ? TRUE : FALSE ;
    return $is_journal;
   
}
function array_delete($array, $element) {
    if(($key = array_search($element, $array)) !== false) {
    unset($array[$key]);
    return $array;
}
}


