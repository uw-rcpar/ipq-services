<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
//header('Content-Type: text/event-stream');
// recommended to prevent caching of event data.
//header('Cache-Control: no-cache'); 

//create the array to insert into the saved_quizzes table
function create_saved_session_array($session, $student_id, $quiz_date) {

    $data = array('student_id' => $student_id, 'session_name' => $session['session']['name'], 'session_type' => 'quiz', 'average_time' => $session['session']['summary']['timeAverage'], 'section' => $session['session']['section'], 'total_percent' => $session['session']['summary']['totalPercent'], 'current_percent' => $session['session']['summary']['currentPercent'], 'attempts_average' => $session['session']['summary']['attemptsAverage'], 'complete_percent' => $session['session']['summary']['completePercent'], 'timestamp' => $quiz_date, 'active' => 1, );

    return $data;
}

//create the array to insert into the question_scores table
function create_quesion_scores_array($session, $session_id, $student_id) {

    $answers = $session['session']['answers'];
    $quizlets = $session['session']['quizlets'];
    $breakdown = $session['session']['summary']['breakdown'];

    $question_data = array();

    foreach ($answers as $answer) {

        if ($answer['type'] == "multiple-choice" || $answer['type'] == "mcq") {
            $type = 'mcq';
        } else {
            $type = 'tbs';
        }

       
        $noted = '[]';
        $flagged = 'false';

        //get flagged and notes
        $length = count($quizlets);
        for ($i = 0; $i < $length; $i++) {
           foreach ($quizlets[$i] as $quizlet) {
            if ($quizlet['id'] == $answer['question_id']) {
                $noted = (empty($quizlet['notes'])) ? '[]' : json_encode($quizlet['notes']);
                $flagged = $quizlet['isFlagged'];
                }
            }
        }
        

        //get the super fucking burried chapter id
        $chapter_id = 0;
        $attempts = array();
        foreach ($breakdown as $bdata) {
            $bquestions = $bdata['questions'];
            foreach ($bquestions as $bquestion) {
                if ($bquestion['question_id'] == $answer['question_id']) {
                    $chapter_id = $bdata['chapter_id'];
                   

                }
            }

        }

        
        $time = json_encode($answer['attempts']);
        
        $attempts = implode(',', $answer['attempts']);
        $attempts = "[$attempts]";
        
        
        
        //get the answer id. Look for an answer_id that is not 0. The last answer_id is the most recent
        if ($type=='mcq'){
            $answer_string=0;
            $my_answers=$answer['answers'];
            
              foreach ($my_answers as $my_answer) {
                if ($my_answer != 0 && !is_array($my_answer)) {
                    $answer_string=$my_answer;
                    
                    }  
            
            
            }
              
                  //status is either correct, incorrect or skipped
                 if ($answer['score'] == 1 && $answer_string != 0) {
                     
                     $status = 'correct';
                     
                 } elseif ($answer['score'] == 0 && $answer_string == 0) {
                     
                      $status = 'skipped';
                 } elseif ($answer['score'] == 0 && $answer_string != 0) {
                     
                      $status = 'incorrect';
                 }
            
            
        } else {
            
            //TBS questions have different format
            $answer_string=json_encode($answer['answers']);
            
             //status is either correct, incorrect or skipped
             if ($answer['score'] == 1) {
                 
                 $status = 'correct';
                 
             } elseif ($answer['score'] != 1 && $attempts == "[]") {
                 
                  $status = 'skipped';
             } elseif ($answer['score'] != 1 && $attempts != "[]") {
                 
                  $status = 'incorrect';
             }
        }
        
         
        
         
        
        
        $data = array(
        //id is the question id
            'id' => $answer['question_id'], 
            'session_id' => $session_id, 
            'student_id' => $student_id, 
            'quizlet_num' => 0, 
            'type' => $type, 
            'status' => $status, 
            'noted' => $noted, 
            'flagged' => $flagged, 
            'answer_id' => $answer_string, 
            'score' => $answer['score'], 
            'chapter_id' => $chapter_id, 
            'attempts' => $attempts, 
            'time' => $time, );

        $question_data[] = $data;

    }

    return $question_data;
}

function send_message($id, $message, $progress) {
    $d = array('message' => $message , 'progress' => $progress);
      
    echo "id: $id" . PHP_EOL;
    echo "data: " . json_encode($d) . PHP_EOL;
    echo PHP_EOL;
      
    ob_flush();
    flush();
}
