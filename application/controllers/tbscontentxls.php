<?php

class Tbscontentxls extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function spreadsheet() {
		gc_collect_cycles();
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		//ini_set('display_startup_errors', TRUE);

		$this->load->library('excel');
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
		//$this->load->library('PHPExcel/iofactory');

		$this->load->model('testcenter_model');
		$data = $this->testcenter_model->get_all_tbs();
		$objPHPExcel = new PHPExcel();

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="IPQ TBS Questions.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$linkStyle = array(
		  'font' => array(
		    'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
		    'color' => array('rgb' => '0000CF')
		  )
		);

		$objPHPExcel->setActiveSheetIndex(0);
//		$objPHPExcel->getActiveSheet()->setCellValue('A1', $question['id']);
		$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray(array('font'=>array(
																					'bold' => true
																				)
																			));
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
		
		//$objPHPExcel->getActiveSheet()->setCellValue('A2', "Topic");
		
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "Question ID"); //$objPHPExcel->getActiveSheet()->setCellValue('B2', "Question URL");
		$objPHPExcel->getActiveSheet()->setCellValue('B2', "Question");
		$objPHPExcel->getActiveSheet()->setCellValue('C2', "Edit URL");
		$objPHPExcel->getActiveSheet()->setCellValue('D2', "Tagged Topics");
		$objPHPExcel->getActiveSheet()->setCellValue('E2', "Created");
		$objPHPExcel->getActiveSheet()->setCellValue('F2', "Edited");

		$i = 3;
		foreach($data as $question) {
							//var_dump($question);
							
							$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $question['id']);
							$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $question['type']);
							$linkUrl = $this->testcenter_model->get_edit_url($question);
							$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, 'http://testcenter.rogercpareview.com/' . $linkUrl);
							$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray($linkStyle);
							if (strlen($question['topic_id']) > 0) {
								$topicsList = $this->testcenter_model->get_topics_ids($question['topic_id']);

								$topics = '';
								foreach($topicsList as $topic) {
									$topics = $topics . $topic['chapter'] . ' - ' . $topic['topic'] ."\n";
								}

								$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $topics);
								$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->getAlignment()->setWrapText(true);
							}
							else {
								$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, 'No tagged topics.');
							}

							$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $question['createdBy'] . " on " . $question['createdOn']);
							if($question['updatedBy'] != '') {
								$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $question['updatedBy'] . " on " . $question['updatedOn']);
							} else {
								$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, 'n/a');
							}
		    					$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(50);
							$i++;	
							//gc_collect_cycles();
		}

		foreach(range('A','S') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		$objPHPExcel->getActiveSheet()->calculateColumnWidths();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

		

		//var_dump($data);
		exit;
	}
}
