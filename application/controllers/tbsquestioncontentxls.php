<?php

class Tbsquestioncontentxls extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function spreadsheet() {
		gc_collect_cycles();
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		//ini_set('display_startup_errors', TRUE);

		$this->load->library('excel');
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
		//$this->load->library('PHPExcel/iofactory');

		$this->load->model('testcenter_model');
		$data = $this->testcenter_model->get_all_tbs();

		$i = 0;
		foreach($data as $tbs) {
			$entry = json_decode($tbs['question_json'], true);
			if (!isset(json_decode(urldecode($entry['question']), true)['question']['widgets'])) {
				$question = json_decode(urldecode($entry['question']), true)['question'];
			}
			else {
				$question = json_decode(urldecode($entry['question']), true)['question']['widgets'][0]['model']['content'];
			}
			if (!isset(json_decode(urldecode($entry['question']), true)['question']['solution'])) {
				$answers = "Solution not defined";
			}
			else {
				$answers =  json_decode(urldecode($entry['question']), true)['question']['solution'];
			}

			if (is_array($question)) {
				$question = json_decode(urldecode($entry['question']), true)['question']['question'];
			}

			$path = 'sphider/admin/tbs-question-inventory/' . strtolower($tbs['section']) . '/chapter-' . strval(intval($tbs['chapter_id'])+1) . '/' . strtolower($tbs['type']);

		        if (!is_dir($path)) {
		                mkdir($path, 0777, true);
		        }

		        $fp = fopen($path . "/question-" . $tbs['id'] . ".doc","wb");

			if (!$fp) {
				echo "Could not create doc file";
			}

        		$content = "<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\"><body>Question #" . $tbs['id'] . "<br /><br />"  . $question . "<br /><br />" . $answers . "</body></html>";

		        fwrite($fp,$content);
		        fclose($fp);
			$i++;
		}
		exec('cd /opt/bitnami/apache2/htdocs/restserver/ && zip -r tbs-question-inventory.zip tbs-question-inventory');
	
		if (!file_exists('/opt/bitnami/apache2/htdocs/restserver/sphider/admin/tbs-question-inventory.zip')) {
			echo "Zip file does not exist";
		}
		else {
			header("Content-type: application/zip"); 
			header("Content-Disposition: attachment; filename=tbs-question-inventory.zip");
			header("Pragma: no-cache"); 
			header("Expires: 0"); 
			readfile("/opt/bitnami/apache2/htdocs/restserver/sphider/admin/tbs-question-inventory.zip");
		}
		exit();
	}
}
