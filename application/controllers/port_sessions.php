<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Port_sessions extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        
        //header('Content-Type: text/event-stream');
        // recommended to prevent caching of event data.
       // header('Cache-Control: no-cache'); 
        
        $quiz_limit=500;
        $this -> load -> model('port_sessions_model');
        $this -> load -> helper('port_sessions');
        $start_id=$this->input->get('start_id');
        $processed=$this->input->get('processed');
        $quiz_count=$this -> port_sessions_model -> get_quiz_count();
        
        $quizzes = $this -> port_sessions_model -> get_quizzes($start_id,$quiz_limit);
        
        $i=1;
        foreach ($quizzes as $quiz) {

            //session hold the bulk of the data
            $session = json_decode($quiz['summary'], TRUE);

            //format for insert into saved_quizzes
            $data = create_saved_session_array($session, $quiz['student_id'], $quiz['timestamp']);
            //no data than nevermind
            if (empty($data) || empty($data['session_name'])) {
                continue;
            }
			//if not even attempted forget it
			if ($data['current_percent']==0 && $data['complete_percent']==0) {
                continue;
            }
            //insert into saved_quizzes
            $session_id = $this -> port_sessions_model -> set_saved_quizzes($data);
            //create a question_scores array
            $question_data = create_quesion_scores_array($session, $session_id, $quiz['student_id']);
            //ensure that $question_data is all valid
            
           // var_dump($question_data);
           
            //data looks good, let's insert
            if (!empty($question_data)) {
                $result = $session_id = $this -> port_sessions_model -> set_question_scores($question_data);
                if ($result['success'] == 0) {
                   // echo "fail on " . $session_id . "<br>";
                }
                
            //time consumer:
            foreach ($question_data as $qdata) {
                //check for the latest answer
                 $section_overview = $this -> port_sessions_model -> get_section_overview($qdata);
                 //nothing there than insert
                  $qdata['question_id']=$qdata['id'];
                     $tmp=array(
                            'question_id' => $qdata['id'], 
                            'session_id' => $qdata['session_id'],
                            'student_id' => $qdata['student_id'],
                            'type' => $qdata['type'], 
                            'status' => $qdata['status'], 
                            'chapter_id' => $qdata['chapter_id'], 
                            'timestamp' => $quiz['timestamp'],
                     );
                 if (empty($section_overview)){
                    
                     $id = $this -> port_sessions_model -> set_section_overview($tmp);
                 } else {
                     //update
                     $id = $this -> port_sessions_model -> update_section_overview($tmp);
                     //echo "updated <br>";
                 }
                
            }    
                
            }
            //increment to the last start_id
            $start_id=$quiz['id'];
            //$progress=($i/$quiz_limit) * 100;
            //send_message($start_id, 'is complete ' . $i . ' of 50' , $progress); 
            $i++;
        }
        
        //wrap it up if we're done
        if ($processed >= $quiz_count[0]['total']){
            echo "$processed records processed. That's all folks!";
            exit;
        }

        $processed=$processed+$quiz_limit;
        $percentage_complete=($processed/$quiz_count[0]['total']) * 100;
        //pass data to view
        $view_data=array(
            'total_records'=>$quiz_count[0]['total'],
            'start_id'=>$start_id,
            'processed'=>$processed,
            'percentage_complete'=>$percentage_complete,
            
        );
        $this->load->view('port_sessions_view', $view_data);
        //send_message('CLOSE', 'Process', 'complete');
    }

}
