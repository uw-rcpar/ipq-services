<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * manage_docs
 *
 * Main controler for doc management for data entry
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Steve
 * @link		http://stevewarelabs.com/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Manage_docs extends REST_Controller
{
	
	/**
	 * secure this beeotch with cookies set in auth scripts
	 * data cookie is set for /testmodule
	 * username cookie is set in /testmodule-admin
	 */
	
	public function __construct()
    {
        parent::__construct();

        if(!isset($_COOKIE['username']) && !isset($_COOKIE['data'])){
            //return FailedResponse();
            $this->response(array('error' => 'authentication required'), 403);
            exit();
        }
	}
	
	
	//get the documents
	//url http://localhost/restserver/index.php/api/manage_docs/getdocs/format/json
	public function getdocs_get()
    {
		
		
		//get the doc type. Default is "mc"
		$doc_type = (!$this->get('type')) ? 'mc' : $this->get('type');
		    			
		//current dir
		$currentdir=getcwd();
		//parent dir
		$parentdir=dirname($currentdir);
		
		//doc dir
		$this->load->helper('myhelpers');
		$doc_directory=get_document_directory($doc_type);
		
		//error on $doc_type
		if ($doc_directory=='not found') {
			$this->response(array('error' => 'document type not found!'), 404);
		}
		
		//get the entire directory for searching		
		$map = $parentdir . $doc_directory;
		
		
		$result=array('success'=>1);
		
		// get all the file names
		$docs = scandir($map);
		
		if (!$docs) {
			$this->response(array('error' => 'Couldn\'t find any docs!'), 404);
		}
		
		$this->load->model('testcenter_model');
		
		//foreach ($docs as $doc) {
		foreach (glob($map."/*.docx") as $doc) {		
			
			//remove crap
			if (strlen($doc)>3) {
				
			$title = str_replace($map.'/', '', $doc);
			$url = str_replace($parentdir, '', $doc);
			
			$mydoc = $this->testcenter_model->get_doc($title, $doc_type);
			
			//if the file is not in the db, put it there
			if (!$mydoc) {
				
				
				
				$data=array(
				
					'title'=>$title,
					//'url'=>$doc_directory.'/'.$doc,
					'url'=>$url,
					'type'=>$doc_type
				
				);
				//insert the doc
				$id = $this->testcenter_model->set_doc($data);
			}
			
			}//end strlen condition
		}//end loop
		
		$result = $this->testcenter_model->get_docs($doc_type);	
		
		
		$documents=array('documents'=>$result);
		
		$this->response($documents, 200);
	}

	public function checkout_get()
    {
    	
		//no document id, forget it
		if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		$id=$this->get('id');
		
		$this->load->model('testcenter_model');		
		
		$data=array(		
			
			'checkedOutBy'=>$_COOKIE['username'],
			'checkedOutAt'=>date("Y-m-d H:i:s")			
		
		);
		$result = $this->testcenter_model->checkout_doc($data,$id);
		
		//update the student table with the docID
		$data=array('docID'=>$id);
		$student_id=$_COOKIE['studentid'];
		$result = $this->testcenter_model->update_student_info($data,$student_id);
		
		$this->response($result, 200);
    }
	
	public function checkin_get()
    {
			
    	//no document id, forget it
		if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		$id=$this->get('id');
		
		$this->load->model('testcenter_model');		
		
		$data=array(		
			
			
			'checkedInAt'=>date("Y-m-d H:i:s")			
		
		);
		$result = $this->testcenter_model->checkout_doc($data,$id);
		
		//set the docID back to 0
		$data=array('docID'=>0);
		$student_id=$_COOKIE['studentid'];
		$result = $this->testcenter_model->update_student_info($data,$student_id);
		
		$this->response($result, 200);
    }
	
	public function flag_document_post()
	{
		$this->input->post(); // returns all POST items without XSS filter 	
		
		if (!$this->post('id')){
			
			$this->response(NULL, 400);
		}
		
		$question=$this->post();
		
		$id=$question['id'];
		
		$data=array(
		
			'id'=>$question['id'],
			'reportText'=>$question['report']['reportText'],
			'isFlagged'=>1
		
		);
		
		$this->load->model('testcenter_model');
		$result = $this->testcenter_model->flag_document($data,$id);
		
		$this->response($result, 200);
	}
	
	//unflag the doc
	public function unflag_document_get()
	{
		$this->input->get(); // returns all POST items without XSS filter 	
		
		if (!$this->get('id')){
			
			$this->response(NULL, 400);
		}
		
		$question=$this->get();
		
		$id=$question['id'];
		
		$data=array(
		
			'id'=>$question['id'],
			'isFlagged'=>0
		
		);
		
		$this->load->model('testcenter_model');
		$result = $this->testcenter_model->flag_document($data,$id);
		
		$this->response($result, 200);
	}
	
	//search docs for strings
	public function search_docs_get()
	{
			
		//no search string forget it
		if(!$this->get('search'))
        {
        	$this->response(NULL, 400);
        }
		
		$this->load->helper('myhelpers');
		$this->load->model('testcenter_model');
		
		//the string to search for
		$search=$this->get('search');
		$search = strip_tags($search);
		
		//get the doc type. Default is "mc"
		$doc_type = (!$this->get('type')) ? 'mc' : $this->get('type');
		    			
		//current dir
		$currentdir=getcwd();
		//parent dir
		$parentdir=dirname($currentdir);
		
		//doc dir
		$doc_directory=get_document_directory($doc_type);
		
		//error on $doc_type
		if ($doc_directory=='not found') {
			$this->response(array('error' => 'document type not found!'), 404);
		}
		
		//get the entire directory for searching		
		$map = $parentdir . $doc_directory;
		
		$matches=array('documents'=>'');
		
		//read each document file
		foreach (glob($map."/*.docx") as $doc) {
		    $contents = read_file_docx($doc);
			
			var_dump($contents);
			exit;
			
			//if the string is found, add to the matches
		    if (!strpos($contents, $search)) continue;
			//get the starting position of the string, return 30 characters on either side
			$start_pos=strpos($contents, $search) - 90;
			
			//end position. since we are starting 30 characters before the start, we need to end 60 characters after
			$length=strlen($search) + 180;
			$snippet=substr($contents,$start_pos,$length);
			
			//snippet might contain nasty characters. Remove if necessary
			if (!preg_match("//u", $snippet)) {
			       $snippet='Error - Invalid UTF-8 sequence. You have angered the json gods.';
			  }
			
			
			//remove the directory to get the title of the doc
			$dir=$map.'/';
			$title=str_replace($dir, '', $doc);
			
			$match = $this->testcenter_model->get_doc($title, $doc_type);
			
			foreach ($match as $mymatch) {
				
				$matches['documents'][]=array(
				
					'id'=>$mymatch['id'],
					'title'=>$mymatch['title'],
					'url'=>$mymatch['url'],
					'checkedOutBy'=>$mymatch['checkedOutBy'],
					'checkedInAt'=>$mymatch['checkedInAt'],
					'checkedOutAt'=>$mymatch['checkedOutAt'],
					'isFlagged'=>$mymatch['isFlagged'],
					'reportText'=>$mymatch['reportText'],
					'snippet'=>$snippet
				
				
				);
				
			}
			
		    
		}
		
		$this->response($matches, 200);
		
	}

		
	// returns all flagged documents
	public function flagged_docs_get()
    {
			
		$this->load->model('testcenter_model');
    	
		$result = $this->testcenter_model->get_flagged_docs();
		
		$this->response($result, 200);
    }
	
	
	
	
}
	