<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Course
 *
 * Helper functions that pull or write data to/from Drupal services
 * 
 * UPDATE AND POST DRUPAL URLS
 * course_services/bm - bookmark
 * course_services/hl - book highlight
 * course_services/vh - video history (where they are in the video)
 * course_services/tn - topic notes
 *
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Course extends REST_Controller
{
		
	//setup some application variables	
	private $Domain;
	private $Username;
	private $Password;
	private $Service;
	private $Protocol;
	private $Short_name;
	
	
	public function __construct()
    {
        parent::__construct();
		//adding http methods for Sam
		$this->allowed_http_methods = array('get', 'delete', 'post', 'put', 'options');
		
		$method = $_SERVER['REQUEST_METHOD'];
   		if($method == "OPTIONS") {
        	$this->response('OK', 200);
            exit();
	    }
		
		/**
		 * Setup defaults to build a url structure like http://dingo.rogercpareviewdev.com/course_services/cr
		 */
		
		$this->Protocol = 'http://';
		$this->Domain = 'dingo.rogercpareviewdev.com';
		$this->Username = 'taylor.steven.v@gmail.com';
		$this->Password = 'rlouisa01';
        $this->Service = 'course_services';
		//shortname is the name of the resource set in Drupal
		$this->Short_name = '/cr';
		
		//load up the course_helper functions
		$this->load->helper('course');
		//load up the cookie helper
		$this->load->helper('cookie');
		//setup the url helper
		$this->load->helper('url');
		
		//if no login in the url and no sessid cookie, bootosis for you beeootch!
		$uri=uri_string();
		$headers =  apache_request_headers();
		
		
		
		if (strpos($uri,'login') == false &&  !isset($headers['sessid']) && !isset($_COOKIE['sessid']) ){
			
		   $this->response(array('Nope nope nope'), 403);
            exit();
		} 
		
	}
		
		
	
	/**
	 * log into the Drupal server to get the user data
	 */
	public function login_get()
	{
		//if no password is passed just login with Steve's account for testing
		//!!!!!REMOVE IN PRODUCTION!!!!!
		
		
		if(!$this->get('password'))
        {
        	$password=$this->Password;
			$username=$this->Username;
        
		}else {
			
			//user the creds passed in the url
			$password=$this->get('password');
			$username=$this->get('username');    
        	
        }
		
		
		//connect to Drupal
		$login_user = rest_connect($this->Protocol, $this->Domain, $this->Service, $username, $password);
		
		
		
		//no session, heave-ho time
		if (!isset($login_user->sessid)) {
			$this->response(array('error' => 'Unauthorized'), 401);
		}
		
			
		//store the session in a cookie
		$sessid = array(
			'name'=>'sessid',
			'value'=>$login_user->sessid,
			'expire' => '86500',
		);
		
		$session_name = array(
			'name'=>'session_name',
			'value'=>$login_user->session_name,
			'expire' => '86500',
		);
		
		$token = array(
			'name'=>'token',
			'value'=>$login_user->token,
			'expire' => '86500',
		);
		
		$name = array(
			'name'=>'name',
			'value'=>$login_user->user->name,
			'expire' => '86500',
		);
		
		$uid = array(
			'name'=>'uid',
			'value'=>$login_user->user->uid,
			'expire' => '86500',
		
		);
		
		
		$this->input->set_cookie($sessid);
		$this->input->set_cookie($session_name);
		$this->input->set_cookie($token);
		$this->input->set_cookie($name);
		$this->input->set_cookie($uid);
		
		//setup a filter for login data only
		if($this->get('filter'))
        {
        	//send data back
			$this->response($login_user, 200);
		} 
		
		//get the entitlement data in an array
		$entitlements=get_user_entitlements($login_user);
		
		
		//We'll do some entitlement magic here, but for now pass all 4 parts of both online and cram
		$online_sections=array('AUD','REG','BEC','FAR');
		$cram_sections=array('AUD','REG','BEC','FAR');
		
		
		//get the menu data starting here
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $this->Short_name);
		
		
		$sections=array();
		
		//call the service
		$results=rest_get_data($request_url,$login_user);
		
		$courses=$results['content'];
		
		//get cram
		$path = '/cr?filter=cram';
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_get_data($request_url,$login_user);
		
		$cram_courses=$results['content'];
		
		//call helper function
		$menu['content']=array();
		$online_menu = format_coruse_menu($courses, $online_sections, 'online');	
		$cram_menu = format_coruse_menu($cram_courses, $cram_sections, 'cram');
		$menu['content']=$online_menu;
		
		
		
		$menu['contents']=$cram_menu;
		$menu['menu']=array_merge($menu['content'],$menu['contents']);
		
		unset($menu['contents']);
		unset($menu['content']);
		
		$menu['login']=$login_user;
		
		
		
		//send data back
		$this->response($menu, 200); // 200 being the HTTP response code
		
	}
	
	/**
	 * Get the course menu as per model at 
	 * https://rcpa-projects.rogercpareview.com/projects/courseware-student/wiki/Course_navigation_model
	 * @param section is the exam section "AUD" "FAR" "BEC" "REG". if not passed returns everything
	 */
	public function course_menu_get()
	{
			
		$exam_section='ALL';
		
		//
		if($this->get('section'))
        {
        	$exam_section=$this->get('section');		
		}
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $this->Short_name);	
		
		$sections=array();
		
		//call the service
		$results=rest_get_data($request_url);
		
		$courses=$results['content'];
		
		//get cram
		$path = '/cr?filter=cram';
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_get_data($request_url);
		
		$cram_courses=$results['content'];
		//var_dump($cram_courses);
		//exit;
		
		$online_sections=array('AUD','REG','BEC','FAR');
		$cram_sections=array('AUD','REG','BEC','FAR');
		
		//call helper function
		$menu['content']=array();
		$online_menu = format_coruse_menu($courses, $online_sections, 'online');	
		$cram_menu = format_coruse_menu($cram_courses, $cram_sections, 'cram');
		$menu['content']=$online_menu;
		
		
		
		$menu['contents']=$cram_menu;
		$menu['content']=array_merge($menu['content'],$menu['contents']);
		
		unset($menu['contents']);
		$this->response($menu, 200);
	}
    
    
	/**
	 * Get the topic info 
	 */
    public function topic_contents_get()
	{
		//id required
		if(!$this->get('id'))
        {
        	$this->response(array('error' => 'No topic id passed.'), 400);
		} else {
			$id=$this->get('id');
		}
		
		$path='/tr/'.$id;
		$key='topic_'.$id;
		$video_tracks=array();
		$notes=array();
		$videobookmarks=array();
		$timestamps=array();
		$video_ids=array();
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		
		//call the service
		$results=rest_get_data($request_url);
		
			
		//if not an array return error
		if (!is_array($results)) {
			 $this->response(array('error' => $results), 404);
		}
		
		
		//format per requirements https://rcpa-projects.rogercpareview.com/projects/courseware-student/wiki/Topic_contents_model
		//drupal outputs "topic_1234" where 1234 is the id
		$topic['topic']=$results[$key];
		
		//page data is further nested
		$pages=array(
			'url'=>$topic['topic']['page_url'],
			'type'=>$topic['topic']['page_type'],
			'range'=>$topic['topic']['page_range'],
		);
		
		
		
		//setup top level topic
		$topic_top_level=array(
			'prefix'=> 'XXX YY.Z',
			'title' => $topic['topic']['title'],
			'id'=> $topic['topic']['id'],
			'previous'=>$topic['topic']['previous'],
			'next'=>$topic['topic']['next'],
			
		);
		
		
		//reformat video tracks
		$tracks=$topic['topic']['content']['video_tracks'];
		
			
		//for now, just english and japanese
		foreach ($tracks as $track) {
			
			//english	
			if (isset($track['vtt_en'])){
				$video_tracks[] = array(
					"language"=>"EN", 
					"label"=>"English", 
					"src"=>$track['vtt_en'],
				);
			}
			
			//japanese
			if (isset($track['vtt_jp'])){
				$video_tracks[] = array(
					"language"=>"JP", 
					"label"=>"Japenese", 
					"src"=>$track['vtt_jp'],
				);
			}
			
		}
		
		
		//put the tracks and other variables into the url array
		$urls=array();
		foreach ($topic['topic']['content']['videos'] as $key=>$value) {
			//$video['tracks']=$video_tracks;
			$topic['topic']['content']['videos'][0]['tracks']=$video_tracks;				

			if (isset($value['video_low_resolution'])) {
					
				$urls[]=array(				
					'bitrate'=>'low',
					'url'=>$value['video_low_resolution'],				
				);
			}

			if (isset($value['video_medium_resolution'])) {
					
				$urls[]=array(				
					'bitrate'=>'medium',
					'url'=>$value['video_medium_resolution'],				
				);
			}
			
			if (isset($value['video_high_resolution'])) {
					
				$urls[]=array(				
					'bitrate'=>'high',
					'url'=>$value['video_high_resolution'],				
				);
			}
			
			if (isset($value['video_duration'])) {
				$topic['topic']['content']['videos'][0]['length']=$value['video_duration'];
			}		
			
		}
		
		//set the urls
		$topic['topic']['content']['videos'][0]['urls']=$urls;
		
		//clean up the video array. Should only have one element for the time being
		foreach ($topic['topic']['content']['videos'] as $key=>$value) {
			
			if ($key > 0) {
				unset($topic['topic']['content']['videos'][$key]);
			}
		}
		
		//remove the video_tracks element
		unset($topic['topic']['content']['video_tracks']);
		
		
		$topic['topic']['content']['pages'][]=$pages;
		
		//var_dump($topic);
		//exit;		
		
		
		//need to make another call to the topic notes service to get the user data
		$path='/tn/'.$id;
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_get_data($request_url);
		
		foreach ($results as $result => $value) {
			
			$created=date('Y-m-d H:i:s', $value['date_created']);
			
			$note=array(
				'id'=>$value['id'],
				'text'=>$value['body']['value'],
				'created'=>$created,
			);
			$notes[]=$note;
		}
		
		
		
		//call for video bookmarks
		$path='/bm/'.$id;
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_get_data($request_url);
		
		//first go through and get the video references. Currently there will only be one
		foreach ($results as $result => $value) {
			
				$video_ids[]=$value['video_reference'][0];
			
		}
		
		
		
		
		$last_video_id=0;
		//go back through and build timestamp arrays for each video id
		foreach ($video_ids as $video_id) {
			
			//only unique video ids
			if ($video_id != $last_video_id) {
			
		
			foreach ($results as $result => $value) {			
				$videos['video']=$video_id;
				if ($video_id == $value['video_reference'][0]){
				$timestamp=array(
					'id'=>$value['id'],
					'title'=>$value['title'],
					'time'=>$value['time'],
					'note'=>$value['video_bookmark_note'],
				);
				$videos['timestamps'][]=$timestamp;
				}
			}
			
			//only unique video ids
			$last_video_id=$video_id;
			$videobookmarks[]=$videos;
		}
		}
		
		
		//call for video history - pass the id for just the one topic
		$path='/vh/'.$id;
		
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_get_data($request_url);
				
		//time viewed default to 0
		$timeviewed=0;
		$i=0;
		//look in the videos array for the same video id as the results
		foreach ($topic['topic']['content']['videos'] as $video) {
				
			$timeviewed=0;
			
			foreach ($results as $result => $value) {
				if ($video['id'] == $value['video_id']){
					//mark the last postition 
					$timeviewed = $value['last_position'];
				}
			}
			$topic['topic']['content']['videos'][$i]['timeviewed']=$timeviewed;
			$i++;
		}
		
				
		//add notes to the topic data
		$topic['topic']['userdata']['notes']=$notes;
		$topic['topic']['userdata']['videobookmarks']=$videobookmarks;
		
		$content=$topic['topic']['content'];
		$userdata=$topic['topic']['userdata'];
		
		$topic=array(
			'topic'=>$topic_top_level,
			'content'=>$content,
			'userdata'=>$userdata,
		);
		
		$this->response($topic, 200);
	}


	/**
	 * create a note. Need the following format for drupal:
	 * $node_data = array(
	  'uid'=>'1',
	  'name'=>'admin',
	 *  'status' => 1, //publish the node, if not put it on 0
	  'title' => 'This is a new note',	  
	  'body[und][0][format]' =>'full_html',
	  'body[und][0][value]' => '<p>This is my second note associated with Basic Types of Audits</p>',
	  'field_topic_reference[und][0][target_id]'=>'3792',
	);
	 * 
	 */
	
	public function note_post()
	{
		
		$this->input->post(); // returns all POST items without XSS filter
		
		//if no note passed forget it
		if (!$this->post('note')) {
			$this->response(array('error' => 'no note object in post or some other error'), 400);
		}
			
		$note=$this->post();
		$path='/tn/';
		
		/**
		 * course passes this format:
		 * {
			    "userid":"10001",
			    "topic":"10001",
			    "note":"<p>Some text with basic html formatting</p>",
			    "id":"30001" //only present for PUT
			}
		 */
		
		//title is not passed so use the first 15 characters
		$no_html=strip_tags($note['note']); 
		$title=substr($no_html,0,15);
		
		$node_data = array(
			  'uid'=>$_COOKIE['uid'],
			  'name'=>$_COOKIE['name'],
			  'status' => 1, //publish the node, if not put it on 0
			  'title' => $title,	  
			  'body[und][0][format]' =>'full_html',
			  'body[und][0][value]' => $note['note'],
			  'field_topic_reference[und][0][target_id]'=>$note['topic'],
			);
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_post_data($request_url, $node_data);
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
/**
 * PUT for updating. Must pass an id
 */
	public function note_put()
	{
		
		$this->put(); // returns all POST items without XSS filter
		
		//if no note passed forget it
		if (!$this->put('note')) {
			$this->response(array('error' => 'no note object in post or some other error'), 400);
		}
			
		$note=$this->put();
		$path='/tn/';
		
		/**
		 * course passes this format:
		 * {
			    "userid":"10001",
			    "topic":"10001",
			    "note":"<p>Some text with basic html formatting</p>",
			    "id":"30001" //only present for PUT
			}
		 */
		
		//title is not passed so use the first 15 characters
		$no_html=strip_tags($note['note']); 
		$title=substr($no_html,0,15);
		
		$node_data = array(
			  'uid'=>$_COOKIE['uid'],
			  'name'=>$_COOKIE['name'],
			  'status' => 1, //publish the node, if not put it on 0
			  'title' => $title,	  
			  'body[und][0][format]' =>'full_html',
			  'body[und][0][value]' => $note['note'],
			  'field_topic_reference[und][0][target_id]'=>$note['topic'],
			);
			
		//for updating the node id is passed to drupal in the url
		$path=$path.$note['id'];
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with PUT
		$results=rest_post_data($request_url, $node_data,'PUT');
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * DELETE for deleting notes. Must pass an id
	 * MUST USE THIS TYPE OF URL STUCTURE: 
	 * /restserver/index.php/api/course/note/id/4978
	 */
	public function note_delete()
	{
			
		$stuff=$this->get();	
		
		//if no note passed forget it
		if (!$this->get('id')) {
			$this->response(array('error' => 'no id passed in or some other error'), 400);
		}

			
		$nid=$this->get('id');
		$path='/tn/';
		
		//node data is just an empty array. Only need the id to delete		
		$node_data = array();
			
		//append node id to the url
		$path=$path.$nid;
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with DELETE
		$results=rest_post_data($request_url, $node_data,'DELETE');
		
		//$result returns boolean true
		
		$data=array();
		if ($results[0]==TRUE) {
			
			$data=array(			
				
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * create a video bookmark
	 * 
	 * post format:
	 * {
		    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
		    "video":"10031",
		    "time":"1300000", //milliseconds
		    "note":"Some optional text noting the significance of this time index",
		    "id":"40001"//primary key of video bookmark, only present on PUT
		}
	 */
	 public function video_bookmark_post()
	{
		
		$this->input->post(); // returns all POST items without XSS filter
		
		//if no time passed forget it
		if (!$this->post('time')) {
			$this->response(array('error' => 'no time key in post or some other error'), 400);
		}
			
		$video_bookmark=$this->post();
		$path='/bm/';
		
		//course passes this format:
		/*{
		    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
		    "video":"10031",
		 * "topic":"3792",
		    "time":"1300000", //milliseconds
		    "note":"Some optional text noting the significance of this time index",
		    "id":"40001"//primary key of video bookmark, only present on PUT
		}*/
		
				
		$node_data = array(
		  'uid'=>$_COOKIE['uid'],
			'name'=>$_COOKIE['name'],
		  'title' => 'Video bookmark',	  
          "field_video_bookmark_note[und][0][value]" => $video_bookmark['note'],          
          "field_time[und][0][value]" => $video_bookmark['time'],
          "field_topic_reference[und][0][target_id]" => $video_bookmark['topic'],
          "field_rcpa_video[und][0][target_id]" => $video_bookmark['video'],                      
          'status' => 1, //publish the node, if not put it on 0	
			);
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_post_data($request_url, $node_data);
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * Update video bookmark
	 */
	
	public function video_bookmark_put()
	{
		
		$this->put(); // returns all POST items without XSS filter
		
		//if no time passed forget it
		if (!$this->put('time')) {
			$this->response(array('error' => 'no time object in post or some other error'), 400);
		}
			
		$video_bookmark=$this->put();
		$path='/bm/';
		
		/**
		 * course passes this format:
		 * {
			    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
			    "video":"10031",
			    "time":"1300000", //milliseconds
			    "note":"Some optional text noting the significance of this time index",
			    "id":"40001"//primary key of video bookmark, only present on PUT
			}
		 */
		
		$node_data = array(
	          "field_video_bookmark_note[und][0][value]" => $video_bookmark['note'],
	          "field_topic_reference[und][0][target_id]" => $video_bookmark['topic'], 
	          "field_time[und][0][value]" => $video_bookmark['time'],
	          "field_rcpa_video[und][0][target_id]" => $video_bookmark['video'],              
	          'status' => 1, //publish the node, if not put it on 0	
	          'title' => "video bookmark api update"
			);
		
		
			
		//for updating the node id is passed to drupal in the url
		$path=$path.$video_bookmark['id'];
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with PUT
		$results=rest_post_data($request_url, $node_data,'PUT');
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * Delete a video bookmark
	 */
	public function video_bookmark_delete()
	{
			
		$stuff=$this->get();	
		
		//if no note passed forget it
		if (!$this->get('id')) {
			$this->response(array('error' => 'no id passed in or some other error'), 400);
		}

			
		$nid=$this->get('id');
		$path='/bm/';
		
		//node data is just an empty array. Only need the id to delete		
		$node_data = array();
			
		//append node id to the url
		$path=$path.$nid;
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with DELETE
		$results=rest_post_data($request_url, $node_data,'DELETE');
		
		//$result returns boolean true
		
		$data=array();
		if ($results[0]==TRUE) {
			
			$data=array(			
				
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	
	/**
	 * Create a video history timestamp (time viewed)
	 */
	public function video_history_post()
	{
		
		$this->input->post(); // returns all POST items without XSS filter
		
		//if no time passed forget it
		if (!$this->post('video')) {
			$this->response(array('error' => 'no video key in post or some other error'), 400);
		}
			
		$video_history=$this->post();
		$path='/vh/';
		
		//course passes this format:
		/*{
			    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
			    "video":"10031",
			    "time":"1300000" 
		 * 		"topic":"3792"
			}*/
		
				
		$node_data = array(
			  'uid'=>$_COOKIE['uid'],
			'name'=>$_COOKIE['name'],
			  'title' => 'Video history node created with API',	  
				'status' => 1, //publish the node, if not put it on 0
			  'body[und][0][format]' =>'full_html',
			  'body[und][0][value]' => '<p>rest Body</p>',
			  "field_topic_reference[und][0][target_id]" => $video_history['topic'],
		          "field_rcpa_video[und][0][target_id]" => $video_history['video'],
		          "field_last_position[und][0][value]" => $video_history['time'],
	);
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_post_data($request_url, $node_data);
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
/**
 * Video history update. Note that the PUT function will create the record if it does not exist in the database
 */
 public function video_history_put()
	{
		
		$this->put(); // returns all POST items without XSS filter
		
		//if no time passed forget it
		if (!$this->put('time')) {
			$this->response(array('error' => 'no time key in post or some other error'), 400);
		}
			
		$video_history=$this->put();
		$path='/vh/';
		
		/**
		 * course passes this format:
		 * {
			    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
			    "video":"10031",
			    "time":"1300000",
		 * 		"topic":"3792"
			}
		 */
		
		//call out GET to webservice to see if record already exists
		//build the corresponding url to drupal service
		$path='/vh/'.$video_history['topic'];
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		
		//call the service
		$results=rest_get_data($request_url);
		
				
		//If last_position is already set, update the record
		if (!empty($results)) {
			
			
			
			foreach ($results as $result => $value) {			
				$video_history_id=$value['id'];
			}
			
			//make the update
			//for updating the node id is passed to drupal in the url
			$path='/vh/'.$video_history_id;
			
			$node_data = array(
			  'body[und][0][value]' => '<p>viewtime update</p>',
			  'title' => 'A node updated with services 3.x and REST',	  
		          'status' => 1, //publish the node, if not put it on 0		  
			  "field_topic_reference[und][0][target_id]" => $video_history['topic'],            
			  "field_rcpa_video[und][0][target_id]" => $video_history['video'], 
			   "field_last_position[und][0][value]" => $video_history['time'],
			);
				
			//build the corresponding url to drupal service
			$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
			//call the service with PUT
			$results=rest_post_data($request_url, $node_data,'PUT');
			
			$data=array();
			if (isset($results['nid'])) {
				
				$data=array(			
					'id'=>$results['nid'],
	    			'success'=>'true',			
				);
				
			}else{
				
				$data=array(			
					'error'=>$results,
	    			'success'=>'false',			
				);		
					
				
			}
		
			$this->response($data, 200);
			
			
		}
		/**
		 * If the record does not exist lets make one
		 */
		
		//course passes this format:
		/*{
			    "userid":"10001", //or can be taken from the cookie, however drupal best identifies the current user
			    "video":"10031",
			    "time":"1300000" 
		 * 		"topic":"3792"
			}*/
		
				
		$node_data = array(
			  'uid'=>$_COOKIE['uid'],
			'name'=>$_COOKIE['name'],
			  'title' => 'Video history node created with API',	  
				'status' => 1, //publish the node, if not put it on 0
			  'body[und][0][format]' =>'full_html',
			  'body[und][0][value]' => '<p>rest Body</p>',
			  "field_topic_reference[und][0][target_id]" => $video_history['topic'],
		          "field_rcpa_video[und][0][target_id]" => $video_history['video'],
		          "field_last_position[und][0][value]" => $video_history['time'],
		          'field_video_id[und][0][value]' => $video_history['video'],
			);
		
		//rebuild the path
		$path='/vh/';
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_post_data($request_url, $node_data);
		
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * Create a book highlight
	 */
	 public function highlight_post()
	{
		
		$this->input->post(); // returns all POST items without XSS filter
		
		//if no note passed forget it
		if (!$this->post('highlights')) {
			$this->response(array('error' => 'no highlight object in post or some other error'), 400);
		}
			
		$highlight=$this->post();
		$path='/hl/';
		
		/**
		 * course passes this format:
		 * {
			    "id":"40001", //only present on PUT and DELETE
			    "page":"3",
			    "topic":"10001",
			    "book_edition":"2014",
			    "highlight":"<some json-encoded string data from the clipboard>" 
			}
		 */
		
		
		$node_data = array(
	  		'uid'=>$_COOKIE['uid'],
			  'name'=>$_COOKIE['name'],
	  			'title' => 'Highlight for book edition '.$highlight['book_edition'],	  
	          //"field_topic_exam_chapter_ref[und][0][value]" => 245,
	         // "field_page[und][0][value]" => $highlight['page'],
	          "field_highlight[und][0][value]" => $highlight['highlights'],
	          "field_topic_reference[und][0][target_id]" => $highlight['topic'],                      
	          'status' => 1, //publish the node, if not put it on 0		  
		);
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service
		$results=rest_post_data($request_url, $node_data);
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
/**
 * PUT to update highlight
 */
 public function highlight_put()
	{
		
		$this->put(); // returns all POST items without XSS filter
		
		//if no note passed forget it
		if (!$this->put('highlights')) {
			$this->response(array('error' => 'no note highlight in post or some other error'), 400);
		}
			
		$highlight=$this->put();
		$path='/hl/';
		
		/**
		 * course passes this format:
		 * {
			    "id":"40001", //only present on PUT and DELETE
			    "page":"3",
			    "topic":"10001",
			    "book_edition":"2014",
			    "highlight":"<some json-encoded string data from the clipboard>" 
			}
		 */
		
		
		$node_data = array(
		  		'title' => 'Highlight for book edition '.$highlight['book_edition'],
	          "field_topic_reference[und][0][target_id]" => $highlight['topic'],                      
	          'status' => 1, //publish the node, if not put it on 0	
	          //"field_page[und][0][value]" => $highlight['page'],
	          "field_highlight[und][0][value]" => $highlight['highlights'],
		);
			
		//for updating the node id is passed to drupal in the url
		$path=$path.$highlight['id'];
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with PUT
		$results=rest_post_data($request_url, $node_data,'PUT');
		
		$data=array();
		if (isset($results['nid'])) {
			
			$data=array(			
				'id'=>$results['nid'],
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
/**
 * DELETE page highlight
 */
	public function highlight_delete()
	{
			
		$stuff=$this->get();	
		
		//if no note passed forget it
		if (!$this->get('id')) {
			$this->response(array('error' => 'no id passed in or some other error'), 400);
		}

			
		$nid=$this->get('id');
		$path='/hl/';
		
		//node data is just an empty array. Only need the id to delete		
		$node_data = array();
			
		//append node id to the url
		$path=$path.$nid;
			
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		//call the service with DELETE
		$results=rest_post_data($request_url, $node_data,'DELETE');
		
		//$result returns boolean true
		
		$data=array();
		if ($results[0]==TRUE) {
			
			$data=array(			
				
    			'success'=>'true',			
			);
			
		}else{
			
			$data=array(			
				'error'=>$results,
    			'success'=>'false',			
			);		
				
			
		}
		
		$this->response($data, 200);
		
	}
	/**
	 * returns highlight for given topic
	 */
	public function highlight_get()
	{
		/**
		 * looking for id - can be topic id or highlight node id
		 */
		if (!$this->get('id')) {
			$this->response(array('error' => 'no id passed in or some other error'), 400);
		}
		
		$id=$this->get('id');
		$path='/hl/'.$id;
		$highlights=array();
		
		//build the corresponding url to drupal service
		$request_url = server_request_url($this->Protocol, $this->Domain, $this->Service, $path);
		
		//call the service
		$results=rest_get_data($request_url);
		
			
		//if not an array return error
		if (!is_array($results)) {
			 $this->response(array('error' => $results), 404);
		}

		foreach ($results as $key => $value) {
			$highlights['id']=$id;
			$highlights['highlights']=$value['highlight'];
		}
		$this->response($highlights, 200);
		
	}
}