<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Monitor sevice is semi-public and supplies quiz info to Monitoring Software interface
 *
 * 
*/

require APPPATH.'/libraries/REST_Controller.php';

class Monitor extends REST_Controller
{
	
	
	public function __construct()
    {
        
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Max-Age: 1000');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
		parent::__construct();
        

        //$this->load->library('Scache');
	}
	/*
	 * Expects POST in this format:
	 * {"pid":"3345","sections":"AUD, FAR","restricted":{"AUD":"1,2,3"},"start_date":"","end_date":"","uids":{"1234":"0","751234":"0","71234":"0","51234":"0","341234":"0","123443":"0"}}
	 * Return the IPQ stats for the monitoring center. We are looking for the aggregate % based on the sections provided. Just need to populate the uids array for each uid listed.
	 */
	function student_quizzes_post(){
		
		$data=$this->post();
		$prefix='';
		$exam_sections=explode(",",$data['sections']);
		$sections=$data['sections'];
		$sections=explode(",", $sections);
		$sections=implode("','", $sections);
		$sections="'$sections'";
		
		//we need the ids as a list for query
		$uids=$data['uids'];
		$uids = implode(',', array_keys($uids)); 
		
		//assemble data for query
		$query_data=array(
			'sections'=>$sections,
			'start_date'=>$data['start_date'],
			'end_date'=>$data['end_date'],
			'pid'=>$data['pid'],
			'uids'=>$uids,	
		
		);
				
		$this->load->model('testcenter_model');
		
		$results = $this->testcenter_model->get_saved_quizzes_monitor($query_data);
		
		//summarize data
		$count=0;
		//change uids back to an array
		$uids = explode(",", $uids);
		$summary=array();
		$count=0;
		$total_percent=0;
		$average_correct=0;
		// return array for each uid
		foreach ($uids as $uid) {
			
		
		
		foreach ($exam_sections as $exam_section) {
					
					
			foreach ($results as $result) {
				
				if (strtoupper($exam_section) == strtoupper($result['section']) && $uid == $result['student_id']) {
					
					$count++;
					//average the averages
					$total_percent=$total_percent+$result['total_percent'];
					$average_correct=$total_percent/$count;
					
				}
				
			}
			$tmp=array(
				'section'=>$exam_section,
				'average_correct_percentage'=>$average_correct,
				'number_of_quizzes_taken'=>$count,
			);
			$count=0;
			$average_correct=0;	
			$summary[$uid][$exam_section]=$tmp;			
			
		}
		
		
		
		}
		$this->response($summary, 200); // 200 being the HTTP response code
	}
    
}