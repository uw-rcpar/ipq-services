<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Milkyway sevice is semi-public and supplies quiz info to CF interface
 *
 * 
*/

require APPPATH.'/libraries/REST_Controller.php';

class Milkyway extends REST_Controller
{
	//return ALL sessions based on COLDFUSION student id
	function student_quizes_get(){
		
		if(!$this->get('cfid'))
        {
        	$this->response(array('error' => 'No cfid passed in url'), 400);
        }
		
		$this->load->model('testcenter_model');
        
		$cfid=$this->get('cfid');
		
		$student=$this->testcenter_model->get_student_by_cfid($cfid);
		
		$summary=array();
		
		//no records return empty set
		if (!$student) {
			$this->response($summary, 200);
		}		
		
				
		
		
		$sessions=$this->testcenter_model->get_saved_sessions($student[0]['id']);
		
		foreach ($sessions as $session) {
			
			$summary['sessions'][]=array(
			
				'id'=>$session['id'],
				'summary'=>$session['summary']
			
			);
			
		}
		
		$this->response($summary, 200); // 200 being the HTTP response code
	}
    
}