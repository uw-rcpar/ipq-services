<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * authorization api. sets cookies required for all other controlers
 *
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Auth extends REST_Controller
{
	//Admin Login 
	public function admin_login_post()
	{
		if ($this->post('username')) {
			//hard code login for now
			$username=$this->post('username');
			$password=$this->post('password');
			$this->load->model('testcenter_model');
			$login = $this->testcenter_model->get_login($username, $password);
			
			//if login is good, check for is_admin boolean from db
			if ($login) {
				
				
				
				if($login[0]['is_admin']==1){
					$result=array('SUCCESS'=>1);
					$result['sections']=array('AUD','BEC','FAR','REG');	
					$result['message']='All G dawg';
					
					//only set the studentID cookie if all is cool on login
					$cookie_id = array(
					    'name'   => 'studentid',
					    'value'  => $login[0]['id'],
					    'expire' => '0'
					    
					);
					
					//set an admin cookie
					$cookie_admin = array(
					    'name'   => 'isadmin',
					    'value'  => 'true',
					    'expire' => '0'
					    
					);
					
					$cookie_name = array(
					    'name'   => 'username',
					    'value'  => $username,
					    'expire' => '0'
					    
					);
					
					$this->input->set_cookie($cookie_id); 
					$this->input->set_cookie($cookie_name);
					$this->input->set_cookie($cookie_admin);
					$this->response($result, 200);
				
				
				} else {
					$result=array('SUCCESS'=>0);
					$result['sections']=array('FAIL');
					$result['message']='You do not have authoritay to admin this beotch';
					$this->response($result, 200);
				}
				
			} else {
				$result=array('SUCCESS'=>0);
				$result['sections']=array('FAIL');
				$result['message']='We have no record of that username or password';
				$this->response($result, 200);
			}			
			
		} else {
			$result=array('SUCCESS'=>0);
			$result['sections']=array('FAIL');
			$result['message']='Please enter both a username and password or stop wasting my time.';
			$this->response($result, 200);
		}
	}
}
	