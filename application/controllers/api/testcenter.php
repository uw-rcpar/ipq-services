<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Testcenter
 *
 * Main controler for test center api
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Steve
 * @link		http://stevewarelabs.com/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
//require APPPATHE.'/libraries/Scache.php';

class Testcenter extends REST_Controller
{
	
	/**
	 * secure this beeotch with cookies set in auth scripts
	 * data cookie is set for /testmodule
	 * username cookie is set in /testmodule-admin
	 */
	
	public function __construct()
    {
        parent::__construct();
        //look for drupal session cookie
        $drupal_session=FALSE;
        $names = array_keys($_COOKIE);
        foreach ($names as $name) {
            
            if (substr($name, 0, 4)=='SESS') {$drupal_session=TRUE;}
            
            
        }
        

  
        //override for local dev
        if ('localhost' != $_SERVER['HTTP_HOST']) {
        
        //both of these cookies are required
        //both of these cookies are required || $drupal_session==FALSE
        if(!isset($_COOKIE['Drupal_visitor_ticket']) || !isset($_COOKIE['data']) ){

            //return FailedResponse();
            $this->response(array('error' => 'authentication required'), 403);
            exit();
        }
        }
	}
	
	
	
	
	 
    //get questions and answers
	function multiple_choice_get()
    {
        	
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		
		$id=$this->get('id');
		$this->load->model('testcenter_model');
		$questions = $this->testcenter_model->get_questions_answers($id);
		
        if($questions)
        {
            	
			//format the db results into an array like views/topic.json
            $myquestions=array();
			$current_question=NULL;   
			
			$i=0;
            
            foreach ($questions as $question) {
            	
					
            	if ($current_question != $question['question_id']) {
					$myquestions['questions'][]=array(
						'question'=>$question['question'],
						'id'=>$question['question_id'],
						'chapter_id'=>$question['chapter_id'],
						'topic_id'=>$question['topic_id'],
						'col1_header'=>$question['col1_header'],
						'col2_header'=>$question['col2_header'],
						'col3_header'=>$question['col3_header'],
						'col4_header'=>$question['col4_header'],
						'answer_id'=>$question['answer_id'],
						'explanation'=>$question['explanation'],
						'video_id'=>$question['video_id'],
						'section'=>$question['section'],
						'difficulty'=>$question['difficulty']
						
						);						
					$current_question=$question['question_id'];
					//add in sub topics
					foreach($questions as $choice){
						if ($choice['question_id'] == intval($current_question)) {	
							$myquestions['questions'][$i]['choices'][]=array('choice'=>$choice['choice'], 'id'=>$choice['id'], 'explanation'=>$choice['explanation'], 'is_correct'=>$choice['is_correct']);
							
						}
					}
					$i++;				
					
				}
			    
			}	
				
				
            $this->response($myquestions, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any questions!'), 404);
        }
    }

	function quizzes_count_get() {
		if($student_id = $this->get('student_id')) {
			try {
				$section = $this->get('section') ? $this->get('section') : null;
				$this->load->model('testcenter_model');
				$data = $this->testcenter_model->get_quizzes_count($student_id, $section);
				$this->response($data, 200);
			} catch (Exception $e) {
				$this->response($e, 200);
			}
		} else {
			$this->response(false);
		}
	}

	function quizlets_by_qid_post() {
		$myquizlets['quizlets']=array();
		$this->load->model('testcenter_model');
		$this->load->helper('myhelpers');
		$tmp = array();
		
//		$cf_student_id=get_cf_student_id();
		
		//get the json post
		$pref=$this->post();

		if (!isset($pref['multiquizlet']) && !isset($pref['multitype'])) {
			$quizlets = array();
			array_push($quizlets, $pref['qids']);
		}
		else {
			$quizlets = json_decode($pref['qids'], true);
		}
		foreach($quizlets as  $key => $quizlet) {
		
			if (!isset($pref['multiquizlet']) && !isset($pref['multitype'])) {
				$qids = implode(',',$quizlet);
			}
			else if(!isset($pref['multitype'])) {
//				$qids = implode(',',$quizlet['questions']);
				$qids = array();
				foreach($quizlet['questions'] as $q) {
					array_push($qids, $q['id']);
				}
				$qids = implode(',', $qids);
			}
			else {
				// multitype
				$qids = $quizlet['qids'];
				$qids = implode(',',$qids);
			}
			
			$prefix = '';
			//$topics='1,5';
			$no_questions=count($qids);
			//question id list
			$qid_list='';
			$i=0;
			$quizlet_questions=array();
			if (!isset($pref['multiquizlet']) && !isset($pref['multitype'])) {
					if ($pref['type'] == 'mcq' || $pref['type'] == 'multiple-choice') {
						$quizlet_questions = $this->testcenter_model->get_mc_by_ids($qids);
						$type = 'mcq';
					}
					else {
						$quizlet_questions = $this->testcenter_model->get_tbs_by_ids($qids);
						$type = 'tbs';
					}
			}
			else {
					if ($quizlet['type'] == 'mcq' || $quizlet['type'] == 'multiple-choice') {
						$quizlet_questions = $this->testcenter_model->get_mc_by_ids($qids);
						$type = 'mcq';
					}
					else {
						$quizlet_questions = $this->testcenter_model->get_tbs_by_ids($qids);
						$type = 'tbs';
					}
			}
					
					if ($quizlet_questions) {
						
						//sor the results on questionID so we can have the same order after saving the session
						usort($quizlet_questions, function($a, $b) { return $a["id"] - $b["id"]; });
						
												
						//$questions['questions']=array();
						//for multiple choice, get the answers
						if ($type =='mcq') {
							
							$questions['type']='multiple-choice';
						
							//list out the question info
							foreach ($quizlet_questions as $question) {
								
								$questions['questions'][]=array(				
								'difficulty'=>$question['difficulty'],
								'id'=>$question['id'],
								'deprecated'=>$question['deprecated'],
								'question'=>$question['question']
								);
								
								
								//we need the videoID from CF to form the course url
				
								$topic_data = $this->testcenter_model->get_topics_by_qid($question['id'], $type);
								
								$topic_array = array();
								
								//topic and chapter in its own array
								foreach ($topic_data as $topic_row) {
									
									$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
									
									$topic_array[]=array(
									'topic_id'=>$topic_row['topic_id'],
									'chapter_id'=>$topic_row['chapter_id'],
									'course_url'=>$course_url
									);
								}
				
				
				
								$questions['questions'][$i]['topics']=$topic_array;
								
								//get the answers
								$quizlet_answers = $this->testcenter_model->get_quizlets_answers($question['id']);
								
								//list out the choices				
								foreach ($quizlet_answers as $answer) {
									
									if (intval($answer['question_id']) == intval($question['id'])) {
										
										$questions['questions'][$i]['choices'][]=array(
										'choice'=>$answer['choice'],
										'explanation'=>$answer['explanation'],
										'id'=>$answer['id'],
										'is_correct'=>$answer['is_correct']
										);
									}
									
									
								}
								$i++;
								
							}
						}  else {
							//format the tbs questions
							$questions['type']=$type;
						
							//list out the question info
							foreach ($quizlet_questions as $question) {
								$questions['questions'][]=array(				
								'difficulty'=>$question['difficulty'],
								'id'=>$question['id'],
								'question_json'=>$question['question_json'],
								'type'=>$question['type']
								);
								
								//we need the videoID from CF to form the course url
								$topic_data = $this->testcenter_model->get_topics_by_qid($question['id'], $type);
								
								$topic_array = array();
								
								//topic and chapter in its own array
								foreach ($topic_data as $topic_row) {
									
									$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
									
									$topic_array[]=array(
									'topic_id'=>$topic_row['topic_id'],
									'chapter_id'=>$topic_row['chapter_id'],
									'course_url'=>$course_url
									);
								}		
				
								$questions['questions'][$i]['topics']=$topic_array;
								
								$i++;
								
							}
							
							}				
						
					}
					unset($quizlet_questions);	
									
				$pickedTopics=array();
				$prefix = '';
					$myquizlets['quizlets'][]=$questions;
					
					//reset questions array
					unset($questions['questions']);
		}

//		$this->response(array('test'=>$myquizlets), 200); // 200 being the HTTP response code
		$this->response($myquizlets, 200);

	}

	//get quizlets
	function quizlets_post() 
	{
		
		
		
		$myquizlets['quizlets']=array();
		$this->load->model('testcenter_model');
		$this->load->helper('myhelpers');
		
		$sid=get_sid();
		
		//get the json post
		$prefs=$this->post();
		
		//NASBA flag

		$nasba = (array_key_exists('nasba', $prefs) && $prefs['nasba']=='true') ? TRUE : FALSE ;
         //get filterQuestions flag - one of these values: "unanswered" or "incorrect".
        $filterQuestions=array_key_exists('filterQuestions', $prefs) ? $prefs['filterQuestions'] : 'none';
		$prefs= array_key_exists('quizlets', $prefs) ? $prefs['quizlets'] : $prefs;
       
		//sort the post by value so mcq goes before tbs
		sort($prefs);	
//		$filtered = array();
		$exclude_list_mcq = array(0);
		$exclude_list_tbs = array(0);
				
		//get a list of all the topicIDs to pass to the db
		foreach ($prefs as $pref) {
				
			
			$topics='';
			$prefix = '';
			//$topics='1,5';
			$section='aud';
			$no_questions=0;
			$type='multiple-choice';
			//question id list
			$qid_list='';
			$i=0;
			$quizlet_questions=array();
			
			
			//get the number of questions for the query
			$no_questions=$pref['qty'];
			
			$pickedTopics[] = $pref['pickedTopics'];
			
			//set type to multiple-choice
			if (!isset($pref['type'])) {
				
				$pref['type']='multiple-choice';
			}
			
    			foreach ($pickedTopics as $mytopics) {
					
    				//get a list of topicIDs for the questions query
					foreach ($mytopics as $topic) {
						$topics .=$prefix . $topic['topicID'];
    					$prefix = ',';
					}
					
					
					//get the questions after you have the topic list
					
					if ($pref['type'] == 'multiple-choice') {
						$quizlet_questions = $this->testcenter_model->get_quizlets_questions($pref['type'],$topics,$no_questions,$sid,$nasba,$filterQuestions, implode(',', $exclude_list_mcq));
//						$filtered = $quizlet_questions['filtered'];
//						$quizlet_questions = $quizlet_questions['results'];
						foreach ($quizlet_questions as $id) {
							$exclude_list_mcq[] = $id['id'];
						}
					}
					if ($pref['type'] !== 'multiple-choice') {
						$quizlet_questions = $this->testcenter_model->get_quizlets_questions($pref['type'],$topics,$no_questions,$sid,$nasba,$filterQuestions, implode(',', $exclude_list_tbs));
//						$quizlet_questions = $quizlet_questions['results'];
						foreach ($quizlet_questions as $id) {
							$exclude_list_tbs[] = $id['id'];
						}
					}
				
					
						
									
					
					if ($quizlet_questions) {
						
						//sor the results on questionID so we can have the same order after saving the session
						usort($quizlet_questions, function($a, $b) { return $a["id"] - $b["id"]; });
						
												
						//$questions['questions']=array();
						//for multiple choice, get the answers
						if ($pref['type']=='multiple-choice') {
							
							$questions['type']='multiple-choice';
						
							//list out the question info
							foreach ($quizlet_questions as $question) {
								
								$questions['questions'][]=array(				
								'difficulty'=>$question['difficulty'],
								'id'=>$question['id'],
								'deprecated'=>$question['deprecated'],
								'question'=>$question['question']
								);
								
								
								//we need the videoID from CF to form the course url
								$topic_data = $this->testcenter_model->get_topics_ids($question['topic_id']);
								
								$topic_array = array();
								
								//topic and chapter in its own array
								foreach ($topic_data as $topic_row) {
									
									$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
									
									$topic_array[]=array(
									'topic_id'=>$topic_row['topic_id'],
									'chapter_id'=>$topic_row['chapter_id'],
									'course_url'=>$course_url
									);
								}
				
				
				
								$questions['questions'][$i]['topics']=$topic_array;
								
								//get the answers
								$quizlet_answers = $this->testcenter_model->get_quizlets_answers($question['id']);
								
								//list out the choices				
								foreach ($quizlet_answers as $answer) {
									
									if (intval($answer['question_id']) == intval($question['id'])) {
										
										$questions['questions'][$i]['choices'][]=array(
										'choice'=>$answer['choice'],
										'explanation'=>$answer['explanation'],
										'id'=>$answer['id'],
										'is_correct'=>$answer['is_correct']
										);
									}
									
									
								}
								$i++;
								
							}
						
						}  else {
							//format the tbs questions
							$questions['type']=$pref['type'];
						
							//list out the question info
							foreach ($quizlet_questions as $question) {
								
								$questions['questions'][]=array(				
								'difficulty'=>$question['difficulty'],
								'id'=>$question['id'],
								'question_json'=>$question['question_json'],
								'type'=>$question['type']
								);
								
								
								//we need the videoID from CF to form the course url
								$topic_data = $this->testcenter_model->get_topics_ids($question['topic_id']);
								
								$topic_array = array();
								
								//topic and chapter in its own array
								foreach ($topic_data as $topic_row) {
									
									$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
									
									$topic_array[]=array(
									'topic_id'=>$topic_row['topic_id'],
									'chapter_id'=>$topic_row['chapter_id'],
									'course_url'=>$course_url
									);
								}		
				
				
								$questions['questions'][$i]['topics']=$topic_array;
								
								
								$i++;
								
							}
			
							
								
							}				
						
						
						
					}
					
					unset($quizlet_questions);	
									
				}
				$pickedTopics=array();
				
				$prefix = '';
				if (isset($questions)) {
					$myquizlets['quizlets'][]=$questions;
					
					//reset questions array
					unset($questions['questions']);
				}
				else {
					$myquizlets['quizlets'][]=null;
				}
		}
		//make sure mcq's come first
		sort($myquizlets['quizlets']);
//		$myquizlets['filtered'] = $filtered;
//		$myquizlets['excluded_mcq'] = $exclude_list_mcq;
//		$myquizlets['excluded_tbs'] = $exclude_list_tbs;
		$this->response($myquizlets, 200); // 200 being the HTTP response code
		
		
	}

	function allcontent_get(){
        //we neede the session variable from the url controller/resource/paramenter/value
        //  /api/testcenter/topics/section/aud/format/json        
       
       	//if( ! $cache = $this->scache->read("allcontent") ) {
		if(!$this->get('section'))
        {
        	$this->response(NULL);
        }
        try {	
	        $get_chapter = $this->get('chapter');
			$get_section = $this->get('section');
			$this->load->model('testcenter_model');
			$data = $this->testcenter_model->get_allcontent($get_chapter, $get_section);
        	$this->response($data, 200);
        	
	    } catch(Exception $e) {
	    	$this->response($e, 200);
	    }
		 
	}
	
	//get topics and sub topics - add some code
	function chapters_get()
    {
        //we neede the session variable from the url controller/resource/paramenter/value
        //  /api/testcenter/topics/section/aud/format/json        
        if(!$this->get('section'))
        {
        	$this->response(NULL, 400);
        }	
		
        
		$section=$this->get('section');
		$this->load->model('testcenter_model');
		$chapters = $this->testcenter_model->get_topics($section);
		//get the number of multiple choice questions per topic
		$no_multiple_choice = $this->testcenter_model->get_num_mcq($section);
		//get the number of task based simulation questions
		$no_tbs = $this->testcenter_model->get_num_tbs($section);
        
		$mytopics=array();
		$this->load->helper('myhelpers');
		
						
        if($chapters)
        {
            
           
           //format the db results into an array like views/topic.json
           
			$current_topic=NULL;   
			$mytopics['section']=$chapters[0]['section'];
			$i=0;
			//m stands for 'multiple choice'. Represents the number of multiple choice questions
			$m=0;
			//t represents the number of task based simulations
			$t=0;
            
            foreach ($chapters as $chapter) {
            	
					
            	if ($current_topic != $chapter['chapter_id']) {
					$mytopics['chapters'][]=array('chapter'=>$chapter['chapter'],'id'=>$chapter['chapter_id']);						
					$current_topic=$chapter['chapter_id'];
					//add in sub topics
					foreach($chapters as $subtopic){
						if ($subtopic['chapter_id'] == intval($current_topic)) {
							
							$m=$this->testcenter_model->get_count_mc($subtopic['id']);
							
							$t=get_no_mcq($no_tbs, $subtopic['id']);
							
							//prefix based on chapter and topic order
							$prefix=$subtopic['chapter_prefix'].'.'.$subtopic['topic_prefix'];
							
							
							//$mytopics['chapters'][$i]['topics']['questions']=$questions;
							$mytopics['chapters'][$i]['topics'][]=array('topic'=>$subtopic['topic'], 'id'=>$subtopic['id'], 'm'=>$m, 't'=>$t, 'prefix'=>$prefix);
							
						}
					}
					$i++;	
				}
			    
			}			
			 
            $this->response($mytopics, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any topics!'), 404);
        }
    }
	


	//get topics
	function topics_get()
    {
    	if(!$this->get('section'))
        {
        	$this->response(array('error' => 'No exam section passed. Use far, aud, reg or bec'), 400);
        }
        
		$section=$this->get('section');
		$this->load->model('testcenter_model');
		$topics = $this->testcenter_model->get_topics($section);
		$mytopics=array();
		
		if($topics)
        {
        	//var_dump($topics);
			//format the db results into an array like views/topic.json
           
			$current_topic=NULL;   
			$mytopics['section']=$topics[0]['section'];
			$i=0;
			foreach ($topics as $topic) {
				$mytopics['topic'][]=array($topic['chapter']=>$topic['chapter_id'],$topic['topic']=>$topic['id']);		
			}
			$this->response($mytopics, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any topics!'), 404);
		}
	}

	/**
	 * insert record in student_progress table
	 * data array needs these paramenters
	 * 
	 * student_id - required
	 * question_id - required
	 * answer_id - required
	 * correct_answer -required
	 * notes
	 * bookmark - default false
	 */
	function student_progress_post()
    {
    	//no id gets the ol heave ho	
    	/*
		if(!$this->get('id') && !$this->post('id'))
				{
					$this->response(NULL, 400);
				}*/
				
		
		$message = array('id' => $this->get('id'), 'notes' => $this->post('notes'), 'student_id' => $this->post('student_id'), 'message' => 'ADDED!');
		
		
        $this->response($message,200); 
		//insert
		if ($this->post('id')) {
			
			$data = array(
				'student_id' => $this->post('id'), 
				'question_id' => $this->post('question_id'), 
				'answer_id' => $this->post('answer_id'), 
				'correct_answer' => $this->post('correct_answer'),
				'notes' => $this->post('notes'),
				'bookmark' => $this->post('bookmark')
				);
				
			
			$this->load->model('testcenter_model');
			$result = $this->testcenter_model->set_student_progress($data);
			if($result) {
				$this->response($result, 200);
			} else {
				$this->response(array('error' => 'Insert failed!'), 400);
			}
		} else {
			$this->response(array('error' => 'no freakin post data!'), 400);
		}
	}

	function section_test_post(){
		
		if ($this->post('email')) {
			$result['sections'][]=array('far','aud','reg','bec');
			$result['sections'][]=array('success'=>'TRUE');
			$this->response($result, 200);
		} else {
			$this->response(array('error' => 'no email passed in post or some other error'), 400);
		}
	}
/*
	function get_all_students_post() {
		$this->load->model('testcenter_model');
		$students = $this->testcenter_model->get_all_students();
		$this->response($students, 200);
	}
*/
		
	function calculate_chapter_scores_post($sid) {
		$this->load->model('testcenter_model');
			$sid = $student['id'];
			$saved_sessions = $this->testcenter_model->get_saved_sessions($sid);
			foreach($saved_sessions as $session) {
				$breakdowns = json_decode($session['summary'], true)['session']['summary']['breakdown'];
				foreach($breakdowns as $breakdown) {
					$chapter_id = $breakdown['chapter_id'];
					$correctQuestions[$chapter_id] = $breakdown['qtyCorrect'];
					if (!isset($total_questions[$chapter_id])) {
						$total_questions[$chapter_id] = $this->testcenter_model->get_num_questions_by_chapter($chapter_id);
					}
				}
			}
			foreach($correctQuestions as $chapter => $correct) {
	//var_dump($correct/$total_questions[$chapter]);
				$chapter_scores[$chapter] = round(($correct/$total_questions[$chapter])*100, 2);
			}
			$student_progress = array(
				'student_id' => $sid,
				'chapter_scores' => json_encode($chapter_scores)
			);
			$result = $this->testcenter_model->set_student_progress($student_progress);
echo "foo";
		$this->response($chapter_scores, 200);
		$this->response(NULL, 200);
	}
	function save_quizlet_results_post(){
			
			
		//check the results
		$this->input->post(); // returns all POST items without XSS filter
		$results=$this->post();
		if (!$results['session']['name']) {
			
			$this->response(array('error' => 'No name variable passed'), 400);
		}		
		
		//student id and name are in a json string inside a cookie
		$cookie_obj=json_decode($this->input->cookie('data', TRUE));
		$cookie_data = (array) $cookie_obj;
		//var_dump($cookie_data);
		//exit;
		
		//set unanswered to list
		$unanswered_list = implode(',', $results['session']['unanswered']);
		
		//dump the entire post as a json object into the summary field
		$summary = json_encode($results);
		
		$data=array(
		
			'session_name'=>$results['session']['name'],
			//studentid set at login
			'student_id'=>$cookie_data['studentID'],
			'total_questions'=>$results['session']['totalQuestions'],
			'unanswered'=>$unanswered_list,
			'summary'=>$summary
		);
		
		//if all good, lets save this puppy
		$this->load->model('testcenter_model');
		$result = $this->testcenter_model->save_quizlet($data);
		
		
		foreach ($results['session']['answers'] as $answer) {
			
			$answers_list=implode(',', $answer['answers']);
			$attempts_list=implode(',', $answer['attempts']);
			
			$data=array(
			
			'quizlet_id'=>$result['quizlet_id'],
			'question_id'=>$answer['question_id'],
			'quizlet'=>$answer['quizlet'],
			'type'=>$answer['type'],
			'answers'=>$answers_list,
			'attempts'=>$attempts_list,
			'score'=>$answer['score']
						
			);
			
			$detail_id = $this->testcenter_model->save_quizlet_details($data);
		}
		
		
		
		$this->response(array('success'=>'true', 'quizID'=> $result['quizlet_id'], 'data'=>$detail_id), 200);
	}

	function get_session_metadata_post() {
		$results=$this->post();
		$session_id = $results['session_id'];
	
		$this->load->model('testcenter_model');
		$result = $this->testcenter_model->get_session_metadata($session_id);
		$this->response($result, 200);
	}
	
	//return ALL sessions based on student id
	function return_quizlet_results_get(){
		
		$this->load->helper('myhelpers');
		$this->load->model('testcenter_model');
		$student_id=get_sid();
		
		$summary=array();
		
		$sessions=$this->testcenter_model->get_saved_sessions($student_id);
		
		foreach ($sessions as $session) {
			$session_id = $session['id'];
			$summary['sessions'][]=array(
			
				'id'=>$session_id,
				'summary'=>$session['summary'],
				'metadata'=>$this->testcenter_model->get_session_metadata($session_id)
			
			);
			
		}
		
		$this->response($summary, 200); // 200 being the HTTP response code
	}
	
	//return ALL sessions based on COLDFUSION student id
	function student_quizes_get(){
		
		if(!$this->get('cfid'))
        {
        	$this->response(array('error' => 'No cfid passed in url'), 400);
        }
		
		$this->load->model('testcenter_model');
        
		$cfid=$this->get('cfid');
		
		$student=$this->testcenter_model->get_student_by_cfid($cfid);
		
		$summary=array();
		
		//no records return empty set
		if (!$student) {
			$this->response($summary, 200);
		}		
		
				
		
		
		$sessions=$this->testcenter_model->get_saved_sessions($student[0]['id']);
		
		foreach ($sessions as $session) {
			
			$summary['sessions'][]=array(
			
				'id'=>$session['id'],
				'summary'=>$session['summary']
			
			);
			
		}
		
		$this->response($summary, 200); // 200 being the HTTP response code
	}

	function student_progress_get()
    {
    	if(!$this->get('student_id'))
        {
        	$this->response(NULL, 400);
        }
		//set a default question id to 0. Means it has not passed
		$question_id = (!$this->get('question_id') ? 0 : $this->get('question_id'));
		
		
		$student_id=$this->get('student_id');
		$this->load->model('testcenter_model');
		$student_info = $this->testcenter_model->get_student_info($student_id,$question_id);
		
		
		if($student_info)
        {
            	
			//format the db results into an array like views/topic.json
            $mystudent_info=array();
			$current_question=NULL;
			$mystudent_info['student_id']=$student_info[0]['student_id'];   
			
			$i=0;
            
            foreach ($student_info as $question) {
            	
					
            	if ($current_question != $question['question_id']) {
					$mystudent_info['questions'][]=array(
						'question_id'=>$question['question_id'],
						'correct_answer'=>$question['correct_answer'],
						'timer_preference'=>$question['timer_preference'],
						'notes'=>$question['notes'],
						'bookmark'=>$question['bookmark']						
						);						
					$current_question=$question['question_id'];
					//add in sub topics
					foreach($student_info as $answer){
						if ($answer['question_id'] == intval($current_question)) {
							
							$mystudent_info['questions'][$i]['answer_attempts'][]=array('answer_id'=>$answer['answer_id'], 'is_correct'=>$answer['is_correct'], 'seconds_to_complete'=>$answer['seconds_to_complete'], 'timestamp'=>$answer['timestamp']);
							
						}
					}
					$i++;				
					
				}
			    
			}	
				
				
            $this->response($mystudent_info, 200); // 200 being the HTTP response code
        }
		 
		 else {
			$this->response(array('error' => 'No student progress data for parameters passed'), 400);
		}
		
	}
	
	
	
	
	
	//save answer
	public function save_question_post()
	{
		
		$this->input->post(); // returns all POST items without XSS filter 	
			
		if ($this->post('question')) {
			
						
			$today = date("Y-m-d H:i:s");
			
			$question=$this->post('question', FALSE);
			
			$json = json_decode($question, true);
			//$json = $question;
			
									
			//print_r($question);
			//setup a new array for the insert
			$data=array();
			$data_choice=array();
			$this->load->model('testcenter_model');
			
			//save which doc the admin is currently using
			$student = $this->testcenter_model->get_student($_COOKIE['studentid']);
			$docID=$student[0]['docID'];
			
			//set duplicate override, default false
			$override_duplicate = ($this->post('override_duplicate')) ? TRUE : FALSE ;
			
			//multiple choice
			switch ($json['type']) {
			    case "multiple-choice":
					
					$prefix = '';
					$topic_id_list='';
					
					//set topicIDs to a list
					
					foreach($json['topics'] as $topic) {
						
						$topic_id_list .=$prefix . $topic['topic_id'];
    					$prefix = ',';
						$mychapter=$topic['chapter_id'];
						
					}
						
					$data['topic_id']=$topic_id_list;
					$data['chapter_id']=$mychapter;
					$data['question']=$json['question'];
					//$data['section']=$json['section'];
					$data['section']='aud';
					//record who entered the question
					$data['entered_by_studentid']=$_COOKIE["studentid"];
					$data['docID']=$docID;
					$data['createdBy']=$_COOKIE["username"];
					$data['createdOn']=$today;
					
					//before proceeding let's check for a duplicate question
					$is_duplicate=$this->testcenter_model->check_duplicate($data['question']);
					if (!empty($is_duplicate) && !$override_duplicate) {
						
						
						$duplicate=array();
						
						$topics=$this->testcenter_model->get_topics_by_qid($is_duplicate[0]['id']);
						
						$duplicate['error']='Duplicate question';
						$duplicate['duplicate_question']=$is_duplicate;
						$duplicate['topics']=$topics;
							
						$this->response($duplicate, 200);
						
					}
					
					
					
					//set_question returns newly inserted id					
					$question_id=$this->testcenter_model->set_question($data);
					
					//insert answers
					foreach($json['choices'] as $choice) {
						
						$data_choice['question_id']=$question_id;
						$data_choice['choice']=$choice['choice'];
						$data_choice['explanation']=$choice['explanation'];
						$data_choice['is_correct']=$choice['is_correct'];
						
						$answer=$this->testcenter_model->set_answer($data_choice);
						
						//if this is the correct answer update the questions table
						if ($data_choice['is_correct']==1) {
							$correct['answer_id']=$answer['answer_id'];
							$correct['explanation']=$data_choice['explanation'];
							$a=$this->testcenter_model->set_correct_answer($correct, $data_choice['question_id']);
							
						}
						
						}
						//print_r($data);
						$result=array('SUCCESS'=>1,'question_id'=>$question_id);
			        
			        break;
			   
			    //simulation
			    case "simulation":
			        
			        break;
			    
			}
			
			
			//$result['sections']=array('AUD','BEC','FAR','REG');
			
			$this->response($result, 200);
		} else {
			$this->response(array('error' => 'no question object in post or some other error'), 400);
		}
	}

	
	public function get_question_preview_v2_post() {
		$this->input->post();
		$data=$this->post();
		$limit=200;
		$quiz_data=array();
		
		$chapter_id=$data['chapter_id'];		
		$status=$data['category'];
		$type=$data['type'];
		$nasba = (array_key_exists('nasba', $data) && $data['nasba']=='true') ? TRUE : FALSE;
		
		$this->load->helper('myhelpers');
		$sid=get_sid();
		
		$this->load->model('testcenter_model');
		
		//chapter ids is an array
		$chapter_list=implode(',', $chapter_id);
				
		if ($chapter_id) {
			$questions = $this->testcenter_model->get_question_preview_v3($sid, $chapter_list, $status, $type, $limit, $nasba);			
		}
        
        
				
		if (isset($questions['mcq'])) {
		    
            
			$qid=0;
			$questions_summary=array();
			$choices=array();
			$answer=array();
			foreach ($questions['mcq'] as $question) {
				
				if($qid != $question['qs_id']) {
						
					$qid = $question['qs_id'];
					//reset choices variable
					$choices=array();
					$answer=array();
					if (!array_key_exists('noted',$question)){
						$question['noted']='[]';
					}
                    
                   
					
					$question_data=array(
						'id'=>$question['qs_id'],
						'answer_id'=>$question['my_answer_id'],
						'question'=>$question['question'],
						'explanation'=>$question['q_explanation'],					
						'noted'=>$question['noted'],
					
					);
					
				}
				
				//build the choice or answers array
				if($qid == $question['qs_id']) {
					
					$choice=array(
						'id'=>$question['answer_id'],
						'question_id'=>$question['qs_id'],
						'choice'=>$question['choice'],
						'explanation'=>$question['explanation'],
						'is_correct'=>$question['is_correct'],
					
					);
					$choices[]=$choice;
					
					//pull out the student's answer
					if ($question['answer_id']==$question['my_answer_id']) {
						
						$answer=array(
							'id'=>$question['answer_id'],
							'question_id'=>$question['qs_id'],
							'choice'=>$question['choice'],
							'explanation'=>$question['explanation'],
							'is_correct'=>$question['is_correct'],
					
						);
						
					}
					
				}			
				$question_data['choices']=$choices;
				if (!empty($answer)) {$question_data['answer']=$answer;}				
				$questions_summary[$qid]=$question_data;
			}
			$result['mcq']=$questions_summary;
			
			
		}

		//format tbs
		if (isset($questions['tbs'])) {
			
			$qid=0;
			$questions_summary=array();
			
			foreach ($questions['tbs'] as $question) {
				
				if($qid != $question['qs_id']) {
				    
                    if (!array_key_exists('noted',$question)){
                        $question['noted']='[]';
                    }
                    
                    if (!array_key_exists('my_answer_id',$question)){
                        $question['my_answer_id']='';
                    }
						
					$qid = $question['qs_id'];
					//reset choices variable
					$question_data=array(
						'id'=>$question['qs_id'],
						'answer_id'=>$question['my_answer_id'],
						'question_json'=>$question['question_json'],
						'noted'=>$question['noted'],
						'type'=>$question['type'],
					
					);
					
				}
				$questions_summary[$qid]=$question_data;
			}
			$result['tbs']=$questions_summary;
			
			
		}
		$quiz_data['questions']=$result;
		$this->response($quiz_data, 200);	
//		$this->response(array($chapter_id, $status, $type), 200);
	}

	public function get_question_preview_post() {
		$this->input->post();
		$this->load->model('testcenter_model');
//		$student_id = $this->post('student_id');
		$quizlets = $this->post('quizlets');
		$chapter_id = $this->post('chapter_id');
		$status = $this->post('category');
		$limit = $this->post('limit');
		$type = $this->post('type');
		$session_id = $this->post('session_id');

		$this->load->helper('myhelpers');
		$sid=get_sid();
		
		if ($type == 'multipleChoice' || $type == 'mcq') {
			$type = 'mcq';
		} else if ($type != 'mcq' && $type != 'multipleChoice') {
			$type = 'tbs';
		}
		
		if ($chapter_id) {
			$questions = $this->testcenter_model->get_question_preview_v4($sid, $chapter_id, $status, $type, $limit);			
		}
		else if ($quizlets) {
			//quizlets model designed by Dominique
			$results = $this->testcenter_model->get_question_preview_quizlets($sid, $quizlets, $status, $type, $session_id, $limit);
			$this->response($result, 200);	
		}
		
		//format for mcq
		if ($type=='mcq') {
			$question_data = array();
			$choicesIdByQuestionId = array();
			foreach ($questions as $question) {
				$qidx = 'qs_id_' . $question['qs_id'];
				$choice = array(
					'id' => $question['answer_id'],
					'question_id' => $question['qs_id'],
					'choice' => $question['choice'],
					'explanation' => $question['explanation'],
					'is_correct' => $question['is_correct']	
				);
				if(array_key_exists($qidx, $question_data)) {
					if(array_key_exists($qidx, $choicesIdByQuestionId) && !in_array($choice['id'], $choicesIdByQuestionId[$qidx])) {
						$question_data[$qidx]['choices'][] = $choice;
						$choicesIdByQuestionId[$qidx][] = $choice['id'];
					}
				} else {
					$question_data[$qidx] = array(
						'id' => $question['qs_id'],
						'answer_id' => $question['my_answer_id'],
						'question' => $question['question'],
						'explanation' => $question['q_explanation'],
						'noted' => $question['noted'],
						'choices' => array($choice)
					);
					$choicesIdByQuestionId[$qidx] = array($choice['id']);
				}
				//pull out the student's answer
				if ($question['answer_id']==$question['my_answer_id']) {
					$question_data[$qidx]['answer'] = array(
						'id' => $question['answer_id'],
						'question_id' => $question['qs_id'],
						'choice' => $question['choice'],
						'explanation' => $question['explanation'],
						'is_correct' => $question['is_correct']
					);
				}
			}
			$result['questions'] = array_values($question_data);
			$result['type']='mcq';
		}

		//format tbs
		if ($type=='tbs') {
			
			$qid=0;
			$questions_summary=array();
			
			foreach ($questions as $question) {
				
				if($qid != $question['qs_id']) {
						
					$qid = $question['qs_id'];
					//reset choices variable
					$question_data=array(
						'id'=>$question['qs_id'],
						'answer_id'=>$question['my_answer_id'],
						'question_json'=>$question['question_json'],
						'noted'=>$question['noted'],
						'type'=>$question['type'],
						'type_long_name'=>$question['type_long_name'],
					
					);
					
				}
				$questions_summary[$qid]=$question_data;
			}
			$result['questions']=$questions_summary;
			$result['type']='tbs';
			
		}
		
		$this->response($result, 200);	
//		$this->response(array($chapter_id, $status, $type), 200);
	}

	//edit the question
	public function update_mc_question_post()
	{
			
		$this->input->post(); // returns all POST items without XSS filter 	
		
		if ($this->post('question')) {
			
			$question=$this->post('question', FALSE);
			
			$result = $question;
			
			$this->load->model('testcenter_model');
			
			//edit mc or tbs
			switch ($result['type']) {
			    case "multiple-choice":
					
					//topics are stored as a list		
					$topics=$result['topics'];
					$prefix = '';
					$topic_id_list='';
					
					foreach($topics as $topic) {
									
						$topic_id_list .=$prefix . $topic['id'];
						$prefix = ',';
						$mychapter=$topic['chapter'];
						
					}
					
					$question_id = $result['id'];
					$today = date("Y-m-d H:i:s");
					//setup data array to send for update
					$data=array(
						
						'topic_id' => $topic_id_list,
						'chapter_id'=>$mychapter,
						'question' => $result['question'],
						'deprecated' => $result['deprecated'],
						'updatedOn' => $today,
						'updatedBy'=>$_COOKIE["username"],
						'createdOn' => $result['createdOn']
					
					);	
					$update_result=$this->testcenter_model->update_mc_question($data, $result['id']);
					
					//update answers
					foreach($result['choices'] as $choice) {
						
						$data_choice['question_id']=$question_id;
						$data_choice['choice']=$choice['choice'];
						$data_choice['explanation']=$choice['explanation'];
						$data_choice['is_correct']=$choice['is_correct'];
						
						$answer=$this->testcenter_model->update_mc_answers($data_choice, $choice['id']);
						
						//if this is the correct answer update the questions table
						if ($data_choice['is_correct']==1) {
							$correct['answer_id']=$choice['id'];
							$correct['explanation']=$data_choice['explanation'];
							$a=$this->testcenter_model->set_correct_answer($correct, $data_choice['question_id']);
							
						}
						
						}
					
			        
			        break;
			   
			    //simulation
			    case "simulation":
			        
			        break;
			    
			}
			
			
			
			$this->response($update_result, 200);
			
		} else {
			$this->response(array('error' => 'no question object in post or some other error'), 400);
		}
		
		
	}


	//get list of questions for edit
	public function list_questions_get()
	{
			
		//if an id is passed, just get that one question for edit
			
		$id=$this->get('edit');
		
		if (!$id) {
			$id=0;
		}
		
		$this->load->model('testcenter_model');
		
		//returns the latest 25 questions
		$questions = $this->testcenter_model->get_questions($id);
		
        if(!empty($questions))
        {
            	
			//format the db results into an array like views/topic.json
            $myquestions=array();
			$current_question=NULL;   
			
			$i=0;
            
            foreach ($questions as $question) {
            	
					//get the document info if it is recorded
					$document_url='';
					$isFlagged=0;
					if (is_numeric($question['docID'])) {
						
						$doc = $this->testcenter_model->get_document($question['docID']);
						if ($doc) {$document_url=$doc[0]['url']; $isFlagged=$doc[0]['isFlagged'];}
						
					}

					$topic_data=$this->testcenter_model->get_topics_ids($question['topic_id']);
            	
					$myquestions['questions'][]=array(
						'question'=>$question['question'],
						'id'=>$question['id'],
						'difficulty'=>$question['difficulty'],
						/*'topics'=>array(
							'topic_id'=>$question['topic_id'],
							'chapter_id'=>$question['chapter_id']							
							),*/
						'topics'=>$topic_data,
						'document'=>array(
							'url'=>$document_url,
							'id'=>$question['docID'],
							'isFlagged'=>$isFlagged
						),
						'deprecated'=>$question['deprecated'],
						'createdOn'=>$question['createdOn'],
						'createdBy'=>$question['createdBy'],
						'updatedOn'=>$question['updatedOn'],
						'updatedBy'=>$question['updatedBy'],
						'type'=>'multiple-choice'
						);
						
											
					
					//get answers for this questionID
					$choices=$this->testcenter_model->get_quizlets_answers($question['id']);
					//add in sub topics
					foreach($choices as $choice){
						
							
							$myquestions['questions'][$i]['choices'][]=array(
							
							'choice'=>$choice['choice'], 
							'id'=>$choice['id'], 
							'explanation'=>$choice['explanation'], 
							'is_correct'=>$choice['is_correct']);
							
						}
					
					$i++;				
					
				
			    
			}	
				
				
            $this->response($myquestions, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any questions!', 'id'=>$id), 200);
        }
	}

	//delete question and associated answers
	public function delete_question_get()
	{
			
		$this->load->model('testcenter_model');
			
		$questionID=$this->input->get('q');
		
		$result = $this->testcenter_model->delete_question($questionID);
		
		$this->response($result, 200);		
		
	}
	
	//delete tbs question
	public function delete_tbs_question_get()
	{
		$this->load->model('testcenter_model');
			
		$questionID=$this->input->get('id');
		
		$result = $this->testcenter_model->delete_tbs_question($questionID);
		
		$this->response($result, 200);	
	}
	
	//delete a single answer or choice
	public function delete_choice_get()
	{
			
		$id=$this->get('id');
		
		if (!$id) {
			$this->response(array('error' => 'Choice id required!'), 412); //Precondition Failed
		}
			
		$this->load->model('testcenter_model');
			
		$result = $this->testcenter_model->delete_choice($id);
		
		$this->response($result, 200);		
		
	}
	
	public function question_ids_get()
	{
		$username=$this->get('username');
		
		if (!$username) {
			$this->response(array('error' => 'username required!'), 412); //Precondition Failed
		}
			
		$this->load->model('testcenter_model');
			
		$result = $this->testcenter_model->get_question_ids($username);
		
		$questions=array('questions'=>$result);
		
		$this->response($questions, 200);		
	}
	
	//add a single choice from the edit screen
	public function set_choice_post()
	{
			
		$this->input->post(); // returns all POST items without XSS filter 	
			
		$result=$this->post();
		
		//var_dump($result);
		//exit;
		
		//no json return the error
		if (!$result) {
			$this->response(array('error' => 'Improper or missing json'), 412); //Precondition Failed
		}
		
		//insert answers
		$data=array(
		
			'is_correct'=>$result['choice']['is_correct'],
			'explanation'=>$result['choice']['explanation'],
			'choice'=>$result['choice']['choice'],
			'question_id'=>$result['questionID']
		
		);
		
		
			
		$this->load->model('testcenter_model');
			
		//returns the new id
		$answer = $this->testcenter_model->set_answer($data);
		
		$choice=array(
		
			'id'=>$answer['answer_id'],
			'is_correct'=>$result['choice']['is_correct'],
			'explanation'=>$result['choice']['explanation'],
			'choice'=>$result['choice']['choice'],
		
		);
		
		//application needs the answer in an array called choice
		$mychoice=array(
		
			'choice'=>$choice,
		
		);
		
		$this->response($mychoice, 200);		
		
	}

	public function content_document_get() 
	{
		$this->load->model('testcenter_model');
		$student = $this->testcenter_model->get_student($_COOKIE['studentid']);
		$docID=$student[0]['docID'];
		$data = $this->testcenter_model->get_document($docID);
		if(!$data) {
			$data = array(
				'error' => 'No document assigned to user'
			);
		}
		$this->response($data, 200);
	}

	public function save_tbs_question_post()
	{
		$this->input->post(); // returns all POST items without XSS filter 	
			
		$result=$this->post();
		
		//convert the post back into json
		$question_json = json_encode($result);
		
		//no json return the error
		if (!$result) {
			$this->response(array('error' => 'Improper or missing json'), 412); //Precondition Failed
		}


		$this->load->model('testcenter_model');
        
        date_default_timezone_set('America/Los_Angeles');
		
		$today = date("Y-m-d H:i:s");
		
		//get which doc the admin is currently using
		$student = $this->testcenter_model->get_student($_COOKIE['studentid']);
		$docID=$student[0]['docID'];
		
		//topics are stored as a list
		$topics=$this->post('topics', FALSE);
        //$topics=$result['topics'];
		//$topics=json_decode($topics, true);
		$prefix = '';
		$topic_id_list=array();
		
		foreach($result['topics'] as $topic) {
						
			$topic_id_list[]=$topic['id'];
			$mychapter=$topic['chapterID'];
			
		}
        $topic_ids=implode(",", $topic_id_list);
        
        		
		//insert tbs question data
		$data=array(
		
			'topic_id'=>$topic_ids,
			'chapter_id'=>$mychapter,
			'question_json'=>$question_json,
			'deprecated'=>$result['deprecated'],
			'entered_by_studentid'=>$_COOKIE["studentid"],
			'docID'=>$docID,
			'type'=>$this->post('type', FALSE),
			'researchType'=>$this->post('researchType', FALSE),
			'createdBy'=>$_COOKIE["username"],
			'createdOn'=>$today
					
		);		
			
		//returns the new id
		$id = $this->testcenter_model->save_tbs_question($data);
		$data['success']=1;
		$data['id']=$id['id'];
		$this->response($data, 200);
	}


	public function update_tbs_question_post()
	{
		$this->input->post(); // returns all POST items without XSS filter 	
			
		$result=$this->post();
		
		//no json return the error
		if (!$result) {
			$this->response(array('error' => 'Improper or missing json'), 412); //Precondition Failed
		}
		
		//topics are stored as a list		
		$topics=$result['topics'];
		$prefix = '';
		$topic_id_list='';
		
		foreach($topics as $topic) {
						
			$topic_id_list .=$prefix . $topic['id'];
			$prefix = ',';
			$mychapter=$topic['chapter'];
			
		}
				
		//convert the post back into json
		$question_json = json_encode($result);
		
				
		$this->load->model('testcenter_model');
		
		$today = date("Y-m-d H:i:s");
		
		//get which doc the admin is currently using
		$student = $this->testcenter_model->get_student($_COOKIE['studentid']);
		$docID=$result['docID'];//$student[0]['docID'];
		
				
		//format tbs question data
		$data=array(
		
			'topic_id'=>$topic_id_list,
			'chapter_id'=>$mychapter,
			'question_json'=>$question_json,
			'entered_by_studentid'=>$_COOKIE["studentid"],
			'docID'=>$docID,
			'deprecated'=>$result['deprecated'],
			'type'=>$result['type'],
			'updatedBy'=>$_COOKIE["username"],
			'updatedOn'=>$today
					
		);		
			
		//get the id
		$id=$result['id'];
		$updated = $this->testcenter_model->update_tbs_question($data,$id);
		$data['success']=1;
		$this->response($data, 200);
	}

	
	public function search_mc_questions_get()
	{
			
		//no search string forget it
		if(!$this->get('search'))
        {
        	$this->response(NULL, 400);
        }
		
		$search=$this->get('search');
		
		$this->load->model('testcenter_model');
		
		//returns the latest 25 questions
		$questions = $this->testcenter_model->search_questions_answers($search);
		
				
        if($questions)
        {
            	
			//format the db results into an array like views/topic.json
            $myquestions=array();
			$current_question=NULL;   
			
			$i=0;
            
            foreach ($questions as $question) {
            	
					//get the document info if it is recorded
					$document_url='';
					$isFlagged=0;
					if (is_numeric($question['docID'])) {
						
						$doc = $this->testcenter_model->get_document($question['docID']);
						if ($doc) {$document_url=$doc[0]['url']; $isFlagged=$doc[0]['isFlagged'];}
						
					}
            	
					$myquestions['questions'][]=array(
						'question'=>$question['question'],
						'id'=>$question['question_id'],
						'difficulty'=>$question['difficulty'],
						'topics'=>array(
							'topic_id'=>$question['topic_id'],
							'chapter_id'=>$question['chapter_id']							
							),
						'document'=>array(
							'url'=>$document_url,
							'id'=>$question['docID'],
							'isFlagged'=>$isFlagged
						),
						'createdOn'=>$question['createdOn'],
						'createdBy'=>$question['createdBy'],
						'updatedOn'=>$question['updatedOn'],
						'updatedBy'=>$question['updatedBy'],
						'type'=>'multiple-choice'
						);
						
											
					
					//get answers for this questionID
					$choices=$this->testcenter_model->get_quizlets_answers($question['question_id']);
					//add in sub topics
					foreach($choices as $choice){
						
							
							$myquestions['questions'][$i]['choices'][]=array(
							
							'choice'=>$choice['choice'], 
							'id'=>$choice['id'], 
							'explanation'=>$choice['explanation'], 
							'is_correct'=>$choice['is_correct']);
							
						}
					
					$i++;				
					
				
			    
			}	
				
				
            $this->response($myquestions, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any questions!', 'id'=>$id), 404);
        }
	}

	//get typs questions by id, or type, or all
	public function tbs_questions_get()
	{
		//get the question type and username. Default is "none"
		$type = (!$this->get('type')) ? 'none' : $this->get('type');
		$username = (!$this->get('username')) ? 'none' : $this->get('username');
		
				//check for an id. Default is 0
		$id = (!$this->get('id')) ? 0 : $this->get('id');
		
		$this->load->model('testcenter_model');		
		
		$questions = $this->testcenter_model->get_tbs_questions($id,$type,$username);
		
		
		
				
		foreach ($questions as $question) {
			
			if (isset($question['topic_id']) and $question['topic_id'] != '') {
			
			$topic_data=$this->testcenter_model->get_topics_ids($question['topic_id']);

			//get the document info if it is recorded
			$document_url='';
			$isFlagged=0;
			if (is_numeric($question['docID'])) {
				
				$doc = $this->testcenter_model->get_document($question['docID']);
				if ($doc) {$document_url=$doc[0]['url']; $isFlagged=$doc[0]['isFlagged'];}
				
			}
			
			$tbs_questions[]=array(
				'question_json'=>$question['question_json'],
				'topic_id'=>$question['topic_id'],
				'chapter_id'=>$question['chapter_id'],
				'entered_by_studentid'=>$question['entered_by_studentid'],
				'docID'=>$question['docID'],
				'document'=>array(
							'url'=>$document_url,
							'id'=>$question['docID'],
							'isFlagged'=>$isFlagged
						),
				'type'=>$question['type'],
				'deprecated'=>$question['deprecated'],
				'researchType'=>$question['researchType'],
				'createdBy'=>$question['createdBy'],
				'createdOn'=>$question['createdOn'],
				'id'=>$question['id'],
				'topics'=>$topic_data
				
				
				
			);	
			
			}
		}
		
		
		$this->response($tbs_questions, 200); // 200 being the HTTP response code
		
		
	}
	
	public function test_auth_get()
	{
		
		
		$this->load->helper('myhelpers');
		$this->load->model('testcenter_model');	
		$sid=get_sid();
		$student = $this->testcenter_model->get_student($sid);	
		
		$access = is_authorized($student, 1);
			
		$this->response($access, 200); // 200 being the HTTP response code
	}
	
	//return a list of sections the student is authorized to view
	public function section_auth_get()
	{
		$this->load->helper('myhelpers');
		$this->load->model('testcenter_model');	
		$sid=get_sid();
		$student = $this->testcenter_model->get_student($sid);		
		$roles=get_sections($student);
		$this->response($roles, 200);
		
	}
	
	//issue report from students
	public function issue_report_post()
	{
			
		
		
		if ($this->post('issue')) {
			
			$mc_question_id=0;
			$tbs_question_id=0;
			$this->load->model('testcenter_model');
			$this->load->helper('myhelpers');
			
			$issue=$this->post('issue');
			//$student_id=get_cf_student_id();
			$sid=get_sid();
			
			
			
			//get the mc or tbs question id
			if (isset($issue['type']) && $issue['type']=='multiple-choice') {
				
				$mc_question_id = $issue['question'];
				$edit_url = 'http://testcenter.rogercpareview.com/testmodule-admin/search-edit.php#/question/multiple-choice/edit/';
				
			} else {
				
				$tbs_question_id = $issue['question'];
				$edit_url = 'http://testcenter.rogercpareview.com/testmodule-admin/build-widget-question.php#/edit/';
			}

			//setup data array to send for insert
			$data=array(
				
				'mc_question_id' => $mc_question_id,
				'tbs_question_id' => $tbs_question_id,
				'student_id' => $sid,
				'description' => $issue['description'],
				'browser' => $_SERVER['HTTP_USER_AGENT']
			
			);
			
			$result = $this->testcenter_model->set_issue_report($data);
			
			//send an email
			$student = $this->testcenter_model->get_student($sid);
			
			$config = Array(
				    'protocol' => 'smtp',
				    'smtp_host' => 'pod51011.outlook.com',
				    'smtp_crypto' => 'tls',
				    'smtp_port' => 587,
				    'smtp_user' => 'staff@rogercpareview.com',
				    'smtp_pass' => 'R0gPe0ple!',
				    'mailtype'  => 'html'
				);
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->set_crlf( "\r\n" );
				
				
				$this->email->from('staff@rogercpareview.com', 'RCPAR Testcenter');
				if ($issue['reason'] == 'tech') {
					$this->email->to('dthomasmenter@rogercpareview.com,csteyer@rogercpareview.com');
				}
				else if ($issue['reason'] == 'content') {
					$this->email->to('dthomasmenter@rogercpareview.com,csteyer@rogercpareview.com');
				}
				else {
					$this->email->to('dthomasmenter@rogercpareview.com,csteyer@rogercpareview.com');
				}
				
				
							
				$msg = array(
				
					'email_line'=>$student[0]['email'],
					'question_type'=>$issue['type'],
					'edit_url'=>$edit_url . $issue['question'],
					'question_id'=>$issue['question'],
					'browser'=>$_SERVER['HTTP_USER_AGENT'],
					'description'=>$issue['description']
				
				);
							
				
							
							
				$this->email->subject('RCPAR Testcenter Issue Report');
				$message = $this->load->view('templates/email', $msg, true);
				$this->email->message($message);
				$this->email->send();
			
			
			$this->response($data, 200);
			
		} else {
			$this->response(array('error' => 'no issue in post or some other error'), 400);
		}
		
		
	}
	
	
	public function questions_by_id_post()
	{
		$questions=$this->post('questions');
		
		
		
		$mcqs=array();
		$tbsqs=array();
		$results=array();
		$i=0;
		$mcq_ids='';
		$tbs_ids='';
		
		//return a list of mcq IDs
		if (isset($questions['multipleChoice'])) {
			$mcq_ids=implode(",", $questions['multipleChoice']);
		}
		if (isset($questions['tbs'])) {
			$tbs_ids=implode(",", $questions['tbs']);
		}
		
		$this->load->helper('myhelpers');
		$this->load->model('testcenter_model');	
		
		
		
		if ($mcq_ids != '') {
			
			$mcqs = $this->testcenter_model->get_mc_by_ids($mcq_ids);
			
			foreach ($mcqs as $question) {
			$quizlet_answers = $this->testcenter_model->get_quizlets_answers($question['id']);
								
			//list out the choices				
			foreach ($quizlet_answers as $answer) {	
				
					
					$mcqs[$i]['choices'][]=array(
					'choice'=>$answer['choice'],
					'explanation'=>$answer['explanation'],
					'id'=>$answer['id'],
					'is_correct'=>$answer['is_correct']
					);
				
				
			}
			//get topic info
			//we need the videoID from CF to form the course url
			$topic_data = $this->testcenter_model->get_topics_ids($question['topic_id']);
			
			$topic_array = array();
			
			//topic and chapter in its own array
			foreach ($topic_data as $topic_row) {
				
				$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
				
				$chap_title=$topic_row['chapter'];
				
				$topic_array[]=array(
				'topic_id'=>$topic_row['topic_id'],
				'topic'=>$topic_row['topic'],
				'chapter_id'=>$topic_row['chapter_id'],
				'course_url'=>$course_url
				);
				
			}
			$mcqs[$i]['chapter']=$chap_title;
			$mcqs[$i]['topics']=$topic_array;
			
			
			$i++;
		}
			
		}
		
		if ($tbs_ids != '') {
			
			$i=0;
			
			$tbsqs = $this->testcenter_model->get_tbs_by_ids($tbs_ids);
			
			foreach ($tbsqs as $question) {
			
			//get topic info
			//we need the videoID from CF to form the course url
			$topic_data = $this->testcenter_model->get_topics_ids($question['topic_id']);
			
			$topic_array = array();
			
			//topic and chapter in its own array
			foreach ($topic_data as $topic_row) {
				
				$chap_title=$topic_row['chapter'];
				
				$course_url = array_key_exists('drupal_topic_id', $topic_row) ? get_course_url($topic_row['drupal_topic_id']) : '';
								
				$topic_array[]=array(
				'topic_id'=>$topic_row['topic_id'],
				'topic'=>$topic_row['topic'],
				'chapter_id'=>$topic_row['chapter_id'],
				'course_url'=>$course_url
				);
			}
			$tbsqs[$i]['chapter']=$chap_title;
			$tbsqs[$i]['topics']=$topic_array;
			
			
			$i++;
		}
			
			
			
		}
		
		$results['questions']['multipleChoice']=$mcqs;
		$results['questions']['tbs']=$tbsqs;
		
		$this->response($results, 200);
		
	}
	
	public function delete_session_get()
	{
		$this->load->model('testcenter_model');
			
		$session_id=$this->input->get('id');
		
		$result = $this->testcenter_model->delete_session($session_id);
		
		$result = $this->testcenter_model->delete_session_details($session_id);
		
		$this->response($result, 200);	
	}
	
	public function update_session_post()
	{
		//check the results
		$this->input->post(); // returns all POST items without XSS filter
		$results=$this->post();
		if (isset($results['id'])) {
			$id=$results['id'];
					
			//dump the entire post as a json object into the summary field
			$summary = json_encode($results);
			
			$data=array(
			
				'summary'=>$summary
			);
	/*
			$student_progress=array(
				'student_id'=>$results['session']['summary']['studentId'],
				'chapter_scores'=>$results['session']['summary']['chapterScores']
			);
	*/
			//if all good, lets save this puppy
			$this->load->model('testcenter_model');
			$result = $this->testcenter_model->update_quizlet_session($data, $id);		
			$this->response(array('success'=>'true'), 200);
		}
		else {
			$this->load->model('testcenter_model');
			$chapters = $results['chapters'];
			$student_progress = array(
				'student_id'=>$results['student_id'],
				'chapter_scores'=>$chapters
			);
/*
		$student_progress=array(
			'student_id'=>'0',
			'chapter_scores'=>array(
				'436'=>array(
					'score'=>'0',
					'mcq'=>array(
						'chapter_id'=>436,
						'student_id'=>0,
						'type'=>'mcq',
						'score'=>'0',
						'incorrect'=>'[]',
						'correct'=>'[]',
						'skipped'=>'[]',
						'noted'=>'[]',
						'flagged'=>'[]'
					)
				)
			)
		);
*/
		$spr = $this->testcenter_model->set_student_progress($student_progress);
			$this->response(array('success'=>'true', 'student_progress'=>$spr), 200);
		}
		
	}
	
	//get the question ids from saved quiz sessions so that we don't show them again
	public function quiz_question_history_get()
	{
		
		$this->load->model('testcenter_model');
		$this->load->helper('myhelpers');
		$sid=get_sid();
		$session = $this->testcenter_model->get_saved_sessions($sid);	
		
		
		
		$ids=get_saved_question_ids($session);
		
		$this->response($ids, 200);	
		
		
	}
		/**
		 * get summary quiz data by chapter
		 * @param section required
		 */
		public function get_chapter_info_get() {
			$section = $this->get()['section'];
			$this->load->model('testcenter_model');
			$this->load->helper('myhelpers');
			$sid=get_sid();
			//session_type is either quiz or exam
			$session_type='quiz';
			$chapter_ids=array();			
								
			$chapter_counts = $this->testcenter_model->get_num_questions_by_chapter_v2($section);
            $journal_questions = $this->testcenter_model->get_journal_by_section($section);
						
			$summary['counts']=$chapter_counts;	
			
			//extract chapter ids
			foreach ($chapter_counts['combined'] as $key => $value) {
				$chapter_ids[]=$key;
			}
			$chapter_list=implode(',', $chapter_ids);
			
			$quiz_data = $this->testcenter_model->get_quiz_data_by_chapter($chapter_list,$sid,$session_type);
			$section_overview = $this->testcenter_model->get_section_overview($sid);
            
			
					
			//setup totals variables
			$total_mcq['correct']=0;
			$total_mcq['incorrect']=0;
			$total_mcq['skipped']=0;
			$total_mcq['noted']=0;
			$total_mcq['flagged']=0;
			
			$total_tbs['correct']=0;
			$total_tbs['incorrect']=0;
			$total_tbs['skipped']=0;
			$total_tbs['noted']=0;
			$total_tbs['flagged']=0;
			
			$mcq_qids=array();
			$tbs_qids=array();
			
			//get summary data for each chapter id
			foreach ($chapter_ids as $chapter_id) {
			    
                //count the number research for each chapter
                $journal_total_available=array();
                $journal_qids=array();
                $journal_unanswered=array();
                $journal_incorrect=array();
                if (!empty($journal_questions)) {
                    foreach ($journal_questions as $journal_question) {
                        if ($journal_question['chapter_id']==$chapter_id) {
                            $journal_total_available[]=$journal_question['question_id'];
                            $journal_qids[]=$journal_question['question_id'];
                            $journal_unanswered[]=$journal_question['question_id'];
                            
                        }
                    }
                    
                }
				
				//setup mcq and tbs variables
								
				$mcq['correct']=0;
				$mcq['incorrect']=0;
				$mcq['skipped']=0;
				$mcq['noted']=0;
				$mcq['flagged']=0;
				//total questions
				if (isset($chapter_counts['mcq'][$chapter_id])) {
					$mcq['unanswered']=$chapter_counts['mcq'][$chapter_id];
				} else {
					$mcq['unanswered']=0;
				}
				
				
				$tbs['correct']=0;
				$tbs['incorrect']=0;
				$tbs['skipped']=0;
				$tbs['noted']=0;
				$tbs['flagged']=0;
				if (isset($chapter_counts['tbs'][$chapter_id])) {
					$tbs['unanswered']=$chapter_counts['tbs'][$chapter_id];
				} else {
					$tbs['unanswered']=0;
				}
			
				foreach ($quiz_data as $quiz) {
					
					//question ids need to be unique
						
					//mcq's first
					if ($quiz['type']=='mcq' && $quiz['chapter_id']==$chapter_id && !in_array($quiz['question_id'], $mcq_qids)) {
						
						//look for correct and incorrect from the section overview table
						$i=search_for_qid($quiz['question_id'], 'mcq', $section_overview);
												
						//if the chapter id's match, total up the correct and incorrect
						if ($i!==FALSE && $section_overview[$i]['chapter_id']==$quiz['chapter_id'] ) {
							
							switch ($section_overview[$i]['status']) {
							   
							    case "correct":
							        
									if ($mcq['unanswered'] > 0) {
							        $mcq['correct']=$mcq['correct']+1;
									//take one away from the unanswered pool
									$mcq['unanswered']=$mcq['unanswered']-1;
									$total_mcq['correct']=$total_mcq['correct']+1;
									}
							        break;
							    
							    case "incorrect":
									
									if ($mcq['unanswered'] > 0) {
							        $mcq['incorrect']=$mcq['incorrect']+1;
									//take one away from the unanswered pool
									$mcq['unanswered']=$mcq['unanswered']-1;
									$total_mcq['incorrect']=$total_mcq['incorrect']+1;     
									}
							        break;
								}
							    
							}
						
						
						
						
						//look for noted
						if ($quiz['noted'] && $quiz['noted'] !='[]' && $quiz['noted'] !=''){
							$mcq['noted']=$mcq['noted']+1;
							$total_mcq['noted']=$total_mcq['noted']+1;
						}
						//look for flagged
						if ($quiz['flagged']==1){
							$mcq['flagged']=$mcq['flagged']+1;
							$total_mcq['flagged']=$total_mcq['flagged']+1;
						}
						$mcq_qids[]=$quiz['question_id'];
					}
					
					//tbs time!
					if ($quiz['type']=='tbs' && $quiz['chapter_id']==$chapter_id && !in_array($quiz['question_id'], $tbs_qids)) {
						
						//look for correct and incorrect from the section overview table
						$i=search_for_qid($quiz['question_id'], 'tbs', $section_overview);
												
						//if the chapter id's match, total up the correct and incorrect
						if ($i!==FALSE && $section_overview[$i]['chapter_id']==$quiz['chapter_id']) {
							
							switch ($section_overview[$i]['status']) {
							   
							    case "correct":
							        
									if ($tbs['unanswered'] > 0) {
							        $tbs['correct']=$tbs['correct']+1;
									//take one away from the unanswered pool
									$tbs['unanswered']=$tbs['unanswered']-1;
									$total_tbs['correct']=$total_tbs['correct']+1;
                                    
                                        //remove from unanswer questions array
                                        if (is_journal($quiz['question_id'], $journal_qids)) {
                                            $journal_unanswered=array_delete($journal_unanswered, $quiz['question_id']);
                                        }
                                    
									}
							        break;
							    
							    case "incorrect":
									
									if ($tbs['unanswered'] > 0) {
							        $tbs['incorrect']=$tbs['incorrect']+1;
									//take one away from the unanswered pool
									$tbs['unanswered']=$tbs['unanswered']-1;
									$total_tbs['incorrect']=$total_tbs['incorrect']+1;
                                    
                                         //remove from unanswer questions array
                                        if (is_journal($quiz['question_id'], $journal_qids)) {
                                            $journal_unanswered=array_delete($journal_unanswered, $quiz['question_id']);
                                            $journal_incorrect[]=$quiz['question_id'];
                                        }
                                    
                                    
                                    
									}
							        break;
								}
							    
							}
						
						//look for noted
						if ($quiz['noted'] && $quiz['noted'] !='[]' && $quiz['noted'] !=''){
							$tbs['noted']=$tbs['noted']+1;
							$total_tbs['noted']=$total_tbs['noted']+1;
						}
						//look for flagged
						if ($quiz['flagged']==1){
							$tbs['flagged']=$tbs['flagged']+1;
							$total_tbs['flagged']=$total_tbs['flagged']+1;
						}
                        //look for research
                        
						//keep tbs q's unique
						$tbs_qids[]=$quiz['question_id'];
					}
					
					
				}

                //format the array
                $unanswered_journal=array();
                foreach ($journal_unanswered as $j_unanswered) {
                    $unanswered_journal[]=$j_unanswered;
                    
                }		
                $journal_unanswered=$unanswered_journal;
				$chapter[$chapter_id]['mcq']=$mcq;
				$chapter[$chapter_id]['tbs']=$tbs;	
                $chapter[$chapter_id]['journal_total_available']=array_merge(array_unique($journal_total_available), array());
                $chapter[$chapter_id]['journal_incorrect']=array_merge(array_unique($journal_incorrect), array());
                $chapter[$chapter_id]['journal_unanswered']=array_merge(array_unique($journal_unanswered), array());
                			
			}

			//total up the number of answered questions
			$total_mcq['answered']=$total_mcq['incorrect']+$total_mcq['correct'];
			$total_mcq['total_available_questions']=array_sum($chapter_counts['mcq']);
			$total_mcq['unanswered']=$total_mcq['total_available_questions']-$total_mcq['answered'];
			
			$total_tbs['answered']=$total_tbs['incorrect']+$total_tbs['correct'];
			$total_tbs['total_available_questions']=array_sum($chapter_counts['tbs']);
			$total_tbs['unanswered']=$total_tbs['total_available_questions']-$total_tbs['answered'];

			$summary['scores']=$chapter;
			$summary['scores']['total']['mcq']=$total_mcq;
			$summary['scores']['total']['tbs']=$total_tbs;
			
			$this->response($summary, 200);
	}
	
	
	

	public function number_of_questions_post()	
	{
		$topic_ids=$this->post();
		$topic_ids = implode(',', $topic_ids['t']);
		
		$this->load->model('testcenter_model');
		
		$no_mcqs = $this->testcenter_model->get_num_mcq_by_topic($topic_ids);
		$no_tbs = $this->testcenter_model->get_num_tbs_by_topic($topic_ids);
//		$chapter_score = $this->testcenter_model->get_chapter_scores($this->post()['student_id']);
		
		$result = array_merge($no_mcqs, $no_tbs);
		
		//don't cache this
		header("Cache-Control: no-store, no-cache, must-revalidate");	
		$this->response($result, 200);	
		
	}

	public function search_questions_by_id_get() {
		//no search string forget it
		if(!$this->get('search')) {
			$this->response(NULL, 400);
		}
		$ids = $this->get('search');
		$this->load->model('testcenter_model');
		$this->load->helper('myhelpers');
		
		$tbs = $this->testcenter_model->get_tbs_by_ids($ids);
		$mcq = $this->testcenter_model->get_mc_by_ids($ids);

		$result['mcq'] = $mcq;
		$result['tbs'] = $tbs;
	
		$this->response($result, 200);
	}
	
	public function search_all_questions_get()
	{
			
		//no search string forget it
		if(!$this->get('search'))
        {
        	$this->response(NULL, 400);
        }
		
		$search=$this->get('search');
		$this->load->model('testcenter_model');
		$this->load->helper('myhelpers');

		$result['results']['mcq']=array();
		$result['results']['tbs']=array();
		$result['results']['research']=array();
		$result['results']['wc']=array();
		
		$tbsresearch=array();
		$tbsjournal=array();
		$tbswc=array();
		
		//returns the latest 25 questions
		$mcqs = $this->testcenter_model->search_questions_answers($search);
		
		//json in db is url encoded so let's do the same to our search string
		$search=encode_tbs_search_string($search);
		
		$tbs = $this->testcenter_model->search_tbs_questions($search);
		
		//$this->response($mcqs, 200);
		
				
        if($mcqs)
        {
            	
			$result['results']['mcq']=$mcqs;
        }
		
		if($tbs)
        {
            
			foreach ($tbs as $tbs_array) {
				
				
				switch ($tbs_array['type'])
				{
				case "tbs-research":
				  $tbsresearch[]=$tbs_array;
					break;
				case "tbs-wc":
				  $tbswc[]=$tbs_array;
					break;
				default:
				  $tbsjournal[]=$tbs_array;
				}
				
				
				
			}
				
			$result['results']['tbs']=$tbsjournal;
			$result['results']['research']=$tbsresearch;
			$result['results']['wc']=$tbswc;
        }

        $this->response($result, 200);
	}

	public function questions_by_topic_post()	
	{
		$topics=$this->post('topicids');		
		
		$mcqs=array();
		$tbsqs=array();
		$questions=array();
		$i=0;
		$mcq_ids='';
		$tbs_ids='';
		$type='mcq';
		
		//return a list of mcq IDs
		if (isset($topics['multipleChoice'])) {
			$mcq_ids=implode(",", $topics['multipleChoice']);
		}
		if (isset($topics['tbs'])) {
			$tbs_ids=implode(",", $topics['tbs']);
		}
		
		
		$this->load->model('testcenter_model');	

		$mc_results= array();
		$tbs_results = array();
		
		if ($mcq_ids != '') {
			
			$mc_results = $this->testcenter_model->get_mc_by_topicids($mcq_ids,$type);			
			
		}
		if ($tbs_ids != '') {
			$tbs_results = $this->testcenter_model->get_tbs_by_topicids($tbs_ids,'tbs');
		}
		$myquestions = array();
		if ($mc_results){
			
			foreach ($mc_results as $result) {
				
				$mc_questions[]=array(	
								'id'=>$result['id'],			
								'question'=>$result['question']
								);
				
			}
			
			$myquestions['mcq'] = array();
			$myquestions['mcq']['questions'] = $mc_questions;
			//var_dump($questions);
			//exit;
		
		} 
		if ($tbs_results) {
			foreach ($tbs_results as $result) {
				if ($result['type'] == 'tbs-wc') {	
					$tbs_questions[]=array(	
									'id'=>$result['id'],			
									'question'=>json_decode(urldecode(json_decode($result['question_json'], true)['question']), true)['question']['question']
									);
				}
				else {
					$tbs_questions[]=array(	
									'id'=>$result['id'],			
									'question'=>json_decode(urldecode(json_decode($result['question_json'], true)['question']), true)['question']['widgets'][0]['model']['content']
									);
				}
				
			}
			
			

			$myquestions['tbs']= array();
			$myquestions['tbs']['questions'] = $tbs_questions;


		}
		$this->response($myquestions, 200);	
		
	}



	public function relaunch_quiz_get(){
		
		if(!$this->get('session_id'))
        {
        	$this->response(array('error' => 'No session_id passed in url'), 400);
        }
		$session_id=$this->get('session_id');
		
		$this->load->helper('myhelpers');
		$student_id=get_sid();
		
		
		$this->load->model('testcenter_model');	
		$results = $this->testcenter_model->get_saved_quiz($student_id, $session_id);
		
		//get the ids
		$session_ids='';
		$prefix='';
		foreach ($results as $result) {
			$session_ids .=$prefix . $result['id'];
			$prefix = ',';
		}
		
		$chapter_ids_summary=array();
		
		//get the question scores
		$question_scores=$this->testcenter_model->get_saved_quiz_questions($session_ids,$student_id);
		$mcq_questions=$question_scores['mcq'];
		$tbs_questions=$question_scores['tbs'];
		
		//breakout mcqs
		$sq_mcq_questions = array();
		$mykeys=array();
		if (!empty($mcq_questions)) {
			$i=0;
			foreach ($mcq_questions as $mcq_question) {
				$qs_key = $mcq_question['question_scores_id'];
				
				if(!isset($sq_mcq_questions[$qs_key])) {
					
					// set given answer id
					if($mcq_question['qs_answer_id']) {
						$mcq_question['answer_id'] = $mcq_question['qs_answer_id'];
					} else {
						$mcq_question['answer_id'] = '';
					}
					//id is supposed to be question id
					$mcq_question['id'] = $mcq_question['question_id'];
					//get associated topics and course urls
					$qtopics = array();
					$topics = $this->testcenter_model->get_topics_by_qid($mcq_question['id'], 'mcq');
					$mcq_question['chapter_id']=$topics[0]['chapter_id'];
					foreach ($topics as $topic) {
						$course_url = array_key_exists('drupal_topic_id', $topic) ? get_course_url($topic['drupal_topic_id']) : '';
						$qtopics[] = array(
							'topic' => $topic['topic'],
							'id' => $topic['topic_id'],
							'course_url' => $course_url
						);
					}
					$mcq_question['topics'] = $qtopics;
					//grab the chapter title
					$chapter = $this->testcenter_model->get_chapters($mcq_question['chapter_id']);				
					$mcq_question['chapter'] = $chapter[0]['chapter'];
					// Add choices property
					$mcq_question['choices'] = array();
					//add current mcq
					$sq_mcq_questions[$qs_key] = $mcq_question;
					
				}
				//Add current choices
				$sq_mcq_questions[$qs_key]['choices'][] = array(
					'id'=>$mcq_question['a_id'],
					'question_id'=>$mcq_question['question_id'],
					'choice' => $mcq_question['choice'],
					'explanation'=>$mcq_question['explanation'],
					'is_correct' => $mcq_question['correct_answer_id'] == $mcq_question['a_id'] ? 1 : 0
				);
			}
		}
		
		$saved_quizzes['questions'] = array_values($sq_mcq_questions);
		

		//breakout tbs
		$prev_tbs_id=0;
		$start=TRUE;
		$choices=array();
		$question=array();
		$qtopics=array();
		
		//get tbs if we have em
		if (!empty($tbs_questions)) {		
		foreach ($tbs_questions as $tbs_question) {			
				
				$question=$tbs_question;
				
				//get associated topics and course urls
				$topics=$this->testcenter_model->get_topics_by_qid($tbs_question['qs_id'],'tbs');
				$question['chapter_id']=$topics[0]['chapter_id'];
				
				foreach ($topics as $topic) {
					$course_url = array_key_exists('drupal_topic_id', $topic) ? get_course_url($topic['drupal_topic_id']) : '';
					$qtopics[]=array(
					
						'topic'=>$topic['topic'],
						'id'=>$topic['topic_id'],						
						'course_url'=>$course_url,
					);
				}
				$question['topics']=$qtopics;
				$qtopics=array();			
				$chapter=$this->testcenter_model->get_chapters($question['chapter_id']);
				if($chapter) {
					$question['chapter']=$chapter[0]['chapter'];
				} else {
					$question['chapter'] = '';
				}
				$saved_quizzes['questions'][]=$question;
			}
			
		}
		
		
		//$saved_quizzes['chapters']=$chapter_ids_summary;
		//$saved_quizzes['chapters']['length']=count($chapter_ids_summary);
		
		//loop through the sessions to create the sessions array wrapper
		$sessions=array();
		$i=0;
		$questions=$saved_quizzes['questions'];
		
		
		foreach ($results as $result) {
			
			//first grab the whole row from saved_quizzes
			$sessions[$i]=$result;
			
			//loop through the questions to apply to the sessions par
			foreach ($questions as $question) {
				
				if (isset($question['session_id']) && $question['session_id']==$result['id']) {
					$sessions[$i]['questions'][]=$question;
					
					//build out chapter ids array
					if (!in_array($question['chapter_id'], $chapter_ids_summary)) {
					$chapter_ids_summary[]=$question['chapter_id'];		
					}
					
				} 
				
			}
			$sessions[$i]['chapters']=$chapter_ids_summary;
			$sessions[$i]['chapters']['length']=count($chapter_ids_summary);
			$chapter_ids_summary=array();
			$i++;
		}
		
		unset($saved_quizzes['questions']);
		$saved_quizzes['sessions']=$sessions;
		
		
		$this->response($saved_quizzes, 200);	
		
		
	}

		public function get_saved_quizzes_get(){
		
		if(!$this->get('student_id'))
        {
        	$this->response(array('error' => 'No student_id passed in url'), 400);
        }
		$student_id=$this->get('student_id');
		$session_id=$this->get('session_id');
		//filter by section - far, aud reg or bec
		$section=(!$this->get('section')) ? 'ALL' : $this->get('section');
		$section=strtoupper($section);
		
		$this->load->model('testcenter_model');	
		$results = $this->testcenter_model->get_saved_quizzes($student_id,$session_id,$section);
		
		//get the ids
		$session_ids='';
		$prefix='';
		foreach ($results as $result) {
			$session_ids .=$prefix . $result['id'];
			$prefix = ',';
		}
		
		$chapter_ids_summary=array();
		
		//get the question scores
		$question_scores=$this->testcenter_model->get_saved_quiz_questions($session_ids,$student_id);
		$mcq_questions=$question_scores['mcq'];
		$tbs_questions=$question_scores['tbs'];
		
		
		//breakout mcqs
		$prev_mcq_id=0;
		$start=TRUE;
		$choices=array();
		$question=array();
		if (!empty($mcq_questions)) {
		foreach ($mcq_questions as $mcq_question) {
			//initialize mcq_id value	
			if ($start==TRUE){
				$prev_mcq_id=$mcq_question['qs_id'];
				$start=FALSE;
			} 
			//break out by question id
			if ($prev_mcq_id==$mcq_question['qs_id']){
				
				$question=$mcq_question;
				//id is supposed to be question id
				$question['id']=$question['question_id'];
				$choices[]=array(
					'id'=>$mcq_question['answer_id'],
					'question_id'=>$mcq_question['question_id'],
					'choice' => $mcq_question['choice'],
					'explanation'=>$mcq_question['explanation']
				);
				
			} else {
				//save and reset variables
				$question['choices']=$choices;
				$saved_quizzes['questions'][]=$question;
				$choices=array();
							
				$question=$mcq_question;
				$choices[]=array(
					'id'=>$mcq_question['answer_id'],
					'question_id'=>$mcq_question['question_id'],
					'choice' => $mcq_question['choice'],
					'explanation'=>$mcq_question['explanation']
				);
			}
			$prev_mcq_id=$mcq_question['qs_id'];
			
			
		}
		}
		//the last foreach needs to be stored
		$question['choices']=$choices;
		$saved_quizzes['questions'][]=$question;
		
		//breakout tbs
		$prev_tbs_id=0;
		$start=TRUE;
		$choices=array();
		$question=array();
		
		//get tbs if we have em
		if (!empty($tbs_questions)) {		
		foreach ($tbs_questions as $tbs_question) {
			//initialize mcq_id value	
			if ($start==TRUE){
				$prev_tbs_id=$tbs_question['qs_id'];
				$start=FALSE;
			} 
			//break out by question id
			if ($prev_tbs_id==$tbs_question['qs_id']){
				
				$question=$tbs_question;
				
				
			} else {
				//save and reset variables
				$saved_quizzes['questions'][]=$question;
										
				$question=$tbs_question;
				
			}
			$prev_tbs_id=$tbs_question['qs_id'];
			
		}
		}
		$saved_quizzes['questions'][]=$question;
		//$saved_quizzes['chapters']=$chapter_ids_summary;
		//$saved_quizzes['chapters']['length']=count($chapter_ids_summary);
		
		//loop through the sessions to create the sessions array wrapper
		$sessions=array();
		$i=0;
		$questions=$saved_quizzes['questions'];
		foreach ($results as $result) {
			
			//first grab the whole row from saved_quizzes
			$sessions[$i]=$result;
			
			//loop through the questions to apply to the sessions par
			foreach ($questions as $question) {
				
				if (array_key_exists('session_id', $question) && $question['session_id']==$result['id']) {
					
					$sessions[$i]['questions'][]=$question;
					
					//build out chapter ids array
					if (!in_array($question['chapter_id'], $chapter_ids_summary)) {
					$chapter_ids_summary[]=$question['chapter_id'];						
					}
					
				}
				
			}
			$sessions[$i]['chapters']=$chapter_ids_summary;
			$sessions[$i]['chapters']['length']=count($chapter_ids_summary);
			$chapter_ids_summary=array();
			$i++;
		}
		unset($saved_quizzes['questions']);
		$saved_quizzes['sessions']=$sessions;
		
		
		$this->response($saved_quizzes, 200);	
		
		
	}

	/**
	 * return quiz summary data
	 */
	public function get_saved_quizzes_summary_get()	{
		
		if(!$this->get('student_id'))
        {
        	$this->response(array('error' => 'No student_id passed in url'), 400);
        }
		$student_id=$this->get('student_id');
		$session_id=$this->get('session_id');
		//filter by section - far, aud reg or bec
		$section=(!$this->get('section')) ? 'ALL' : $this->get('section');
		$section=strtoupper($section);
		
		$this->load->model('testcenter_model');	
		if($page = $this->get('page')) {
			$page = intval($page);
			$quizzes = $this->testcenter_model->get_saved_quizzes_by_page($student_id,$page,$section);
		} else {
			$quizzes = $this->testcenter_model->get_saved_quizzes($student_id,$session_id,$section);
		}
		
		//get the ids
		$session_ids='';
		$prefix='';
		foreach ($quizzes as $quiz) {
			$session_ids .=$prefix . $quiz['id'];
			$prefix = ',';
		}
		
		$summaries = $quizzes ? $this->testcenter_model->get_type_summary($session_ids) : array();
		
		
		
		$sessions=array();
		$quiz_summary=array();
		foreach ($quizzes as $quiz) {
			
			$quizlet=array();
			$completed=TRUE;
			$mcq_qty=0;
			$tbs_qty=0;
			$chapters=array();
			foreach ($summaries as $summary) {
				
				if ($quiz['id']==$summary['session_id']) {
					
					//set completed
					if ($summary['status']=='skipped') {
						$completed=FALSE;
					}
					
					//set mcq quantity
					if ($summary['type']=='mcq'){
						$mcq_qty++;
					} else {
						$tbs_qty++;
					}
					
					//get chapters
					if (!in_array($summary['chapter_id'], $chapters)) {
						$chapters[]=$summary['chapter_id'];
					}
					
				}
								
			}
            
           $chapter_list=implode(",", $chapters);
           $session_chapters=$chapter_list ? $this->testcenter_model->get_multiple_chapters($chapter_list) : array();
           //format like "AUD 1, 2, 3, 4"
           $order=array();
           foreach ($session_chapters as $session_chapter) {
               $chap_section=$session_chapter['section'];
               $order[]=$session_chapter['order'];
           }
           sort($order);
           $chapter_order=implode(", ", $order);
            
			$quizlet[]=array('type'=>'mcq', 'qty'=>$mcq_qty);
			$quizlet[]=array('type'=>'tbs', 'qty'=>$tbs_qty);
			$quiz['quizlets']=$quizlet;
			$quiz['completed']=$completed;
            $quiz['chapters']="<strong>$chap_section:</strong> $chapter_order";
			$quiz['chapters_total']=count($chapters);
			//$quiz_summary[]['quizlets']=$quizlet;
			$quiz_summary[]=$quiz;
			//summary may not have either mcq or tbs
			/*if (count($quizlet) < 2) {
				if ($quizlet[0]['type']=='mcq') {
					$quizlet[]=array('type'=>'tbs', 'qty'=>0);
					} else {
					$quizlet[]=array('type'=>'mcq', 'qty'=>0);	
					}
			}*/
			$sessions['sessions']=$quiz_summary;
			
		}
		
		$this->response($sessions, 200);	
		
	}


	/**
	 * save quiz summary. expects the following json formated object in post
	 * {"student_id":"80","session_name":"REG Quiz January 9th 2014, 12:32:24 pm",
	 * "average_time":"49.2 seconds ","total_percent":"100.0","current_percent":"100.0","attempts_average":"1.2",
	 * "complete_percent":"100","timestamp":"0000-00-00 00:00:00","active":"1"}
	 * 
	 */
	public function save_quiz_post(){
		
		//check the results
		$this->input->post(); // returns all POST items without XSS filter
		$results=$this->post();
		if (!$results['session_name']) {
			
			$this->response(array('error' => 'No json variable passed'), 400);
		}
		$this->load->model('testcenter_model');
		$id = $this->testcenter_model->save_quiz($results);
		$this->response($id, 200);	
		
		
	}
	
	/**
	 * save quiz summary. expects the following json formated object in post
	 * {"id":"80","student_id":"80","session_name":"REG Quiz January 9th 2014, 12:32:24 pm",
	 * "average_time":"49.2 seconds ","total_percent":"100.0","current_percent":"100.0","attempts_average":"1.2",
	 * "complete_percent":"100","timestamp":"0000-00-00 00:00:00","active":"1"}
	 * 
	 */
	public function save_quiz_put(){
		
		//check the results
		$results=$this->put();
		if (!$results['session_name']) {
			
			$this->response(array('error' => 'No json variable passed'), 400);
		}
		
		$this->load->model('testcenter_model');
		$response = $this->testcenter_model->update_quiz($results);
		$this->response($response, 200);	
		
		
	}
	
	/**
	 * DELETE for deleting quizes. Must pass an id
	 * MUST USE THIS TYPE OF URL STUCTURE: 
	 * /restserver/index.php/api/save_quiz/id/4978
	 */
	public function save_quiz_delete(){
		
		//if no id forget it
		$id=$this->get('id');
		if (!$id) {
			
			$this->response(array('error' => 'Must pass id'), 400);
		}		
		$this->load->model('testcenter_model');
		$response = $this->testcenter_model->delete_quiz($id);
		$this->response($response, 200);	
		
		
	}
	/*
	 * get all question_scores based on student_id
	 * student id required session id optional
	 */
	public function get_question_scores_get(){
		    
		$student_id=0;
		if($this->get('student_id'))
        {
        	$student_id=$this->get('student_id');
        }
		//optional session id
		if($this->get('session_id'))
        {
        	$session_id=$this->get('session_id');
        } else {
        	$session_id=0;
        }
		
		$this->load->model('testcenter_model');	
		$results = $this->testcenter_model->get_question_scores($student_id,$session_id);
		$this->response($results, 200);	
		
	}
	
	public function get_attempted_questions_get(){
		    
		
		$student_id=$this->get('student_id');
        $this->load->model('testcenter_model');	
		$results = $this->testcenter_model->get_attempted_questions($student_id);
		
		$cid=0;
		$start=true;
		$i=0;
		$count=0;
		$chapter=array();
		foreach ($results as $result) {
			
			$cid = ($start) ? $cid=$result['chapter_id'] : $cid ;
			$start=FALSE;
			
			if ($result['type']=='mcq') {
				
				$i++;
				if ($result['chapter_id'] == $cid) {
					
					$qids[]=$result['question_id'];
					
					
				} else {
					$chapter[$cid]=$qids;
					$qids=array();
					$qids[]=$result['question_id'];
					$cid=$result['chapter_id'];
					
				}
				
			}
			$summary['mcq']=$chapter;
			$summary['mcq']['total']=$i;
			
			
			if ($result['type']=='tbs') {
				$count++;
				
				if ($result['chapter_id'] == $cid) {
					
					$qids[]=$result['question_id'];
					
				} else {
					$chapter[$cid]=$qids;
					$qids=array();
					$qids[]=$result['question_id'];
					$cid=$result['chapter_id'];
					
				}
				
			}
			$summary['tbs']=$chapter;
			$summary['tbs']['total']=$count;
		}
		
		
		$this->response($summary, 200);	
		
	}
	
	/*
	 * first time save (insert) expects the following format:
	 * 
	 {
        "id":"107",
        "session_id":"270",
        "student_id":"80",
        "quizlet_num":"0",
        "type":"mul",
        "status":"correct",
        "noted":null,
        "flagged":null,
        "answer_id":"[{\"answer\":1,\"buttonID\":\"link_0\",\"givenAnswer\":1,\"keyID\":\"aKey_0\"},{\"answer\":2,\"buttonID\":\"link_2\",\"givenAnswer\":1,\"keyID\":\"aKey_0\"},{\"answer\":1,\"buttonID\":\"link_1\",\"givenAnswer\":2,\"keyID\":\"aKey_0\"}]",
        "score":"0.33",
        "attempts":"[153206]",
        "time":"153.2 seconds "
    }		
	 */
	public function save_question_scores_post(){
		
		//check the results
		$this->input->post(); // returns all POST items without XSS filter
		$results=$this->post();
		if (!$results) {
			
			$this->response(array('error' => 'No json variable passed'), 400);
		}
		$this->load->model('testcenter_model');
		$id = $this->testcenter_model->save_question_scores($results);
		$this->response($id, 200);	
		
		
	}
	
	/*
	 * Use PUT to update. Expects the same format as INSERT
	 */
	public function save_question_scores_put(){
		
		//check the results
		$results=$this->put();
		$quiz = $results['quiz'];
		$question_scores = $results['question_scores'];

		if (!$results['question_scores']) {
			$this->response(array('error' => 'No question object passed'), 400);
		}
		
		if (!$question_scores['student_id']) {
			
			$this->response(array('error' => 'No student ID passed'), 400);
		}
		
		$this->load->model('testcenter_model');

		if ($results['quiz']) {
			if (!$quiz['session_name']) {
				
				$this->response(array('error' => 'No session name passed'), 400);
			}

			$quiz_results = $this->testcenter_model->update_quiz($quiz);
		}
		
		$saved_answer = $this->testcenter_model->update_question_scores($question_scores)['success'];
		if ($saved_answer == 1) {
			$response = $this->testcenter_model->update_section_overview($question_scores);
		}
		$question_scores['saved'] = $saved_answer;
		$this->response(array('quiz'=>$quiz_results, 'question_scores'=>$question_scores), 200);	
	}
    
    /**
     * Get summary question_score data
     * @param session_id required
     */
     public function summary_question_scores_get(){
         
         if(!$this->get('session_id'))
        {
            $this->response(array('error' => 'No session_id passed in url'), 400);
        }
        $this->load->helper('myhelpers');
		$student_id=get_sid();
		
        $this->load->model('testcenter_model');
		
        $results = $this->testcenter_model->get_question_scores_session($this->get('session_id'),$student_id);
		
		        
        //get the number of correct answers
        $mc_correct=0;
        $mc_incorrect=0;
        $mc_skipped=0;
        $mc_flagged=0;
        $mc_notes=0;
        
        $tbs_correct=0;
        $tbs_incorrect=0;
        $tbs_skipped=0;
        $tbs_flagged=0;
        $tbs_notes=0;
		
		//arrays indexed by either 'tbs' or 'msq'
		//$correct = $incorrect = $skipped = $flagged = $notes = [];
        
        //var_dump($results);
        //exit;
        foreach ($results as $result) {
           //look for multiple choice 
		   
		   /* Maybe later...
		   switch($result['status']){
			   case "correct":
			   if(!isset($correct[$result["type"]])) $correct[$result["type"]] = 0;
			   $correct[$result["type"]]++;
			   break;
			   case "skipped":
			   if(!isset($skipped[$result["type"]])) $skipped[$result["type"]] = 0;
			   $skipped[$result["type"]]++;
			   break;
			   default:
			   if(!isset($incorrect[$result["type"]])) $incorrect[$result["type"]] = 0;
			   $incorrect[$result["type"]]++;
			   break;
			   
		   }*/
           if ($result['type']=='mcq') {
               
               if ($result['status']=='correct') {
                   ++$mc_correct;
               } elseif ($result['status']=='skipped') {
                   ++$mc_skipped;
               } elseif($result["status"]=="incorrect") {                   
                   ++$mc_incorrect;
               }
               
               //count notes. Look for non null values
               if (isset($result['noted']) &&  $result['noted'] && $result['noted']!='[]') {
                   ++$mc_notes;
               }
               
               //count flags
               if ($result['flagged']=="1" || $result['flagged']=='true') {
                   ++$mc_flagged;
               }
               
           //look for tbs   
           } else {
                              
               if ($result['status']=='correct') {
                   ++$tbs_correct;
               } elseif ($result['status']=='skipped') {
                   ++$tbs_skipped;
               } elseif($result["status"]=="incorrect") {
                   ++$tbs_incorrect;
               } 
               
               //count notes. Look for non null values
               if (isset($result['noted']) &&  $result['noted'] && $result['noted']!='[]') {
                   ++$tbs_notes;
               } 
               
               //count flags
               if ($result['flagged']=="1" || $result['flagged']=='true') {
                   ++$tbs_flagged;
               }
                   
            
        }
        
     
     }
        
        $response['mcq']['correct']=$mc_correct;
        $response['mcq']['incorrect']=$mc_incorrect;
        $response['mcq']['skipped']=$mc_skipped;
        $response['mcq']['notes']=$mc_notes;
        $response['mcq']['flagged']=$mc_flagged;
        $response['tbs']['correct']=$tbs_correct;
        $response['tbs']['incorrect']=$tbs_incorrect;
        $response['tbs']['skipped']=$tbs_skipped;
        $response['tbs']['notes']=$tbs_notes;
        $response['tbs']['flagged']=$tbs_flagged;
        
        
        $this->response($response, 200);
	
	}

    /*
     * get all question_scores based on uid from drupal
     * 
     */
    public function get_question_scores_saved_quizzes_get(){
            
        $uid=$this->get('uid');     
        $this->load->model('testcenter_model'); 
        //get the IPQ student_id based on the uid
        $results = $this->testcenter_model->get_question_scores_saved_quizzes($uid);
        
        $session_name='';
        $i=-1;
        $data=array();
        
        //var_dump($results);
        //exit;
        
        foreach ($results as $result) {
            
            //unique session name
            if ($result['session_name'] != $session_name) {
                
                $i++;   
                  
                $data[]['quiz']=array( 
                    'session_name' => $result['session_name'],
                    'complete_percent'=>$result['complete_percent'],
                    'quiz_date'=>$result['timestamp'],
                    'attempts_average'=>$result['attempts_average'],
                );
                //reset counters
                $no_mcq = ($result['type']=='mcq') ? 1 : 0 ;
                $no_tbs = ($result['type']=='tbs') ? 1 : 0 ;
                $no_correct = ($result['score']=='1') ? 1 : 0 ;
                
                
            } else {
                //count up the tbs and mcqs
                if ($result['type']=='mcq') {
                    $no_mcq++;
                } else {
                    $no_tbs++;
                }
                
                //count up the score
                if ($result['score']=='1') {
                    $no_correct++;
                }
                
                
            }
            //update the output with the new counts
             
             $data[$i]['quiz']['no_mcq']=$no_mcq;
             $data[$i]['quiz']['no_tbs']=$no_tbs;
             $data[$i]['quiz']['no_correct']=$no_correct;             
             $session_name=$result['session_name'];
        }


        $this->response($data, 200); 
        
    }
}
