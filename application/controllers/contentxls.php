<?php

class Contentxls extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function spreadsheet($section, $chapter) {
		gc_collect_cycles();
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		//ini_set('display_startup_errors', TRUE);

		$this->load->library('excel');
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
		//$this->load->library('PHPExcel/iofactory');

		$this->load->model('testcenter_model');

		$data = $this->testcenter_model->get_allcontent($chapter, $section);


		$objPHPExcel = new PHPExcel();

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="IPQ ' . $section . '-' . $chapter . '.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$linkStyle = array(
		  'font' => array(
		    'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
		    'color' => array('rgb' => '0000CF')
		  )
		);

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['name']);
		$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray(array('font'=>array(
																					'bold' => true
																				)
																			));
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
		
		//$objPHPExcel->getActiveSheet()->setCellValue('A2', "Topic");
		
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "Question ID");
		//$objPHPExcel->getActiveSheet()->setCellValue('B2', "Question URL");
		$objPHPExcel->getActiveSheet()->setCellValue('B2', "Type");
		$objPHPExcel->getActiveSheet()->setCellValue('C2', "Tagged Topics");
		$objPHPExcel->getActiveSheet()->setCellValue('D2', "Created");
		$objPHPExcel->getActiveSheet()->setCellValue('E2', "Edited");


		$i = 3;
		
		foreach($data['children'] as $key1 => $val1) {
			//var_dump($data['children'][$key1]['name']);


				//chapter's topics
				foreach($data['children'][$key1]['children'] as $typeGroup) {
					//now the question type
				
					
						foreach($typeGroup['children'] as $question) {
							//var_dump($question);
							
							$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $question['name']);
							$objPHPExcel->getActiveSheet()->getCell('A'.$i)->getHyperlink($question['name'])->setUrl('http://testcenter.rogercpareview.com'.$question['editUrl']);
							$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($linkStyle);
							$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $question['type']);

							$topicsList = $this->testcenter_model->get_topics_ids($question['topic_id']);
							$topics = '';
							foreach($topicsList as $topic) {
								$topics = $topics . $section . '-' . $topic['order'] . ' ' . $topic['topic'] ."\n";
							}
							$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $topics);
							$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->getAlignment()->setWrapText(true);

							$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $question['createdBy'] . " on " . $question['createdOn']);
							if($question['updatedBy'] != '') {
								$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $question['updatedBy'] . " on " . $question['updatedOn']);
							} else {
								$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, 'n/a');
							}
							
							if($question['type'] == 'multiple-choice') {
								$dupeList = $this->testcenter_model->check_duplicate_by_id($question['name']);
								$cellArr = array('F', 'G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 'P', 'Q', 'R', 'S');	
								
								
								if(count($dupeList) > 1) {

									//$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, count($dupeList) . ' possible matches');//$dupeList[0]['question']);
									$j = 0;
									foreach($dupeList as $dupe) {

										$objPHPExcel->getActiveSheet()->setCellValue($cellArr[$j].$i, $dupe['id']);
										$objPHPExcel->getActiveSheet()->getCell($cellArr[$j].$i)->getHyperlink( $dupe['id'] )->setUrl('http://testcenter.rogercpareview.com/testmodule-admin/search-edit.php#/question/multiple-choice/edit/'.$dupe['id']);
										$objPHPExcel->getActiveSheet()->getStyle($cellArr[$j].$i)->applyFromArray($linkStyle);
										$j++;
									}
								} else {
									//$objPHPExcel->getActiveSheet()->setCellValue($cellArr[$j].$i, 'unknown');
								}
								//$result['editUrl'] = '/testmodule-admin/search-edit.php#/question/multiple-choice/edit/' . $q['name'];
								
							} else {
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'unsupported for this type');
							}
							$i++;	
							//gc_collect_cycles();
							
						}
			}
		}

		foreach(range('A','S') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		$objPHPExcel->getActiveSheet()->calculateColumnWidths();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

		

		//var_dump($data);
		exit;
	}
}