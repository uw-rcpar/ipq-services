<?php
/**
 * Admin Index
 *
 * Question and Simulation management system for adding and editing simulation content.
 *
 * @package		CodeIgniter
 * @category	Controller
 * @author		Sam Edwards and Steve Taylor
 * @link		http://testcenter.rogercpareview/admin/simulator
*/
class Simulator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('testcenter_model');
	}

	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		//$data['questions'] = $this->testcenter_model->get_questions();
		//$jsonObject = json_decode(file_get_contents("http://localhost/restserver/index.php/api/testcenter/multiple_choice/id/2/format/json"),TRUE);
		
		/*if (is_array($jsonObject)) {
			var_dump($jsonObject);
		}*/		

		$data['title'] = 'Edit Questions';
		
		//show it
		$this->load->view('admin/header', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('admin/footer');
	}
	
	
}