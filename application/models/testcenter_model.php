<?php
class Testcenter_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_quizzes_count($student_id, $section = null) {
		$sql_query = "SELECT count(*) as count FROM saved_quizzes WHERE student_id=$student_id";
		if($section) {
			$sql_query .= " AND section='$section'";
		}
		$quizzes = $this->db->query($sql_query);
		return $quizzes->result_array();
	}


	public function get_question_preview_quizlets($student_id, $quizlets, $status, $type, $session_id) {
		if (!$status && !$type/* && !$session_id*/) {
			foreach($quizlets as $qnum => $quizlet) {
				foreach($quizlet['questions'] as $key => $question) {
					$qid = $question['id'];
					$quizlets[$qnum]['type'] = $question['type'];
					if ($question['type'] == 'mcq' || $question['type'] == 'multiple-choice') {
						if (!$session_id) {
							$session_id = $quizlets[0]['questions'][0]['session_id'];
						}
						$youranswer = $this->db->query("SELECT answer_id as answer_id from question_scores WHERE id = $qid and session_id=$session_id")->result_array()[0]['answer_id'];
						if (strlen($youranswer) > 0) {
							$quizlets[$qnum]['questions'][$key]['student_answer_id'] = $youranswer;
							$quizlets[$qnum]['questions'][$key]['student_answer_explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_answers WHERE id=$youranswer")->result_array()[0]['explanation'];
						}
						$quizlets[$qnum]['questions'][$key]['question'] = $this->db->query("SELECT question as question from multiple_choice_questions WHERE id=$qid")->result_array()[0]['question'];
						$quizlets[$qnum]['questions'][$key]['explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_questions WHERE id=$qid")->result_array()[0]['explanation'];
						$quizlets[$qnum]['questions'][$key]['choices'] = $this->db->query("SELECT * from multiple_choice_answers WHERE question_id=$qid")->result_array();
						foreach($quizlets[$qnum]['questions'][$key]['choices'] as $choice) {
							if ($choice['is_correct'] != 0) {
								$quizlets[$qnum]['questions'][$key]['answer'] = $choice;
							}
						}
					}

					else {
						$quizlets[$qnum]['questions'][$key]['question_json'] = $this->db->query("SELECT question_json as question_json from task_based_questions where id=$qid")->result_array()[0]['question_json'];
						$quizlets[$qnum]['questions'][$key]['type'] = $this->db->query("SELECT type as type from task_based_questions where id=$qid")->result_array()[0]['type'];
						
					}
				}
			}
		}
		else {
			$qnum = 0;
			if ($type == 'multiple-choice' || $type == 'multipleChoice') {
				$type = 'mcq';
			}
			foreach($quizlets as $qnum => $quizlet) {
				foreach($quizlet['questions'] as $key =>$q) {
					$qid = $q['id'];
					$question = $this->db->query("SELECT * from question_scores WHERE id=$qid and session_id=$session_id")->result_array()[0];
$test =$question;
					if ($question['status'] == $status && $question['type'] == $type) {
						if (!isset($newquizlets)) {
							$newquizlets = array();
						}
						if (!isset($newquizlets[$qnum]['type'])) {
							if ($type == 'mcq') {
								$longtype = 'multiple-choice';
							}
							else {
								$longtype = 'tbs';
							}
							$newquizlets[$qnum]['type'] = $longtype;
						}
						$newquizlets[$qnum]['questions'][$key] = $question;
						if ($question['type'] == 'mcq') {
							$session_id = $quizlets[0]['questions'][0]['session_id'];
							$youranswer = $this->db->query("SELECT answer_id as answer_id from question_scores WHERE id = $qid and session_id=$session_id")->result_array()[0]['answer_id'];
							$quizlets[$qnum]['questions'][$key]['student_answer_id'] = $youranswer;
							$quizlets[$qnum]['questions'][$key]['student_answer_explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_answers WHERE id=$youranswer")->result_array()[0]['explanation'];
							$newquizlets[$qnum]['questions'][$key]['question'] = $this->db->query("SELECT question as question from multiple_choice_questions WHERE id=$qid")->result_array()[0]['question'];
							$newquizlets[$qnum]['questions'][$key]['explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_questions WHERE id=$qid")->result_array()[0]['explanation'];
							$newquizlets[$qnum]['questions'][$key]['choices'] = $this->db->query("SELECT * from multiple_choice_answers WHERE question_id=$qid")->result_array();
							foreach($quizlets[$qnum]['questions'][$key]['choices'] as $choice) {
								if ($choice['is_correct'] != 0) {
									$quizlets[$qnum]['questions'][$key]['answer'] = $choice;
								}
							}
						}
						else {
							$quizlets[$qnum]['questions'][$key]['question_json'] = $this->db->query("SELECT question_json as question_json from task_based_questions where id=$qid")->result_array()[0]['question_json'];
							$quizlets[$qnum]['questions'][$key]['type'] = $this->db->query("SELECT type as type from task_based_questions where id=$qid")->result_array()[0]['type'];
						}
					}
				}
			}
			$quizlets = $newquizlets;
		}
		$mcqarray = array();
		$tbsarray = array();
		foreach($quizlets as $qnum => $quizlet) {
			if ($quizlet['questions'][0]['type'] !== 'mcq') {
				array_unshift($tbsarray, $quizlet);
			}
			else {
				array_unshift($mcqarray, $quizlet);
			}
		}
		$quizlets = array_merge($mcqarray, $tbsarray);
		return array('quizlets' =>$quizlets);
	}
	

	public function get_question_preview($sid, $chapter_id, $status, $type, $limit) {
		$ids = array();
		$questions = array();
		if ($type == 'multipleChoice' || $type == 'mcq') {
			$type = 'mcq';
		} else if ($type != 'mcq' && $type != 'multipleChoice') {
			$type = 'tbs';
		}
			if ($type == 'mcq') {
				$questions = $this->db->query("SELECT id as id from multiple_choice_questions where chapter_id=$chapter_id")->result_array();
			}
			else {
				$questions = $this->db->query("SELECT id as id from task_based_questions where chapter_id=$chapter_id")->result_array();
			}
		foreach($questions as $q) {
			array_push($ids, $q['id']);
		}
		$most_recent_session = array();
		if (count($ids) > 0) {
			$ids_str = implode(',', $ids);
//			$query_str = 'SELECT * from question_scores WHERE id in (' . $ids_str . ') and status=\'' . $status . '\' and type=\'' . $type . '\'';
//			$query_str = "SELECT * from question_scores WHERE id in ($ids_str) and status='$status' and type='$type'";
			$query_str = "SELECT id as id, answer_id as answer_id, session_id as session_id, status as status from question_scores WHERE id in ($ids_str) and type='$type' and student_id=$sid order by session_id asc";
			$query = $this->db->query("SELECT id as id, answer_id as answer_id, session_id as session_id, status as status from question_scores WHERE id in ($ids_str) and type='$type' and student_id=$sid order by session_id asc")->result_array();
			foreach($query as $q) {
				$qid = $q['id'];
				$most_recent_session[$qid] = array('session_id' => $q['session_id'], 'answer_id' => $q['answer_id'], 'status' => $q['status']);
			}
			
			$i = 0;
			foreach($most_recent_session as $question_id => $q_info) {
				if ($q_info['status'] == $status) {
					if (is_numeric($limit)) {
						if ($i < $limit) {
							$result[$question_id] = array();
							$result[$question_id]['id'] = $question_id;
							$result[$question_id]['answer_id'] = $q_info['answer_id'];
							if ($type == 'mcq') {
								$result[$question_id]['question'] = $this->db->query("SELECT question as question from multiple_choice_questions WHERE id=$question_id")->result_array()[0]['question'];
								$result[$question_id]['explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_questions WHERE id=$question_id")->result_array()[0]['explanation'];
								$result[$question_id]['choices'] = $this->db->query("SELECT * from multiple_choice_answers WHERE question_id=$question_id")->result_array();
								foreach($result[$question_id]['choices'] as $choice) {
									if ($choice['is_correct'] != 0) {
										$result[$question_id]['answer'] = $choice;
									}
								}
							}

							else {
								$result[$question_id]['question_json'] = $this->db->query("SELECT question_json as question_json from task_based_questions where id=$question_id")->result_array()[0]['question_json'];
								$result[$question_id]['type'] = $this->db->query("SELECT type as type from task_based_questions where id=$question_id")->result_array()[0]['type'];
								
							}
						}
						$i++;
					}
					else {
						$result[$question_id] = array();
						$result[$question_id]['id'] = $question_id;
						$result[$question_id]['answer_id'] = $q_info['answer_id'];
						if ($type == 'mcq') {
							$result[$question_id]['question'] = $this->db->query("SELECT question as question from multiple_choice_questions WHERE id=$question_id")->result_array()[0]['question'];
							$result[$question_id]['explanation'] = $this->db->query("SELECT explanation as explanation from multiple_choice_questions WHERE id=$question_id")->result_array()[0]['explanation'];
							$result[$question_id]['choices'] = $this->db->query("SELECT * from multiple_choice_answers WHERE question_id=$question_id")->result_array();
							foreach($result[$question_id]['choices'] as $choice) {
								if ($choice['is_correct'] != 0) {
									$result[$question_id]['answer'] = $choice;
								}
							}
						}

						else {
							$result[$question_id]['question_json'] = $this->db->query("SELECT question_json as question_json from task_based_questions where id=$question_id")->result_array()[0]['question_json'];
							$result[$question_id]['type'] = $this->db->query("SELECT type as type from task_based_questions where id=$question_id")->result_array()[0]['type'];
							
						}
					}
				}
				
			} 
		}
		if (is_numeric($limit)) {
			shuffle($result);
		}
//		$result = $this->db->query("SELECT question as question from multiple_choice_questions WHERE id=$test")->result_array();
//		return array(json_encode($query));
		return array('questions' => $result, 'type' => $type);
//		return array(array(), array(), $most_recent_session, $sid);
	}
	
	public function get_question_preview_v2($sid, $chapter_id, $status, $type, $limit) {
			
		$section_overview='';
		$section_overview_clause='';
		
		
		
		switch ($status) {
			case 'correct':
				$status_clause="qs.status = 'correct'";
				$section_overview=", section_overview so";
				$section_overview_clause="so.question_id = q.id and so.status = 'correct' and so.chapter_id = $chapter_id and";
				break;
				
			case 'incorrect':
				$status_clause="qs.status = 'incorrect'";
				$section_overview=", section_overview so";
				$section_overview_clause="so.question_id = q.id and so.status = 'incorrect' and so.chapter_id = $chapter_id and";
				break;
				
			case 'flagged':
				$status_clause="qs.flagged = 1";
				break;
				
			case 'noted':
				$status_clause="qs.noted <> '[]' and qs.noted <> ''";
				break;
		}
		
		if ($type == 'mcq' && $status != 'incorrect') {
			
		$query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
									FROM question_scores qs, saved_quizzes sq, multiple_choice_questions q, multiple_choice_answers a$section_overview
									where qs.id = q.id and
									$section_overview_clause
									qs.session_id = sq.id and
									q.id = a.question_id and
									qs.type = 'mcq' and
									qs.student_id = $sid and
									qs.chapter_id = $chapter_id and 
									$status_clause
									order by qs.session_id asc, a.id");
		
		
		} elseif ($type == 'tbs')  {
			
			$query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.type as type_long_name FROM question_scores qs, task_based_questions q$section_overview
									where qs.id = q.id and
									$section_overview_clause
									qs.type = 'tbs' and
									qs.student_id = $sid and
									qs.chapter_id = $chapter_id and 
									$status_clause
									order by qs.session_id");
		}
        
        
        //overwrite for incorrect answers in mcq's
       if ($type == 'mcq' && $status == 'incorrect') {
                       
                
                $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
                                            FROM multiple_choice_questions q
                                            inner join multiple_choice_answers a
                                                on q.id = a.question_id
                                            inner join section_overview so
                                                on so.question_id = q.id
                                            inner join question_scores qs
                                                on so.session_id = qs.session_id
                                            where 
                                            so.type = 'mcq' and
                                            so.student_id = $sid and
                                            so.chapter_id = $chapter_id and 
                                            qs.answer_id = (select answer_id from question_scores where session_id = so.session_id and id = so.question_id and status = 'incorrect' and type ='mcq') and
                                            so.status = 'incorrect'
                                            order by so.session_id asc, a.id");
         }
           
        
        
		return $query->result_array();
	}

	public function get_question_preview_v3($sid, $chapter_id, $status, $type, $limit, $nasba) {
		$result=array();
		
		$section_overview='';
		$section_overview_clause='';
				
		
		switch ($status) {
			case 'correct':
				$status_clause="qs.status = 'correct'";
				$section_overview=", section_overview so";
				$section_overview_clause="so.question_id = q.id and";
				break;
				
			case 'incorrect':
				$status_clause="qs.status = 'incorrect'";
				$section_overview=", section_overview so";
				$section_overview_clause="so.question_id = q.id and";
				break;
				
			case 'flagged':
				$status_clause="qs.flagged = 1";
				break;
				
			case 'noted':
				$status_clause="qs.noted <> '[]' and qs.noted <> ''";
				break;
				
			case 'unanswered':
				//get question ids to exclude
				$excludes=array();
				break;
		}
		
		foreach ($type as $qtype) {
			
		
		if ($qtype == 'mcq') {
			
			//unanswered needs a shit ton of processing
			if ($status=='unanswered') {
				$query = $this->db->query("SELECT 
											    question_id as id
											FROM
											    section_overview
											where
											    student_id = $sid and type = 'mcq'
											        and chapter_id IN ($chapter_id)");
					$exclude_ids=$query->result_array();
					foreach ($exclude_ids as $exclude) {
						$excludes[]=$exclude['id'];
					}				
					$exclude_list=implode(',', $excludes);
					$exclude_list = ($exclude_list=='') ? 0 : $exclude_list ;
					$query = $this->db->query("SELECT *, q.id as qs_id, q.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
									FROM question_topics qt, chapters c, topics t, multiple_choice_questions q, multiple_choice_answers a 
									where q.id = a.question_id and
									(q.deprecated <> 1 or q.deprecated is null) and
									qt.topic_id = t.id and
									t.chapter_id = c.id and
									q.id = qt.question_id and
									qt.question_type = 'mcq' and
									c.id IN ($chapter_id) and 
									q.id not in ($exclude_list)
									order by q.id, a.id
									limit 200")->result_array();
									 $result['mcq']=$query;
			
			} elseif ($status=='incorrect'){
			             
			         $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
                                            FROM multiple_choice_questions q
                                            inner join multiple_choice_answers a
                                                on q.id = a.question_id
                                            inner join section_overview so
                                                on so.question_id = q.id
                                            inner join question_scores qs
                                                on so.session_id = qs.session_id
                                            where 
                                            so.type = 'mcq' and
                                            so.student_id = $sid and
                                            so.chapter_id IN ($chapter_id) and 
                                            qs.answer_id = (select answer_id from question_scores where session_id = so.session_id and id = so.question_id and status = 'incorrect' and type ='mcq' limit 1) and
                                            so.status = 'incorrect'
                                            order by so.session_id asc, a.id
                                            limit $limit")->result_array();
                                            $result['mcq']=$query;
			    
			    
			} else {
			
		//note that 200 rows equals 50 questions and answers	
		$query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
									FROM question_scores qs, saved_quizzes sq, multiple_choice_questions q, multiple_choice_answers a$section_overview 
									where qs.id = q.id and
									$section_overview_clause
									qs.session_id = sq.id and
									q.id = a.question_id and
									qs.type = 'mcq' and
									qs.student_id = $sid and
									qs.chapter_id IN ($chapter_id) and 
									$status_clause
									order by qs.session_id asc, a.id
									limit $limit"
									)->result_array();
									 $result['mcq']=$query;		
									
			}
		}
		if ($qtype == 'tbs'){
			$nasba_clause = $nasba ? '' : " q.type <> 'tbs-research' AND";
		            //unanswered needs a shit ton of processing
            if ($status=='unanswered') {
                $query = $this->db->query("SELECT 
                                                question_id as id
                                            FROM
                                                section_overview
                                            where
                                                student_id = $sid and type = 'tbs'
                                                    and chapter_id IN ($chapter_id)");
                    $exclude_ids=$query->result_array();
                    foreach ($exclude_ids as $exclude) {
                        $excludes[]=$exclude['id'];
                    }               
                    $exclude_list=implode(',', $excludes);
                    $exclude_list = ($exclude_list=='') ? 0 : $exclude_list ;
                    $query = $this->db->query("SELECT *, q.id as qs_id
                                    FROM question_topics qt, chapters c, topics t, task_based_questions q
                                    where $nasba_clause qt.topic_id = t.id and
                                    (q.deprecated <> 1 or q.deprecated is null) and
                                    t.chapter_id = c.id and
                                    q.id = qt.question_id and
                                    qt.question_type = 'tbs' and
                                    c.id IN ($chapter_id) and 
                                    q.id not in ($exclude_list)
                                    order by q.id
                                    limit 200")->result_array();
                                     $result['tbs']=$query;
            } else {
		        
		    
			
			$query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id FROM question_scores qs, task_based_questions q$section_overview 
									where qs.id = q.id and
									$section_overview_clause
									$nasba_clause
									qs.type = 'tbs' and
									qs.student_id = $sid and
									qs.chapter_id IN ($chapter_id) and 
									$status_clause
									order by qs.session_id")->result_array();
									
									$result['tbs']=$query;
							}
		}
		}
		return $result;
	}
	
	public function get_question_preview_v4($sid, $chapter_id, $status, $type, $limit) {
            
        $section_overview='';
        $section_overview_clause='';
        
        
        
        switch ($status) {
            case 'correct':
               
               if ($type == 'mcq') {
                      
                  $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
                                            FROM multiple_choice_questions q
                                            inner join multiple_choice_answers a
                                                on q.id = a.question_id
                                            inner join section_overview so
                                                on so.question_id = q.id
                                            inner join question_scores qs
                                                on so.session_id = qs.session_id
                                            where 
                                            so.type = 'mcq' and
                                            so.student_id = $sid and
                                            so.chapter_id = $chapter_id and 
                                            qs.answer_id = (select answer_id from question_scores where session_id = so.session_id and id = so.question_id and status = 'correct' and type ='mcq' limit 1 ) and
                                            so.status = 'correct'
                                            order by so.session_id asc, a.id");
                   
                   
               } else {
                   //tbs
                   $query = $this->db->query("SELECT 
                                                    *,
                                                    qs.id as qs_id,
                                                    qs.answer_id as my_answer_id,
                                                    q.type as type_long_name
                                                FROM
                                                    task_based_questions q
                                                        inner join
                                                    section_overview so ON so.question_id = q.id
                                                        inner join
                                                    question_scores qs ON so.session_id = qs.session_id
                                                where
                                                    so.type = 'tbs' and so.student_id = $sid
                                                        and so.chapter_id = $chapter_id
                                                        and qs.answer_id = (select 
                                                            answer_id
                                                        from
                                                            question_scores
                                                        where
                                                            session_id = so.session_id
                                                                and id = so.question_id
                                                                and status = 'correct'
                                                                and type = 'tbs')
                                                        and so.status = 'correct'
                                                order by qs.session_id");
               }
               
               return $query->result_array();
                break;
                
            case 'incorrect':
                if ($type == 'mcq') {
                      
                  $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
                                            FROM multiple_choice_questions q
                                            inner join multiple_choice_answers a
                                                on q.id = a.question_id
                                            inner join section_overview so
                                                on so.question_id = q.id
                                            inner join question_scores qs
                                                on so.session_id = qs.session_id
                                            where 
                                            so.type = 'mcq' and
                                            so.student_id = $sid and
                                            so.chapter_id = $chapter_id and 
                                            qs.answer_id = (select answer_id from question_scores where session_id = so.session_id and id = so.question_id and status = 'incorrect' and type ='mcq' limit 1) and
                                            so.status = 'incorrect'
                                            order by so.session_id asc, a.id");
                   
                   
               } else {
                   //tbs
                   $query = $this->db->query("SELECT 
                                                    *,
                                                    qs.id as qs_id,
                                                    qs.answer_id as my_answer_id,
                                                    q.type as type_long_name
                                                FROM
                                                    task_based_questions q
                                                        inner join
                                                    section_overview so ON so.question_id = q.id
                                                        inner join
                                                    question_scores qs ON so.session_id = qs.session_id
                                                where
                                                    so.type = 'tbs' and so.student_id = $sid
                                                        and so.chapter_id = $chapter_id
                                                        and qs.answer_id = (select 
                                                            answer_id
                                                        from
                                                            question_scores
                                                        where
                                                            session_id = so.session_id
                                                                and id = so.question_id
                                                                and status = 'incorrect'
                                                                and type = 'tbs')
                                                        and so.status = 'incorrect'
                                                order by qs.session_id");
               }
               return $query->result_array();
                break;
                
            case 'flagged':
                $status_clause="qs.flagged = 1";
                break;
                
            case 'noted':
                $status_clause="qs.noted <> '[]' and qs.noted <> ''";
                break;
        }
        
        if ($type == 'mcq') {
            
        $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.explanation as q_explanation, a.id as answer_id 
                                    FROM question_scores qs, saved_quizzes sq, multiple_choice_questions q, multiple_choice_answers a
                                    where qs.id = q.id and                                    
                                    qs.session_id = sq.id and
                                    q.id = a.question_id and
                                    qs.type = 'mcq' and
                                    qs.student_id = $sid and
                                    qs.chapter_id = $chapter_id and 
                                    $status_clause
                                    order by qs.session_id asc, a.id");
        
        
        } elseif ($type == 'tbs')  {
            
            $query = $this->db->query("SELECT *, qs.id as qs_id, qs.answer_id as my_answer_id, q.type as type_long_name FROM question_scores qs, task_based_questions q
                                    where qs.id = q.id and                                   
                                    qs.type = 'tbs' and
                                    qs.student_id = $sid and
                                    qs.chapter_id = $chapter_id and 
                                    $status_clause
                                    order by qs.session_id");
        }        
        
        
        return $query->result_array();
    }
	
	
	public function get_questions_answers($id = 0)
	{
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->join('multiple_choice_answers', 'multiple_choice_answers.question_id = multiple_choice_questions.id');
		$this->db->where('multiple_choice_questions.id', $id);	
		
		
		$query = $this->db->get();
		return $query->result_array();

	}
/*
	public function get_all_students() {
		$this->db->select('id');
		$this->db->from('students');
		$result = $this->db->get()->result_array();
		return $result;
	}
*/	
	//get a number of multiple choice questions for editing
	public function get_questions($id)
	{
		//if id is not 0 then edit that question
		if ($id!=0) {
			$query = $this->db->get_where('multiple_choice_questions', array('id' => $id));
			return $query->result_array();
		}
			
			
		//get the latest 25 questions	
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->order_by('id', 'desc'); 
		$this->db->limit(25);
				
		$query = $this->db->get();
		return $query->result_array();

	}
	
	public function get_alltopicquestions($type, $topicID) {
		if($type == 'multiple-choice') {

			$query = $this->db->query("SELECT q.id as name, q.difficulty, q.topic_id, q.docID, q.createdOn as qCreatedOn, 
										q.createdBy as createdBy, q.updatedOn as qUpdatedOn, q.updatedBy as qUpdatedBy,  
										'multiple-choice' as type, $topicID as primary_topic, doc.title as docTitle, doc.url as docUrl, 
										doc.type as docType, doc.checkedOutBy as docCheckedOutBy, doc.checkedOutAt as docCheckedOutAt, 
										doc.checkedInAt as docCheckedInAt           
											FROM question_topics t, multiple_choice_questions q            
											JOIN documents doc ON q.docID = doc.id           
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id = $topicID          
										group by  q.topic_id, q.id LIMIT 0, 1000");


/*
			$query = $this->db->query("SELECT q.id as name, q.difficulty, q.topic_id,  q.docID, 'multiple-choice' as type, $topicID as primary_topic
										FROM multiple_choice_questions q, question_topics t 
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id = $topicID
										group by  q.topic_id, q.id");
*/
			
		} else {
			$query = $this->db->query("SELECT q.id as name, q.difficulty, q.topic_id, q.type, q.docID,   
											q.createdOn as qCreatedOn, q.createdBy as createdBy,  
											q.updatedOn as qUpdatedOn, q.updatedBy as qUpdatedBy,   
											$topicID as primary_topic, doc.title as docTitle, doc.url as docUrl,            
											doc.type as docType, doc.checkedOutBy as docCheckedOutBy, doc.checkedOutAt as docCheckedOutAt,            
											doc.checkedInAt as docCheckedInAt              
											FROM question_topics t, task_based_questions q            
												JOIN documents doc ON q.docID = doc.id           
											where q.id = t.question_id and t.question_type = 'tbs'            
											and q.type = '$type' and t.topic_id = $topicID           
											group by q.topic_id, q.id LIMIT 0, 1000");
		}
		$query =  $query->result_array();
		$results= array();
		foreach($query AS $q) {
		  $result = array();
		  $result['name'] = $q['name'];
		  $result['topic_id'] = $q['topic_id'];
		  $result['docID'] = $q['docID'];
		  $result['type'] = $q['type'];
		  $result['primary_topic'] = $q['primary_topic'] ;
		  $result['createdOn'] = $q['qCreatedOn'];
		  $result['createdBy'] = $q['createdBy'];
		  $result['updatedOn'] = $q['qUpdatedOn'];
		  $result['updatedBy'] = $q['qUpdatedBy'];
		  $result['docUrl'] = $q['docUrl'];
		  if($q['type'] == 'multiple-choice') {
				$result['editUrl'] = '/testmodule-admin/search-edit.php#/question/multiple-choice/edit/' . $q['name'];
			} else if($q['type'] == 'tbs-wc') {
				$result['editUrl'] = '/testmodule-admin/build-wc-question.php#/edit/' . $q['name'];
			} else {
				$result['editUrl'] = '/testmodule-admin/build-widget-question.php#/edit/' .$q['name'] ;
			}
			$result['nodeType'] = 'question';
			array_push($results, $result);
		}
		return $results;
				
	}

	public function get_edit_url($question) {
		  if (strlen($question['id']) > 0) {
			$id = $question['id'];
		  }
		  else if (strlen($question['name']) > 0) {
			$id = $question['name'];
		  }
		  else {
			return '';
		  }
		  if($question['type'] == 'multiple-choice') {
				$editUrl = '/testmodule-admin/search-edit.php#/question/multiple-choice/edit/' . $id;
			} else if($question['type'] == 'tbs-wc') {
				$editUrl = '/testmodule-admin/build-wc-question.php#/edit/' . $id;
			} else {
				$editUrl = '/testmodule-admin/build-widget-question.php#/edit/' . $id;
			}
		return $editUrl;
	}

	public function get_session_metadata($session_id) {
		$questions = $this->db->query("select id as id, quizlet_num as quizlet_num, type as type from question_scores where session_id=$session_id order by quizlet_num")->result_array();
		$timestamp = $this->db->query("select timestamp from saved_quizzes where id=$session_id")->result_array()[0]['timestamp'];
		$quizlets = array();
		$chapter_count = array();
		foreach($questions as $q) {
			$quizlet_num = $q['quizlet_num'];
			$qid = $q['id'];
			if ($q['type'] == 'mcq') {
				$chapter = $this->db->query("select chapter_id from multiple_choice_questions where id = $qid")->result_array();		
				$typename = "multiple-choice";
			}
			else {
				$chapter = $this->db->query("select chapter_id from task_based_questions where id = $qid")->result_array();		
				$typename = "task-based simulation";
			}
			if (!isset($chapter_count[$quizlet_num])) {
				$chapter_count[$quizlet_num] = array();
			}
			if (!in_array($chapter, $chapter_count[$quizlet_num])) {
				array_push($chapter_count[$quizlet_num], $chapter[0]['chapter_id']);
			}
			if (!isset($quizlets[$quizlet_num])) {
				$quizlets[$quizlet_num] = array(
						'type'=>array($q['type']),
						'typename'=>$typename,
						'chapters'=>count($chapter_count[$quizlet_num])
				);
			}
			else {
				array_push($quizles[$quizlet_num]['type'], $q['type']);
				$quizles[$quizlet_num]['chapters'] = count($chapter_count[$quizlet_num]);
				
			}
		}
	
		return $result = array($quizlets, $timestamp);
	}

	public function get_alltopicquestions_full($type, $topicID) {
		if($type == 'multiple-choice') {

			$query = $this->db->query("SELECT q.id as name, q.difficulty, q.topic_id, q.docID, q.createdOn as qCreatedOn, 
										q.createdBy as createdBy, q.updatedOn as qUpdatedOn, q.updatedBy as qUpdatedBy,  
										'multiple-choice' as type, $topicID as primary_topic, doc.title as docTitle, doc.url as docUrl, 
										doc.type as docType, doc.checkedOutBy as docCheckedOutBy, doc.checkedOutAt as docCheckedOutAt, 
										doc.checkedInAt as docCheckedInAt           
											FROM question_topics t, multiple_choice_questions q            
											JOIN documents doc ON q.docID = doc.id           
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id = $topicID          
										group by  q.topic_id, q.id LIMIT 0, 1000");
			
		} else {
			$query = $this->db->query("SELECT q.id as name, q.difficulty, q.topic_id, q.type, q.docID, 
											q.question_json,  
											q.createdOn as qCreatedOn, q.createdBy as createdBy,  
											q.updatedOn as qUpdatedOn, q.updatedBy as qUpdatedBy,   
											$topicID as primary_topic, doc.title as docTitle, doc.url as docUrl,            
											doc.type as docType, doc.checkedOutBy as docCheckedOutBy, doc.checkedOutAt as docCheckedOutAt,            
											doc.checkedInAt as docCheckedInAt              
											FROM question_topics t, task_based_questions q            
												JOIN documents doc ON q.docID = doc.id           
											where q.id = t.question_id and t.question_type = 'tbs'            
											and q.type = '$type' and t.topic_id = $topicID           
											group by q.topic_id, q.id LIMIT 0, 1000");
		}
		$query =  $query->result_array();
		$results= array();
		foreach($query AS $q) {
		  $result = array();
		  /*

		  var newModel = JSON.parse(parseQuestion.question_json);
	var question = decodeURIComponent(newModel.question); 
	$scope.question = JSON.parse(question);
	*/
			$question = json_parse($q['question_json']);
			$question = urldecode($question['question']);
			$question = json_parse($question);

		  $result['question_json'] = $question;//json_decode(urldecode($q['question_json']));
		  $result['name'] = $q['name'];
		  $result['topic_id'] = $q['topic_id'];
		  $result['docID'] = $q['docID'];
		  $result['type'] = $q['type'];
		  $result['primary_topic'] = $q['primary_topic'] ;
		  $result['createdOn'] = $q['qCreatedOn'];
		  $result['createdBy'] = $q['createdBy'];
		  $result['updatedOn'] = $q['qUpdatedOn'];
		  $result['updatedBy'] = $q['qUpdatedBy'];
		  $result['docUrl'] = $q['docUrl'];
		  if($q['type'] == 'multiple-choice') {
				$result['editUrl'] = '/testmodule-admin/search-edit.php#/question/multiple-choice/edit/' . $q['name'];
			} else if($q['type'] == 'tbs-wc') {
				$result['editUrl'] = '/testmodule-admin/build-wc-question.php#/edit/' . $q['name'];
			} else {
				$result['editUrl'] = '/testmodule-admin/build-widget-question.php#/edit/' .$q['name'] ;
			}
			$result['nodeType'] = 'question';
			array_push($results, $result);
		}
		return $results;
				
	}

	public function get_allcontent($get_chapter, $get_section) {
		$chapter = $this->get_chapter($get_chapter, $get_section)[0];
			
		$root = array('name' => $chapter['chapter'], 'children' => array(), 'data' => $chapter, 'nodeType' => 'chapter');
        $topics = $this->get_chapter_topics($chapter['id']);
        foreach($topics as $topic) {
			$topic_prefix = $get_section . " " . $chapter['order'] . "." . $topic['order'];
			$topicChild = array('name' => $topic_prefix, 'children' => array(), 'data' => $topic, 'nodeType' => 'topic');
			$questions = array();
			array_push($questions, array('name'=>'mp', 'children' => $this->get_alltopicquestions('multiple-choice', $topic['id']), 'nodeType' => 'questionType' ));
			array_push($questions, array('name' => 'tbs-journal', 'children' => $this->get_alltopicquestions('tbs-journal', $topic['id']), 'nodeType' => 'questionType' ));
			array_push($questions, array('name' => 'tbs-research', 'children' => $this->get_alltopicquestions('tbs-research', $topic['id']), 'nodeType' => 'questionType' ));
			array_push($questions, array('name' => 'tbs-wc', 'children' => $this->get_alltopicquestions('tbs-wc', $topic['id']), 'nodeType' => 'questionType' ));
			$topicChild['children'] = $questions;
			array_push(	$root['children'], $topicChild);
		}	

		return $root;
	}

	public function get_allresearchcontent($get_chapter, $get_section) {
		$chapter = $this->get_chapter($get_chapter, $get_section)[0];
			
		$root = array('name' => $chapter['chapter'], 'children' => array(), 'data' => $chapter, 'nodeType' => 'chapter');
        $topics = $this->get_chapter_topics($chapter['id']);
        foreach($topics as $topic) {
			$topic_prefix = $get_section . " " . $chapter['order'] . "." . $topic['order'];
			$topicChild = array('name' => $topic_prefix, 'children' => array(), 'data' => $topic, 'nodeType' => 'topic');
			$questions = array();
			//array_push($questions, array('name'=>'mp', 'children' => $this->get_alltopicquestions('multiple-choice', $topic['id']), 'nodeType' => 'questionType' ));
			//array_push($questions, array('name' => 'tbs-journal', 'children' => $this->get_alltopicquestions('tbs-journal', $topic['id']), 'nodeType' => 'questionType' ));
			array_push($questions, array('name' => 'tbs-research', 'children' => $this->get_alltopicquestions('tbs-research', $topic['id']), 'nodeType' => 'questionType' ));
			//array_push($questions, array('name' => 'tbs-wc', 'children' => $this->get_alltopicquestions('tbs-wc', $topic['id']), 'nodeType' => 'questionType' ));
			$topicChild['children'] = $questions;
			array_push(	$root['children'], $topicChild);
		}	

		return $root;
	}

	public function get_dupes_mpc() {
		$query = $this->db->query("SELECT id, chapter_id, topic_id, multiple_choice_questions.question 
									FROM multiple_choice_questions
										inner join (select question from multiple_choice_questions GROUP BY question HAVING count(id) > 1) 
									dup ON multiple_choice_questions.question = dup.question");
		$query = $query->result_array();

		return $query();
	}

	public function get_chapter($order, $section) {
		$query = $this->db->query("SELECT c.* from chapters c where c.order = $order and c.section = '$section'" );
		$query = $query->result_array();
		return $query;
	}

	//get just the question ids for an editing screen for a particular user
	public function get_question_ids($username)
	{
		$this->db->select('id');
		$this->db->from('multiple_choice_questions');
		$this->db->where('createdBy', $username);
				
		$query = $this->db->get();
		return $query->result_array();
	}
/*
	public function get_quizlets_questions_by_qid($type, $qids) {
		if ($type != 'tbs') {
			$query = $this->db->query("SELECT q.id, q.deprecated q.difficulty, q.topic_id, q.question from multiple_choice_questions q where q.id IN ('".implode(',',$qids).") and (q.deprecated <> 1 or q.deprecated is null) group by q.id, q.deprecated, q.difficulty, q.topic_id, q.question order by q.id desc");
		}
		else {
			$query = $this->db->query("SELECT q.id, q.deprecated q.difficulty, q.topic_id, q.question from task_based_questions q where q.id IN ('".implode(',',$qids).") and (q.deprecated <> 1 or q.deprecated is null) group by q.id, q.deprecated, q.difficulty, q.topic_id, q.question order by q.id desc");
		}
		return $query->result_array();
	}
*/	
	/**
	 * Get questions for quizlets
	 * @param $type must be "multiple choice" or "TBS"
	 * @param $topics must be comma delimited list 
	 * @param $filter_ids comma delimited list of ids that should NOT be returned
	 */
	public function get_quizlets_questions($type,$topics,$no_questions,$sid,$nasba,$filterQuestions,$filter_ids='0')
	{
		 	
		    
		 $type = ($type != 'tbs') ? 'mcq' : 'tbs';
         //filter the questions on unanswered or incorrect      
		 $exclude_list=0;
         
         $order_by_rand='ORDER BY RAND()';
		 
         switch ($filterQuestions) {
            case "incorrect":
                $query = $this->db->query("SELECT question_id as id FROM section_overview where student_id = $sid and 
                                            type = '$type' and status = 'incorrect'");
                    $exclude_ids=$query->result_array(); 
                    //remove order by rand() clause in the query
                     $order_by_rand=''; 
                    break;
            case "unanswered":
                $query = $this->db->query("SELECT question_id as id FROM section_overview where student_id = $sid and 
                                            type = '$type'");
                    $exclude_ids=$query->result_array(); 
                    //remove order by rand() clause in the query
                     $order_by_rand=''; 
                    
                break;
            
            default:
                $exclude_list=0;
            }
         
         //create a list to exclude from db returns
         if (isset($exclude_ids)) {
             foreach ($exclude_ids as $exclude) {
                        $excludes[]=$exclude['id'];
                    }               
                    $exclude_list=implode(',', $excludes);
                    $exclude_list = ($exclude_list=='') ? 0 : $exclude_list ;     
         }
         
         //decide whether to use IN or NOT IN
         if ($filterQuestions=='incorrect') {
                $filter_clause = "IN";
            } else {
                $filter_clause = "NOT IN";
            }
                    
            				
		if ($type != 'tbs') {
		                
           
               
                $query = $this->db->query("SELECT q.id, q.deprecated, q.difficulty, q.topic_id, q.question 
                                        FROM multiple_choice_questions q, question_topics t 
                                        where q.id = t.question_id and 
                                        t.question_type = 'mcq' and 
                                        q.id $filter_clause ($exclude_list) and q.id NOT IN ($filter_ids) and
                                        (q.deprecated <> 1 or q.deprecated is null) and t.topic_id in ($topics)
                                        group by q.id, q.deprecated, q.difficulty, q.topic_id, q.question 
                                        $order_by_rand LIMIT $no_questions");
           	
			
			
		} else {
			
			if ($nasba==FALSE) {
				//exclude tbs-reseach
				$tbs_sql= "type <> 'tbs-research' AND";
				
			} else {
			    
				$tbs_sql= '';
			
			}
			
			
			$query = $this->db->query("SELECT q.id, q.deprecated, q.difficulty, q.topic_id, q.question_json, q.type 
										FROM task_based_questions q, question_topics t 
										where $tbs_sql q.id = t.question_id and t.question_type = 'tbs' and 
										q.id $filter_clause ($exclude_list) and q.id NOT IN ($filter_ids) and
										(q.deprecated <> 1 or q.deprecated is null) and 
										t.topic_id in ($topics)
										group by q.id, q.deprecated, q.difficulty, q.topic_id, q.question_json, q.type
										$order_by_rand LIMIT $no_questions");
			
			
		}
		return $query->result_array();
//		return array('results'=>$query->result_array(), 'filtered'=>$filter_ids);

	}
	//get the quizlet answers
	//@param $question_id_list a list of question ids returned from get_quizlet_questions
	public function get_quizlets_answers($question_id_list)
	{
		$query = $this->db->query('SELECT * FROM multiple_choice_answers
									WHERE question_id
									IN ('.$question_id_list.' )');
		
		return $query->result_array();

	}
	//get topics and sub topics
	public function get_topics($section = 'far')
	{
			
		
		
		$query = $this->db->query("SELECT *, chapters.order AS chapter_prefix, topics.order AS topic_prefix 
									FROM chapters, topics 
									WHERE topics.chapter_id = chapters.id
									AND chapters.section = '$section'
									order by chapters.section, chapters.order, topics.order");	
		
		
		//$query = $this->db->get();
		return $query->result_array();

	}

	public function get_chapter_topics($chapter_id) {
		$query = $this->db->query("SELECT t.* from topics t where t.chapter_id = $chapter_id order by t.order");
		return $query->result_array();
	}
	
	//get chapters and topics by id list
	public function get_topics_ids($id_list)
	{
		$query = $this->db->query('SELECT *, topics.id as topic_id FROM topics, chapters
			where (chapters.id = topics.chapter_id) and
			(topics.id IN ('.$id_list.'))');
			
		
		
		return $query->result_array();

	}
	
	//get student progress info
	public function get_student_info($student_id, $question_id=0)
	{
		$this->db->select('*');
		$this->db->from('student_progress');
		$this->db->join('student_progress_answers', 'student_progress.student_id = student_progress_answers.student_id');
		$this->db->where('student_progress_answers.question_id = student_progress.question_id'); 
		$this->db->where('student_progress.student_id', $student_id); 
		//if question id is passed
		if ($question_id != 0) {
			$this->db->where('student_progress_answers.question_id', $question_id); 
		}
		$this->db->order_by('student_progress_answers.question_id');		
		$query = $this->db->get();
		return $query->result_array();

	}
	public function update_student_info($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('students', $data); 
		
		return $result = array('success' => TRUE );

	}
	//just get the student data based on id
	public function get_student($id){
		$query = $this->db->get_where('students', array('id' => $id));
		return $query->result_array();
	}
    //just get the student data based on uid
    public function get_drupal_student($uid){
        $query = $this->db->get_where('students', array('drupal_user_id' => $uid));
        return $query->result_array();
    }
	//just get the student data based on coldfusion id
	public function get_student_by_cfid($cfid){
		$query = $this->db->get_where('students', array('cf_student_id' => $cfid));
		return $query->result_array();
	}
	
	
	/**
	 * insert record in student_progress table
	 * data array needs these paramenters
	 * 
	 * student_id - required
	 * question_id - required
	 * answer_id - required
	 * correct_answer -required
	 * notes
	 * bookmark - default false
	 */
	public function set_student_progress($info)
	{
		$chapters = $info['chapter_scores'];
		foreach($chapters as $chapter_id => $types) {
			foreach($types as $type => $data) {
				if ($type !== 'score') {
					$sid = $info['student_id'];
					$score = $chapters[$chapter_id]['score'];
				       $incorrect = json_encode($data['incorrect']);
				       $correct = json_encode($data['correct']);
				       $skipped = json_encode($data['skipped']);
				       $noted = json_encode($data['noted']);
				       $flagged = json_encode($data['flagged']);
//					for debugging
//					$str = "INSERT INTO chapter_scores(chapter_id, student_id, type, score, incorrect, correct, skipped, noted, flagged) values($chapter_id, $sid, $type, $score, $incorrect, $correct, $skipped, $noted, $flagged) on duplicate key update score=$score, incorrect=$incorrect, correct=$correct, skipped=$skipped, noted=$noted, flagged=$flagged";
					$result = $this->db->query("INSERT INTO chapter_scores(chapter_id, student_id, type, score, incorrect, correct, skipped, noted, flagged) values($chapter_id, $sid, '$type', '$score', '$incorrect', '$correct', '$skipped', '$noted', '$flagged') on duplicate key update score='$score', incorrect='$incorrect', correct='$correct', skipped='$skipped', noted='$noted', flagged='$flagged'");
				}
			}
		}
//		for debugging
//		return array('values'=>array($chapter_id, $info['student_id'], $type, $score, $incorrect, $correct, $skipped, $noted,$flagged), 'string'=>$values);
		return $result;

	}
	
	
	public function check_duplicate($question)
	{
		$question=addslashes($question);
		$query = $this->db->query( "SELECT id FROM multiple_choice_questions where question like '%$question%'");
		$result = $query->result_array();
		return $result;

	}

	public function check_duplicate_by_id($question_id) {
		$query = $this->db->query( "SELECT question FROM multiple_choice_questions where id = $question_id");
		$query = $query->result_array();
		$matches = $this->check_duplicate($query[0]['question']);
		return $matches;
	}
	
	public function get_topics_by_qid($id,$type='mcq')
	{
		if($type == 'multiple-choice') {
			$type = 'mcq';
		}
		$query = $this->db->query( "SELECT * FROM question_topics q, topics t 
									where q.topic_id = t.id 
									and q.question_id = $id 
									and q.question_type = '$type'" );
		$result = $query->result_array();
		return $result;
	}
	
	
	/**
	 * Insert the question and return the new ID
	 */
	 public function set_question($data){
	 	
		//get the section
		$query = $this->db->query('SELECT *
									FROM topics, chapters
									WHERE topics.chapter_id = chapters.id
									AND topics.id IN ('.$data['topic_id'].')');
		$topic_data = $query->result_array();
		
		$data['section'] = $topic_data[0]['section'];
		
	 	$this->db->insert('multiple_choice_questions', $data);
		//return the new question id
		$data['question_id']=$this->db->insert_id();
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $data['question_id'],
			   'topic_id' => $topic_id,
			   'question_type' => 'mcq'
			);
			
			$this->db->insert('question_topics', $myfields);
			
		}
		
		return $data['question_id'];
		
	 }
	/**
	 * Insert the answer
	 */
	 public function set_answer($data){
	 	$this->db->insert('multiple_choice_answers', $data);
		//return the new question id
		$data['answer_id']=$this->db->insert_id();
		return $data;
		
	 }
	 /**
	 * Insert the correct answer
	 */
	 public function set_correct_answer($data, $id){
	 	$this->db->update('multiple_choice_questions', $data, "id = ".$id);
		return $data;
		
	 }
	 /**
	  * check for administrator at login
	  */
	  public function get_login($username, $password)
	{
		$this->db->select('*');
		$this->db->from('students');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		
		$query = $this->db->get();
		return $query->result_array();

	}
	/**
	  * get the number of multiple choice questions
	  */
	public function get_num_mcq($section)
	{
		$query = $this->db->query('Select count(*) as records, topic_id
									From multiple_choice_questions
									where section = "'.$section.'"
									group by topic_id');
		
		
		return $query->result_array();

	}
	/**
	  * get the number of task based simulations
	  */
	public function get_num_tbs($section)
	{
		$query = $this->db->query('Select count(*) as records, topic_id
									From task_based_questions
									where section = "'.$section.'"
									group by topic_id');
		
		return $query->result_array();

	}
	 /* Update multiple choice questions for admin edit
	 * @param $data is an array with updatedOn, updatedBy, question, difficulty
	 * @param $id is the questionID
	 */
	public function update_mc_question($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('multiple_choice_questions', $data); 
		
		$topic_ids=explode(",", $data['topic_id']);
		
		//blow out the old entries
		$this->db->delete('question_topics', array('question_id' => $id, 'question_type' => 'mcq')); 
		
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $id,
			   'topic_id' => $topic_id,
			   'question_type' => 'mcq'
			);
			
			$this->db->insert('question_topics', $myfields);
		}
		
		return $result = array('success' => TRUE, 'data' => $data);
		

	}
	//update the choices 
	public function update_mc_answers($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('multiple_choice_answers', $data); 
		
		return $result = array('success' => TRUE );
		

	}
	/**
	 * delete questions and answers
	 * @param $id is the questionID
	 * 
	 */
	public function delete_question($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('multiple_choice_questions');
		
		
		//delete from the answers table
		$this->db->where('question_id', $id);
		$this->db->delete('multiple_choice_answers');
		
		
		return $result = array('success' => TRUE );
		
	}
	
	public function delete_tbs_question($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('task_based_questions');
		
		return $result = array('success' => TRUE );
		
	}
	
	//delete an answer or choice
	public function delete_choice($id)
	{
		
		//delete from the answers table
		$this->db->where('id', $id);
		$this->db->delete('multiple_choice_answers');
		
		
		return $result = array('success' => TRUE );
		
	}
	
	
	public function save_quizlet($data){
		$this->db->insert('quizlet_sessions', $data);
		
		//return the new quizlet id
		$result['quizlet_id']=$this->db->insert_id();
		return $result;
	}
	
	public function save_quizlet_details($data){
		$this->db->insert('quizlet_sessions_details', $data);
		
		//return the new quizlet id
		$result['quizlet_detail_id']=$this->db->insert_id();
		return array('result'=>$result, 'data'=>$data);
	}
	
	//save tbs questions
	public function save_tbs_question($data){
		
		//get the section
		$query = $this->db->query('SELECT *
									FROM topics, chapters
									WHERE topics.chapter_id = chapters.id
									AND topics.id IN ('.$data['topic_id'].')');
									
		$topic_data = $query->result_array();
		
		$data['section'] = $topic_data[0]['section'];
		
		$this->db->insert('task_based_questions', $data);
		
		//return the new quizlet id
		$result['id']=$this->db->insert_id();
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $result['id'],
			   'topic_id' => $topic_id,
			   'question_type' => 'tbs'
			);
			
			$this->db->insert('question_topics', $myfields);
			
		}
		
		
		return $result;
	}
	
	//update tbs question
	public function update_tbs_question($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('task_based_questions', $data); 
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		//blow out the old entries
		$this->db->delete('question_topics', array('question_id' => $id, 'question_type' => 'tbs')); 
		
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $id,
			   'topic_id' => $topic_id,
			   'question_type' => 'tbs'
			);
			
			$this->db->insert('question_topics', $myfields);
		}

		
		
		return $result = array('success' => TRUE);
		

	}
	
	//Search questions and answers
	public function search_questions_answers($search)
	{
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->join('multiple_choice_answers', 'multiple_choice_answers.question_id = multiple_choice_questions.id');
		$this->db->like('multiple_choice_questions.question', $search);
		$this->db->or_like('multiple_choice_answers.choice', $search); 
		
		$query = $this->db->get();
		return $query->result_array();

	}
	/**
	 * Search the TBS json
	 * @param $search must be url encoded with % escaped with a | pipe
	 */
	public function search_tbs_questions($search)
	{
				
		$query = $this->db->query("SELECT * FROM task_based_questions where question_json like '%$search%' escape '|'");	
		return $query->result_array();

	}

	public function get_tbs_questions($id=0,$type='none',$username='none')
	{
		//if passing an id get it
		if ($id !=0 ) {
			$query = $this->db->get_where('task_based_questions', array('id' => $id));
			return $query->result_array();
		}
		
		//if type is passed, get the last 25 of those
		if ($type != 'none' && $username != 'none' ) {
				
			
			$this->db->select('*');
			$this->db->from('task_based_questions');	
			//$this->db->where('type', $type);
			//$this->db->where('type', 'tbs-research');	
			//$this->db->where('type <> "tbs-wc"', $type);
			$this->db->where('createdBy', $username);	
			$this->db->order_by('id', 'desc'); 
			//$this->db->limit(25);
			
			$query = $this->db->get();
			
			return $query->result_array();
		}
		
		//get the latest 25 questions	
		$this->db->select('*');
		$this->db->from('task_based_questions');
		$this->db->order_by('id', 'desc'); 
		//$this->db->limit(25);
				
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_all_mc()
	{
		$query = $this->db->get('multiple_choice_questions');
		return $query->result_array();
	}
	
	public function get_all_tbs()
	{
		$query = $this->db->get('task_based_questions');
		return $query->result_array();
	}
	
	public function get_chapters($id)
	{
		$query = $this->db->get_where('chapters', array('id' => $id));
		return $query->result_array();
	}
/*
	public function get_all_chapters() {
		$query = $this->db->query('SELECT *
									FROM topics, chapters
									WHERE topics.chapter_id = chapters.id
									AND topics.id IN ('.$data['topic_id'].')');
		return $query->result_array();
	}
*/	
	public function set_issue_report($data)
	{
		$this->db->insert('issue_reports', $data); 
		return $data;

	}
	
	//get saved sessions
	public function get_saved_sessions($student_id)
	{
		$data = array(
			'student_id' => $student_id, 
			'active' => 1
		);
		$this->db->order_by('id', 'desc'); 
		$query = $this->db->get_where('quizlet_sessions', $data);
		return $query->result_array();
	}

	public function get_count_mc($topic_id)
	{
		$query = $this->db->query("SELECT Count( * ) AS total 
								FROM multiple_choice_questions 
								WHERE find_in_set($topic_id, topic_id )");		
		$mytotal=$query->result_array();
		return $mytotal[0]['total'];
	}
	
	public function get_mc_by_ids($question_id_list)
	{
		$query = $this->db->query("SELECT * FROM multiple_choice_questions
									WHERE id IN ($question_id_list)
									ORDER BY id");
		
		return $query->result_array();

	}
	
	public function get_tbs_by_ids($question_id_list)
	{
		$query = $this->db->query("SELECT * FROM task_based_questions
									WHERE id IN ($question_id_list)
									ORDER BY id");
		
		return $query->result_array();

	}

	public function delete_session($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('saved_quizzes');
		
		return $result = array('success' => TRUE );
		
	}
	
	public function delete_session_details($id)
	{
			
		//delete from the section overview
		$this->db->where('session_id', $id);
		$this->db->delete('section_overview');
			
		//delete from the questions table
		$this->db->where('session_id', $id);
		$this->db->delete('question_scores');
		
		
		return $result = array('success' => TRUE );
		
	}
	
	public function update_quizlet_session($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('quizlet_sessions', $data); 
		
		return $result = array('success' => TRUE );	
		
	}

	/*
	 * get mcqs by topicID list
	*/

	public function get_chapter_score($sid, $chapter_ids) {
		$chapter_ids = implode(',', $chapter_ids);
		$ids = array();
		$types = ['mcq', 'tbs'];
		$most_recent_session = array();
		$result = array();
		foreach($types as $type) {
			if ($type == 'mcq') {
				$questions = $this->db->query("SELECT id as id, chapter_id as chapter_id from multiple_choice_questions where chapter_id in ($chapter_ids)")->result_array();
			}
			else {
				$questions = $this->db->query("SELECT id as id, chapter_id as chapter_id from task_based_questions where chapter_id in ($chapter_ids)")->result_array();
			}
			foreach($questions as $q) {
				$chapter = $q['chapter_id'];
				$qid = $q['id'];
				if (!isset($ids[$chapter])) {
					$ids[$chapter] = array();
				}
				if (!isset($ids[$chapter][$type])) {
					$ids[$chapter][$type] = array();
				}
				array_push($ids[$chapter][$type], $qid);
//				$ids[$chapter][$type][$qid] = $qid;
			}

			$most_recent_session = array();

			foreach($ids as $chapter => $type_list) {
					foreach($type_list as $type=>$question_list) {
						if (!isset($most_recent_session[$chapter])) {
							$most_recent_session[$chapter] = array();
						}

						if (!isset($most_recent_session[$chapter][$type])) {
							$most_recent_session[$chapter][$type] = array();
						}
					
						if (!isset($result[$chapter]['mcq']['correct'])) {
							$result[$chapter]['mcq']['correct'] = array();
							$result[$chapter]['mcq']['incorrect'] = array();
							$result[$chapter]['mcq']['skipped'] = array();
							$result[$chapter]['mcq']['noted'] = array();
							$result[$chapter]['mcq']['flagged'] = array();
						}
						if (!isset($result[$chapter]['tbs']['correct'])) {
							$result[$chapter]['tbs']['correct'] = array();
							$result[$chapter]['tbs']['incorrect'] = array();
							$result[$chapter]['tbs']['skipped'] = array();
							$result[$chapter]['tbs']['noted'] = array();
							$result[$chapter]['tbs']['flagged'] = array();
						}
						if (count($question_list) > 0) {
							$ids_str = implode(',', $question_list);
	//						$result[$chapter][$type]['query_str'] = "SELECT id as id, session_id as session_id, status as status from question_scores WHERE id in ($ids_str) and type='$type' and student_id=$sid order by session_id asc";
	//						$result[$chapter][$type]['query_str'] = $ids_str;
							$query = $this->db->query("SELECT id as id, session_id as session_id, status as status from question_scores WHERE id in ($ids_str) and type='$type' and student_id=$sid order by session_id asc")->result_array();
							$noted = $this->db->query("SELECT id as id from question_scores where id in ($ids_str) and type='$type' and student_id=$sid and noted <> '[]'")->result_array();
							$flagged = $this->db->query("SELECT id as id from question_scores where id in ($ids_str) and type='$type' and student_id=$sid and flagged = '1'")->result_array();
	//						$result[$chapter][$type]['query_result'] = $query;
	
							foreach($query as $q) {
								$qid = $q['id'];
								$most_recent_session[$chapter][$type][$qid] = array('session_id' => $q['session_id'], 'status' => $q['status']);
							}
							foreach($noted as $n) {
								$qid = $q['id'];
								if (!in_array($qid, $result[$chapter][$type]['noted'])) {
									array_push($result[$chapter][$type]['noted'], $qid);
								}
							}
							foreach($flagged as $n) {
								$qid = $q['id'];
								if (!in_array($qid, $result[$chapter][$type]['flagged'])) {
									array_push($result[$chapter][$type]['flagged'], $qid);
								}
							}
							$result[$chapter][$type]['ids'] = array();
							foreach($most_recent_session[$chapter][$type] as $question_id => $q_info) {
								$status = $q_info['status'];
								if (!in_array($question_id, $result[$chapter][$type]['ids'])) {
									array_push($result[$chapter][$type]['ids'], $question_id);	
								}
								if (!in_array($question_id, $result[$chapter][$type][$status])) {
									array_push($result[$chapter][$type][$status], $question_id);
								}
							}
						}
				}
			}
		}
		foreach($result as $chapter=>$chapter_info) {
			foreach($chapter_info as $type=>$type_info) {
					$recorded_questions = implode(',', array_merge($result[$chapter][$type]['correct'], $result[$chapter][$type]['incorrect'], $result[$chapter][$type]['skipped']));
//					$result[$chapter][$type]['unanswered'] = strlen($recorded_questions);
					$unanswered_ids = array();
					if (strlen($recorded_questions) > 0) {
				
						if ($type == 'mcq') {
							$unanswered_ids = $this->db->query("SELECT id as id from multiple_choice_questions where id not in ($recorded_questions) and chapter_id=$chapter")->result_array();
						}
						else if ($type == 'tbs') {
							$unanswered_ids = $this->db->query("SELECT id as id from task_based_questions where id not in ($recorded_questions) and chapter_id=$chapter")->result_array();
						}
					}
					else {
						if ($type == 'mcq') {
							$unanswered_ids = $this->db->query("SELECT id as id from multiple_choice_questions where chapter_id=$chapter")->result_array();
						}
						else if ($type == 'tbs') {
							$unanswered_ids = $this->db->query("SELECT id as id from task_based_questions where chapter_id=$chapter")->result_array();
						}
					}
					foreach($unanswered_ids as $unanswered) {
						$uid = intval($unanswered['id']);
						if (!in_array($uid, $result[$chapter][$type]['skipped'])) {
							array_push($result[$chapter][$type]['skipped'], $uid);
						}
					}
			}
			if (isset($ids[$chapter]['mcq'])) {
				$mcqcount = count($ids[$chapter]['mcq']);
			}
			else {
				$mcqcount = 0;
			}
			if (isset($ids[$chapter]['tbs'])) {
				$tbscount = count($ids[$chapter]['tbs']);
			}
			else {
				$tbscount = 0;
			}
			$total_questions = $mcqcount + $tbscount;
			$correct_questions = count($result[$chapter]['tbs']['correct']) + count($result[$chapter]['mcq']['correct']);
			$attempted = $correct_questions + (count($result[$chapter]['tbs']['incorrect']) + count($result[$chapter]['mcq']['incorrect']));
			if ($total_questions > 0 && $correct_questions > 0) {
				$result[$chapter]['score'] = ($correct_questions*100)/$attempted;
			}
			else {
				$result[$chapter]['score'] = 0;
			}
		}
		return array('scores' => $result);
/*
		$score = $this->db->query("SELECT * from chapter_scores where student_id=" . $sid . " and chapter_id in (" . implode(',', $chapter_ids) . ")");
		return $score->result_array();
*/
	}

	public function get_num_questions_by_chapter($chapter) {
		$mcq= $this->db->query("SELECT Count(*) as count from multiple_choice_questions where chapter_id='" . $chapter . "'");
		$tbs = $this->db->query("SELECT Count(*) as count from task_based_questions where chapter_id='" . $chapter . "'");
		$total = intval($mcq->result_array()[0]['count']) + intval($tbs->result_array()[0]['count']);
		return $total;
			
	}

    public function get_journal_by_section($section) {
        $tbs = $this->db->query("SELECT 
                                    qt.question_id, c.id as chapter_id
                                FROM
                                    question_topics qt,
                                    chapters c,
                                    topics t,
                                    task_based_questions q
                                where
                                    qt.topic_id = t.id
                                        and q.id = qt.question_id
                                        and t.chapter_id = c.id
                                        and qt.question_type = 'tbs'
                                        and (q.deprecated <> 1
                                        or q.deprecated is null)
                                        and c.section = '$section'
                                        and q.type = 'tbs-journal'
                                order by c.id");
                                
       return $tbs->result_array();
    }

    public function get_research_by_section($section) {
        $tbs = $this->db->query("SELECT 
                                    qt.question_id, c.id as chapter_id
                                FROM
                                    question_topics qt,
                                    chapters c,
                                    topics t,
                                    task_based_questions q
                                where
                                    qt.topic_id = t.id
                                        and q.id = qt.question_id
                                        and t.chapter_id = c.id
                                        and qt.question_type = 'tbs'
                                        and (q.deprecated <> 1
                                        or q.deprecated is null)
                                        and c.section = '$section'
                                        and q.type = 'tbs-research'
                                order by c.id");
                                
       return $tbs->result_array();
    }
		
	public function get_num_questions_by_chapter_v2($section) {
			$mcq= $this->db->query(
			"SELECT t.chapter_id, Count(distinct q.id) as count
				FROM question_topics qt, chapters c, topics t, multiple_choice_questions q
				where qt.topic_id = t.id and
				q.id = qt.question_id and
				t.chapter_id = c.id and
				qt.question_type = 'mcq' and
				(q.deprecated <> 1 or q.deprecated is null) and
				c.section = '$section'
				group by t.chapter_id"
				
				);
			
			$tbs = $this->db->query(
			"SELECT t.chapter_id, Count(distinct q.id) as count
				FROM question_topics qt, chapters c, topics t, task_based_questions q
				where qt.topic_id = t.id and
				q.id = qt.question_id and
				t.chapter_id = c.id and
				qt.question_type = 'tbs' and
				(q.deprecated <> 1 or q.deprecated is null) and
				c.section = '$section'
				group by t.chapter_id"
			);
			
			$counts=array();
			$results=array();
			$tbs_counts=array();
			$mcq_count=$mcq->result_array();
			$tbs_count=$tbs->result_array();
			
			//if no mcq's just return tbs
			if (empty($mcq_count)) {
				
				foreach ($tbs_count as $tbs_c) {
							
					$counts[$tbs_count['chapter_id']] = $tbs_c['count'];
				}
				$results['mcq']=array();
				$results['tbs']=$counts;
				$results['combined']=$counts;
				return $results;
			}
			
			//format into less complex array
			foreach ($mcq_count as $mcq_c) {
							
					$counts[$mcq_c['chapter_id']] = $mcq_c['count'];
			}
			$results['mcq']=$counts;
			foreach ($tbs_count as $tbs_c) {
							
					$tbs_counts[$tbs_c['chapter_id']] = $tbs_c['count'];
			}
			$results['tbs']=$tbs_counts;	
			//add together mcq and tbs
			
			foreach ($counts as $key_count=>$val_count) {				
				
				if (array_key_exists($key_count,$tbs_counts)) {
					
					$counts[$key_count]=$counts[$key_count]+$tbs_counts[$key_count];
				}
				
			}
			$results['combined']=$counts;
			return $results;
			
			
			
	}
	
	public function get_quiz_data_by_chapter($chapter_list,$student_id,$session_type='quiz')
	{
		$query = $this->db->query("SELECT *, q.id as question_id FROM question_scores q, saved_quizzes s
									where q.session_id = s.id and 
									q.student_id = $student_id and q.chapter_id IN ($chapter_list)
									order by s.id desc");
									
		return $query->result_array();
	}
	
	public function get_section_overview($student_id)
	{
		$query = $this->db->get_where('section_overview', array('student_id'=>$student_id));
									
		return $query->result_array();
	}
	
	public function get_attempted_questions($student_id)
	{
		$query = $this->db->query("SELECT q.type, q.chapter_id, q.id as question_id FROM question_scores q, saved_quizzes s
									where q.student_id = $student_id and s.session_type = 'quiz' and q.status <> 'skipped'
									group by q.type, q.chapter_id, question_id");
									
		return $query->result_array();
	}
	
	public function get_num_mcq_by_topic($topic_ids)
	{
		$query = $this->db->query("SELECT Count(distinct q.id) as mcq FROM multiple_choice_questions q, question_topics t where q.id = t.question_id and t.question_type = 'mcq' and (q.deprecated <> 1 or q.deprecated is null) and t.topic_id in ($topic_ids)");
									
		return $query->result_array();

	}
	
	/*
	 * get mcqs by topicID list
	*/
	
	public function get_num_tbs_by_topic($topic_ids)
	{
		$query = $this->db->query("SELECT Count(distinct q.id) as tbs 
									FROM task_based_questions q, question_topics t 
									where q.id = t.question_id and t.question_type = 'tbs' and (q.deprecated <> 1 or q.deprecated is null) and t.topic_id in ($topic_ids)");
									
		return $query->result_array();

	}
	
	public function get_mc_by_topicids($topicids,$type='mcq')
	{
		if ($type != 'tbs') {
			
			
			$query = $this->db->query("SELECT q.*
										FROM multiple_choice_questions q, question_topics t
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id in ($topicids)
										group by q.id, q.difficulty, q.topic_id, q.question");
			
		}
		
		return $query->result_array();
	}

	
	public function get_tbs_by_topicids($topicids,$type='tbs')
	{
		if ($type != 'mcq') {
//			$query = $this->db->query("SELECT * from task_based_questions limit 1");			

			$query = $this->db->query("SELECT q.*
										FROM task_based_questions q, question_topics t
										where q.id = t.question_id and t.topic_id in ($topicids)
										group by q.id, q.topic_id, q.question_json");

			
		}
		return $query->result_array();
	}

	public function get_saved_quizzes($student_id, $session_id, $section){
		$limit=5;
		//build the query based on whether or not we have pagination
		$session_clause = ($session_id == 0) ? '' : "and id < $session_id" ;
		//filter by section
		$section_clause = ($section == 'ALL') ? '' : "and section = '$section'" ;
		
		//put together the final query			
		$query = $this->db->query("SELECT * FROM saved_quizzes WHERE student_id=$student_id $section_clause $session_clause ORDER BY id DESC LIMIT $limit")->result_array();
		return $query;
	}

	public function get_saved_quizzes_by_page($student_id, $page = 1, $section = 'ALL')
	{
		$limit = 5;
		$offset = $page > 0 ? ($page - 1) * $limit : 0;
		$section_clause = $section == 'ALL' ? '' : "AND section = '$section'";
		$query = $this->db->query("SELECT * FROM saved_quizzes WHERE student_id=$student_id $section_clause ORDER BY timestamp DESC LIMIT $limit OFFSET $offset")->result_array();
		return $query;
	}
	
	public function get_saved_quiz($student_id, $session_id){
		
		$query = $this->db->query("SELECT * FROM saved_quizzes WHERE student_id=$student_id and id=$session_id")->result_array();
		return $query;
	}
	
	//save the quiz - insert
	public function save_quiz($data){
		$this->db->insert('saved_quizzes', $data);
		//return the new saved_quizzes id
		$result['id']=$this->db->insert_id();
		return $result;
	}
	
	public function update_quiz($data){
		$this->db->where('id', $data['id']);
		$this->db->update('saved_quizzes', $data);
		$result=array('success'=>1);
		return $result;
		
	}

	//blow em out
	public function delete_quiz($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('saved_quizzes');
		
		return $result = array('success' => TRUE );
		
	}
	
	public function get_question_scores($student_id, $session_id=0)
	{
		if ($session_id != 0){
			$query = $this->db->get_where('question_scores', array('session_id'=>$session_id))->result_array();
		} else {
			$query = $this->db->get_where('question_scores', array('student_id' => $student_id))->result_array();
		}

		foreach($query as $key => $question) {
			$qid = $question['id'];
			if ($question['type'] == 'mcq') {
				$q_info = $this->db->query("SELECT section as section, chapter_id as chapter_id FROM multiple_choice_questions WHERE id=$qid")->result_array()[0];
				$type = 'multiple-choice';
			}
			else {
				$q_info = $this->db->query("SELECT section as section, type as type, chapter_id as chapter_id FROM task_based_questions WHERE id=$qid")->result_array()[0];
				$type = $q_info['type'];
			}
			$query[$key]['section'] = $q_info['section'];
			$query[$key]['chapter'] = $q_info['chapter_id'];
			$query[$key]['type_long'] = $type;
			
		}
		
		return $query;
	}
	
	//save the question_scores - insert. 
	public function save_question_scores($data){
		$values_string='';
		
		// format the insert
		foreach ($data as $row) {
				
			//get the chapter id
			$qid=$row['id'];
			$question_type = ($row['type']=='mcq') ? 'mcq' : 'tbs' ;
			$query = $this->db->query("SELECT t.chapter_id 
										FROM question_topics qt, chapters c, topics t
										where qt.topic_id = t.id and
										t.chapter_id = c.id and
										qt.question_type = '$question_type' and
										qt.question_id = $qid")->result_array()[0];
			$row['chapter_id']=$query['chapter_id'];
            
            //record session_overview if needed
            if ($row['status']=='incorrect' || $row['status']=='correct') {
                
                    $so_data=array(
                        'student_id'=>$row['student_id'], 
                        'question_id'=>$row['id'], 
                        'chapter_id'=>$row['chapter_id'], 
                        'session_id'=>$row['session_id'], 
                        'status'=>$row['status'], 
                        'type'=>$row['type'],
                    
                    );
                    $this->db->where('question_id',$row['id']);
                    $this->db->where('student_id',$row['student_id']);
                    $this->db->where('type',$row['type']);
                    $q = $this->db->get('section_overview');               
                    
                    
                       if ( $q->num_rows() > 0 ) 
                       {
                          
                          $so=$q->result_array();
                          $this->db->where('id',$so[0]['id']);
                          $this->db->update('section_overview',$so_data);
                       } else {
                            //just insert it  
                            $this->db->insert('section_overview',$so_data);                     
                       }
                       
                       
            }

			$row['noted'] = (!isset($row['noted'])) ? '[]' : $row['noted'];
			$row['flagged'] = (!isset($row['flagged'])) ? 'false' : $row['flagged'];


			if(!array_key_exists('flagged', $row)) {
				$row['flagged'] = '0';
			}

			if(!array_key_exists('noted', $row)) {
				$row['noted'] = '';
			}
            

			
			$values_string=$values_string ."(".$row['id'].",".$row['session_id'].",".$row['student_id'].",".$row['quizlet_num'].",'".$row['type']."','".$row['status']."','".$row['noted']."','".$row['flagged']."','".$row['answer_id']."','".$row['score']."','".$row['chapter_id']."'),";
		}
		$values_string=rtrim($values_string, ",");
		//var_dump($values_string);
		//exit;
		//$this->db->insert('question_scores', $data);
		$query = $this->db->query("INSERT INTO question_scores (id,session_id,student_id,quizlet_num,type,status,noted,flagged,answer_id,score,chapter_id) VALUES $values_string");
		//return the new saved_quizzes id
		return $result=array('success'=>1);
	}
	//update question scores
	public function update_question_scores($data){
		$question_id = $data['id'];
		if (isset($data['status'])) {
			date_default_timezone_set('UTC');
			$incoming_timestamp = $data['timestamp'];
			$incoming_timestamp = strtotime($incoming_timestamp);
			$db_timestamp = $this->db->query("SELECT timestamp FROM section_overview where question_id = '$question_id'")->result_array();
			if (count($db_timestamp) > 0) {
				$db_timestamp = $db_timestamp[0]['timestamp'];
				$db_timestamp = strtotime($db_timestamp);
				if ($db_timestamp <= $incoming_timestamp) {
					unset($data['timestamp']);
					$this->db->where('id', $data['id']);
					$this->db->where('session_id', $data['session_id']);
					$this->db->where('student_id', $data['student_id']);
					$this->db->update('question_scores', $data);
					$result=array('success'=>1);
				}
				else {
					$result=array('success'=>array('db_timestamp'=> date("M j Y g:i:s A", $db_timestamp), 'incoming_timestamp'=> date("M j Y g:i:s A", $incoming_timestamp)));
				}	
			}
			else {
				unset($data['timestamp']);
				$this->db->where('id', $data['id']);
				$this->db->where('session_id', $data['session_id']);
				$this->db->where('student_id', $data['student_id']);
				$this->db->update('question_scores', $data);		
				$result=array('success'=>1);
			}
		}

		else {
			unset($data['timestamp']);
			$this->db->where('id', $data['id']);
			$this->db->where('session_id', $data['session_id']);
			$this->db->where('student_id', $data['student_id']);
			$this->db->update('question_scores', $data);		
			$result=array('success'=>1);
		}
		
		return $result;
	}
	
	public function update_section_overview($results){
	    $data = array();
         //skip noted updateds
        if (array_key_exists('noted',$results)){
            //no skipped questions
           
                $result=array('success'=>1);
                return $result;
                
            
            
        }
		
		if (array_key_exists('status',$results)){
		    //no skipped questions
            if ($results['status'] == 'skipped' || $results['status'] == '') {
                //remove from section overview
                $result=array('success'=>1);
                return $result;
                
            } else {
            	$data['status']=$results['status'];
            }
			
		}
		
		//get the chapter_id
		$this->db->where('session_id',$results['session_id']);
		$this->db->where('id',$results['id']);
		$this->db->where('student_id',$results['student_id']);
		$q = $this->db->get('question_scores')->result_array()[0];
		$results['chapter_id']=$q['chapter_id'];
		
		$data=array_merge($data, array(
			'student_id'=>$results['student_id'],
			'question_id'=>$results['id'],
			'chapter_id'=>$results['chapter_id'],
			'session_id'=>$results['session_id'],
			'type'=>$results['type'],
		));
		
		$this->db->where('question_id',$data['question_id']);
		$this->db->where('student_id',$data['student_id']);
		$this->db->where('type',$data['type']);
		$q = $this->db->get('section_overview');
		
		   if ( $q->num_rows() > 0 ) 
		   {
		      $so=$q->result_array();
		      $this->db->where('id',$so[0]['id']);
		      $this->db->update('section_overview',$data);
		   } else {
		   		//need the chapter id
		   
		   	$this->db->insert('section_overview',$data);
		   }	
		$result=array('success'=>1);
		return $result;
		
	}
	
	//get all the question_scores
	public function get_question_scores_session($session_id,$student_id){
		//use in clause for lists
		if (strpos($session_id, ',')) {
			
			$query = $this->db->query("SELECT * FROM question_scores where session_id IN ($session_id) and student_id = $student_id order by type ASC, id DESC");
			
		} else {
			 $query = $this->db->get_where('question_scores', array('session_id' => $session_id, 'student_id' => $student_id));
		}
		
        return $query->result_array();
    }
	
	public function get_type_summary($session_ids){
		$query = $this->db->query("SELECT * 
									FROM question_scores 
									where session_id in ($session_ids)");
                                        
        return $query->result_array();
	}
    
    
    public function get_multiple_chapters($chapter_ids){
        $query = $this->db->query("SELECT * 
                                    FROM chapters 
                                    where id in ($chapter_ids)");
                                        
        return $query->result_array();
    }
	
	
	
	/**
     * Get all the saved quiz data for monitoring software
     * @param $drupal_user_id is just what it says
     */
     
     
    public function get_question_scores_saved_quizzes($drupal_user_id){
        $query = $this->db->query("SELECT st.id as studentid, s.*, q.*
                                        FROM students st, saved_quizzes s, question_scores q
                                        where (st.id = s.student_id) and
                                              (s.student_id = q.student_id) and 
                                              (s.id = q.session_id) and
                                              st.drupal_user_id = $drupal_user_id
                                        order by s.id");
                                        
        return $query->result_array();
    }
	
	public function get_saved_quiz_questions($session_ids,$student_id)
	{
		$results=array();			
		//get mcqs
		$query =$this->db->query("SELECT *, q.answer_id as correct_answer_id, CONCAT(qs.id, '-', qs.quizlet_num) as qs_key, qs.id as qs_id, qs.answer_id as qs_answer_id, a.id as a_id FROM question_scores qs, multiple_choice_questions q, multiple_choice_answers a 
									where qs.id = q.id and
									q.id = a.question_id and
									qs.type = 'mcq' and
									qs.student_id = $student_id and
									qs.session_id in ($session_ids)
									order by qs.session_id");
		
		$mcqs = $query->result_array();
		
		//get tbs
		$query =$this->db->query("SELECT *, qs.id as qs_id FROM question_scores qs, task_based_questions q
									where qs.id = q.id and
									qs.type = 'tbs' and
									qs.student_id = $student_id and
									qs.session_id in ($session_ids)
									order by qs.session_id");
		$tbs = $query->result_array();
		
		$results['mcq']=$mcqs;
		$results['tbs']=$tbs;
		return $results;
	}
	
	/**
	 * gets saved quiz data for monitoring software
	 */
	public function get_saved_quizzes_monitor($data){
		//put together the final query	
		$sections=$data['sections'];
		$uids=$data['uids'];
		$start_date=$data['start_date'];
		$end_date=$data['end_date'];		
		$query = $this->db->query("SELECT * FROM saved_quizzes
									where section in ($sections) and
									student_id in ($uids) and
									timestamp >= '$start_date' and
									timestamp <= '$end_date'")->result_array();
		return $query;
	}
	
	 
	
	/*
	 * for the docs manager in manage_docs controler
	 * see if doc file exists
	 */
	 
	public function get_doc($filename, $type){
		$query = $this->db->get_where('documents', array('title' => $filename, 'type'=>$type));
		return $query->result_array();
	}
	
	//get docs by id
	public function get_document($id){
		$query = $this->db->get_where('documents', array('id' => $id));
		return $query->result_array();
	}
	
	public function get_docs($type){
		$query = $this->db->get_where('documents', array('type' => $type));
		return $query->result_array();
	}
	
	public function get_flagged_docs(){
		$this->db->where('isFlagged', 1);
		$query = $this->db->get('documents');
		return $query->result_array();
	}

	public function set_doc($data){
		$this->db->insert('documents', $data);
		//return the new question id
		$result['id']=$this->db->insert_id();
		return $result;
	}
	
	public function checkout_doc($data, $id){
		$this->db->where('id', $id);
		$this->db->update('documents', $data);
		$result=array('success'=>1);
		return $result;
	}
	
	public function flag_document($data, $id){
		$this->db->where('id', $id);
		$this->db->update('documents', $data);
		$result=array('success'=>1);
		return $result;
	}
}
