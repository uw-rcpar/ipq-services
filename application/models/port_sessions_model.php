<?php
class Port_sessions_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_quizzes($start_id,$limit) {
        $quizzes = $this->db->query("SELECT * FROM quizlet_sessions  where id > $start_id  and active = 1 order by id asc limit $limit");
        return $quizzes->result_array();
    }
    public function get_quiz_count() {
        $quizzes = $this->db->query("SELECT count(*) as total FROM quizlet_sessions where active = 1");
        return $quizzes->result_array();
    }
    /**
     * Insert the saved_quiz data
     */
     public function set_saved_quizzes($data){
        $this->db->insert('saved_quizzes', $data);
        //return the new question id
        $result=$this->db->insert_id();
        return $result;
        
     }
	 public function set_question_scores($data){
	 	$this->db->trans_start();
        $this->db->insert_batch('question_scores', $data);
        $this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
		    // generate an error... or use the log_message() function to log your error
		    return $result=array('success'=>0);
		
		}
        return $result=array('success'=>1);
        
     }
     //get section overview
     public function get_section_overview($data){
        $query = $this->db->get_where('section_overview', array('student_id' => $data['student_id'], 'type'=>$data['type'], 'question_id'=>$data['id']));
        return $query->result_array();
    }
     //insert
     public function set_section_overview($data){
        $this->db->insert('section_overview', $data);
        //return the new question id
        $result=$this->db->insert_id();
        return $result;
        
     }
     //insert
     public function update_section_overview($data){
        $this->db->where(array('student_id' => $data['student_id'], 'type'=>$data['type'], 'question_id'=>$data['question_id']));
        $this->db->update('section_overview', $data);
        $result=array('success'=>1);
        return $result;
        
     }
}