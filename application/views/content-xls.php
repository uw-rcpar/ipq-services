<?php

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="IPQ ' . $section . '-' . $chapter . '.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', "Question ID");
$objPHPExcel->getActiveSheet()->setCellValue('B1', "Question URL");
$objPHPExcel->getActiveSheet()->setCellValue('C1', "Type");
$objPHPExcel->getActiveSheet()->setCellValue('C1', "Topics");
$objPHPExcel->getActiveSheet()->setCellValue('E1', "Created");
$objPHPExcel->getActiveSheet()->setCellValue('F1', "Edited");

$i = 2;
foreach($data as $row) {
$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row['name']);
$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row['editUrl']);

$i++;
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;