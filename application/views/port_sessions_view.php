<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Porting Sessions</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}


</style>
<meta http-equiv="refresh" content="0; url=/restserver/index.php/port_sessions?start_id=<?php echo $start_id;?>&processed=<?php echo $processed;?>" />
</head>
<body>

<h1>Porting Sessions</h1>

<p>Total records to port: <?php echo $total_records;?></p>
<p>Next start_id: <?php echo $start_id;?></p>
<p>Percentage Complete: <?php echo $percentage_complete;?></p>
<p>Records processed: <?php echo $processed;?></p>


</body>
</html>