<?php
	$conn = new mysqli('localhost', 'root','NClUYZQ2s0zT', 'testcenter');
	ini_set('memory_limit', '2000M'); 
	$students = $conn->query('SELECT id FROM students')->fetch_all();
	$sessions = array();
	$starting_sid = 5189;
	$stopping_sid = 5189;
		
	foreach($students as $student) {
			$correctQuestions = array();
			$incorrectQuestions = array();
			$ccount = array();
			$sid = intval($student[0]);
			if ($sid >= $starting_sid && $sid <= $stopping_sid) {
				$sessions[$sid] = $conn->query('select summary, id from quizlet_sessions where student_id=' . $sid)->fetch_all();
//var_dump(count($sessions[$sid]));
				$json = array();
				if (count($sessions[$sid]) > 0) {
					foreach($sessions[$sid] as $session) {
						$summary = $session[0];
						$session_id = $session[1];
//var_dump($summary);
						$summary = json_decode($summary, true);
						$breakdowns = $summary['session']['summary']['breakdown'];

						$average_time = "";
						if (isset($summary['session']['summary']['timeAverage'])) {
							$average_time = $summary['session']['summary']['timeAverage'];
						}
						else { var_dump('Missing time average'); }
						if (isset($summary['session']['quizlets'])) { 
							$quizlets = $summary['session']['quizlets'];
							foreach($quizlets as $quizlet_num => $quizlet) {
								foreach($quizlet as $question) {
									if ($question['type'] == 0) {
										$result = $conn->query('select chapter_id as cid from multiple_choice_questions where id=' . $question['id'])->fetch_assoc();
										$type = 'mcq';
									}
									else {
										$result = $conn->query('select chapter_id as cid from task_based_questions where id=' . $question['id'])->fetch_assoc();
										$type = 'tbs';
									}
									if (!is_null($result['cid'])) {
										if (isset($question['type'])) {
											$qid = $question['id'];
											$chapter_id = $result['cid'];
											if (!isset($correctQuestions[$chapter_id])) {
												$correctQuestions[$chapter_id]['mcq'] = array();
												$incorrectQuestions[$chapter_id]['mcq'] = array();
												$notedQuestions[$chapter_id]['mcq'] = array();
												$flaggedQuestions[$chapter_id]['mcq'] = array();

												$correctQuestions[$chapter_id]['tbs'] = array();
												$incorrectQuestions[$chapter_id]['tbs'] = array();
												$notedQuestions[$chapter_id]['tbs'] = array();
												$flaggedQuestions[$chapter_id]['tbs'] = array();
											}
											$flagged = 'false';
											if (isset($question['isFlagged'])) {
												if ($question['isFlagged'] == true) {
													array_push($flaggedQuestions[$chapter_id][$type], $question['id']);
													$flagged = strval($question['isFlagged']);
												}
											}
											$noted = null;
											if (isset($question['notes'])) {
												if (count($question['notes']) > 0) {
													array_push($notedQuestions[$chapter_id][$type], $question['id']);
													$noted = json_encode($question['notes']);
												}
											}
										$answer_type = $question['type'];
										if ($answer_type == 0) {
											$answer_type = 'multiple-choice';
										}
										else if ($answer_type == 1) {
											$answer_type = 'tbs-journal';
										}
										else if ($answer_type == 2) {
											$anser_type = 'tbs-wc';
										}
										else if ($answer_type == 3) {
											$answer_type = 'tbs-research';
										}
										// put tbs types here

										$stmt = $conn->prepare("INSERT INTO question_scores(id, session_id, student_id, quizlet_num, type, noted, flagged, chapter_id) values(?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update noted=?, flagged=?, chapter_id=?");
										$result = $stmt->bind_param('iiiisssissi', 
											$qid,
											$session_id,
											$sid,
											$quizlet_num,
											$type,
											$noted,
											$flagged,
											$chapter_id,
											$noted,
											$flagged,
											$chapter_id
										);
										$result = $stmt->execute();


	//var_dump($flaggedQuestions[$chapter_id][$type]);
	//var_dump($notedQuestions[$chapter_id][$type]);
										}
									}
								}
							}
						}
						else {
							var_dump("Notes and bookmark data not available");
						}
					} // end session loop
					$unansweredstr = 'skipped';
					$emptystatus = '';
					$stmt = $conn->prepare("UPDATE question_scores set status=? where status=?");
					$result = $stmt->bind_param('ss', 
						$unansweredstr,
						$emptystatus
					);
					$result = $stmt->execute();
				} // end if
var_dump($sid);
		}
	}
	$conn->close();


?> 
