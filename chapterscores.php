<?php
	$conn = new mysqli('localhost', 'root','NClUYZQ2s0zT', 'testcenter');
	ini_set('memory_limit', '128M'); 
	$students = $conn->query('SELECT id FROM students')->fetch_all();
	$sessions = array();
	$starting_sid = 2262;
	$stopping_sid = 2262;
	$session_id = 0;
		
	foreach($students as $student) {
			$correctQuestions = array();
			$incorrectQuestions = array();
			$ccount = array();
			$sid = intval($student[0]);
			if ($sid >= $starting_sid && $sid <= $stopping_sid) {
							$all_chapters = $conn->query('select id as cid from chapters')->fetch_all();
//var_dump($all_chapters);die();
							$types = ['tbs','mcq'];

//var_dump($all_chapters);die();
							foreach($all_chapters as $chapter) {
								foreach($types as $type) { 
								$chapter = $chapter[0];
								$chapter_id = $chapter;

								$chapter_questions = array();
								if ($type == 'mcq') {
									$qs = $conn->query("select id from multiple_choice_questions where chapter_id=$chapter_id")->fetch_all();
								}
								else {
									$qs = $conn->query("select id from task_based_questions where chapter_id=$chapter_id")->fetch_all();
								}
								foreach($qs as $q) {
									array_push($chapter_questions, $q[0]);
								}
								$chapter_questions = implode(',', $chapter_questions);

								$mcq = $conn->query('select count(*) as count from multiple_choice_questions where chapter_id=' . $chapter_id)->fetch_assoc();
								$tbs = $conn->query('select count(*) as count from task_based_questions where chapter_id=' . $chapter_id)->fetch_assoc();
								$total_questions[$chapter_id] = intval($mcq['count']) + intval($tbs['count']);
									
//var_dump($chapter_questions);die();
								$correctQuestions[$chapter_id][$type] = array();
								$incorrectQuestions[$chapter_id][$type] = array();

								if (strlen($chapter_questions) > 0) {
//									$correct = $conn->query('select id as id, session_id as session_id from question_scores where status="correct" and student_id=' . $sid . ' and id in (' . $chapter_questions. ')')->fetch_all();
									$answers = $conn->query('select id as id, session_id as session_id, status as status from question_scores where student_id=' . $sid . ' and id in (' . $chapter_questions. ') order by session_id asc')->fetch_all();
								}
//var_dump($correct->fetch_all());die();
								$most_recent_session = array();
								foreach($answers as $a) {
									$key = $a[0];
									$most_recent_session[$key] = array('session_id' => $a[1], 'status' => $a[2]);
								}
								
								foreach($most_recent_session as $key => $q_info) {
									if ($q_info['status'] == 'correct') {
										if (!in_array($key, $correctQuestions[$chapter_id][$type])) {
											array_push($correctQuestions[$chapter_id][$type], $key);
										}
									}
									if ($q_info['status'] == 'incorrect') {
										if (!in_array($key, $incorrectQuestions[$chapter_id][$type])) {
											array_push($incorrectQuestions[$chapter_id][$type], $key);
										}
									}
								}

										if (count($correctQuestions[$chapter_id][$type]) > 0 || count($incorrectQuestions[$chapter_id][$type]) > 0) {
											if ($type == 'tbs') {
												$query = 'select group_concat(id order by id) as id from task_based_questions where chapter_id='.$chapter_id;
											}
											else {
												$query = 'select group_concat(id order by id) as id from multiple_choice_questions where chapter_id='.$chapter_id;
											}
											if (count($correctQuestions[$chapter_id][$type]) && count($incorrectQuestions[$chapter_id][$type]) == 0) {
	// correct q's
												
												$query = $query . ' and id not in (' . implode(',', $correctQuestions[$chapter_id][$type]);
											}
	// both incorrect and correct q's
											if (count($correctQuestions[$chapter_id][$type]) > 0 && count($incorrectQuestions[$chapter_id][$type]) > 0) {
												$query = $query . ' and id not in (' . implode(',', $correctQuestions[$chapter_id][$type]) . ',' . implode(',', $incorrectQuestions[$chapter_id][$type]);
											}
											if (count($incorrectQuestions[$chapter_id][$type]) > 0 && count($correctQuestions[$chapter_id][$type]) == 0) {
	// incorrect questions
												$query = $query .  ' and id not in (' . implode(',', $incorrectQuestions[$chapter_id][$type]);
											}
											$query = $query . ')';
											$unanswered[$chapter_id][$type] = explode(',',$conn->query($query)->fetch_all(MYSQLI_ASSOC)[0]['id']); 
	//var_dump($query);
//	var_dump($chapter);
//var_dump($unanswered[$chapter_id]);die();
										}
										else {
											if ($type == 'tbs') {
												$unanswered[$chapter_id][$type] = explode(',',$conn->query('select group_concat(id order by id) as id from task_based_questions where chapter_id='.$chapter_id)->fetch_all(MYSQLI_ASSOC)[0]['id']);
											}
											else {
												$unanswered[$chapter_id][$type] = explode(',',$conn->query('select group_concat(id order by id) as id from multiple_choice_questions where chapter_id='.$chapter_id)->fetch_all(MYSQLI_ASSOC)[0]['id']);
											}
//	var_dump($chapter);
										}

								}
		
								if (!isset($correctQuestions[$chapter]['tbs'])) {
									$correctQuestions[$chapter]['tbs'] = array();
								}
								if (!isset($correctQuestions[$chapter]['mcq'])) {
									$correctQuestions[$chapter]['mcq'] = array();
								}
								$ccount[$chapter] = count($correctQuestions[$chapter]['tbs']) + count($correctQuestions[$chapter]['mcq']);

								if ($ccount[$chapter] == 0 || $total_questions[$chapter_id] == 0) {
									$score = 0;
								}
								else {
									$score = round(($ccount[$chapter]*100)/$total_questions[$chapter_id], 0);
								}
								$chapter_scores[$chapter] = array(
									'score'=>$score,
									'tbs'=> array(
										'incorrect' => isset($incorrectQuestions[$chapter]['tbs']) ? $incorrectQuestions[$chapter]['tbs'] : [],
										'correct' => isset($correctQuestions[$chapter]['tbs']) ? $correctQuestions[$chapter]['tbs'] : [],
										'noted' => isset($notedQuestions[$chapter]['tbs']) ? $correctQuestions[$chapter]['tbs'] : [],
										'flagged' => isset($flaggedQuestions[$chapter]['tbs']) ? $flaggedQuestions[$chapter]['tbs']  : [],
										'skipped' => isset($unanswered[$chapter_id]['tbs']) ? $unanswered[$chapter_id]['tbs'] : []
									),
									'mcq'=> array(
										'incorrect' => isset($incorrectQuestions[$chapter]['mcq']) ? $incorrectQuestions[$chapter]['mcq']  : [],
										'correct' => isset($correctQuestions[$chapter]['mcq']) ? $correctQuestions[$chapter]['mcq'] : [],
										'noted' => isset($notedQuestions[$chapter]['mcq']) ? $notedQuestions[$chapter]['mcq']  : [],
										'flagged' => isset($flaggedQuestions[$chapter]['mcq']) ? $flaggedQuestions[$chapter]['mcq']  : [],
										'skipped' => isset($unanswered[$chapter_id]['mcq']) ? $unanswered[$chapter_id]['mcq']  : []
									)
								);
								$type = 'tbs';
								$stmt = $conn->prepare('insert into chapter_scores(chapter_id, student_id, type, score, incorrect, correct, skipped, noted, flagged) values (?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update score=?, incorrect=?, correct=?, skipped=?, noted=?, flagged=?');
								$result = $stmt->bind_param('iisssssssssssss', 
									$chapter_id, 
									$sid, 
									$type, 
									$chapter_scores[$chapter_id]['score'],
									json_encode($chapter_scores[$chapter_id]['tbs']['incorrect']),
									json_encode($chapter_scores[$chapter_id]['tbs']['correct']),
									json_encode($chapter_scores[$chapter_id]['tbs']['skipped']),
									json_encode($chapter_scores[$chapter_id]['tbs']['noted']),
									json_encode($chapter_scores[$chapter_id]['tbs']['flagged']),
									$chapter_scores[$chapter_id]['score'],
									json_encode($chapter_scores[$chapter_id]['tbs']['incorrect']),
									json_encode($chapter_scores[$chapter_id]['tbs']['correct']),
									json_encode($chapter_scores[$chapter_id]['tbs']['skipped']),
									json_encode($chapter_scores[$chapter_id]['tbs']['noted']),
									json_encode($chapter_scores[$chapter_id]['tbs']['flagged'])
								);
								$result = $stmt->execute();

								$type = 'mcq';
								$stmt = $conn->prepare('insert into chapter_scores(chapter_id, student_id, type, score, incorrect, correct, skipped, noted, flagged) values (?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update score=?, incorrect=?, correct=?, skipped=?, noted=?, flagged=?');
								$result = $stmt->bind_param('iisssssssssssss', 
									$chapter_id, 
									$sid, 
									$type, 
									$chapter_scores[$chapter_id]['score'],
									json_encode($chapter_scores[$chapter_id]['mcq']['incorrect']),
									json_encode($chapter_scores[$chapter_id]['mcq']['correct']),
									json_encode($chapter_scores[$chapter_id]['mcq']['skipped']),
									json_encode($chapter_scores[$chapter_id]['mcq']['noted']),
									json_encode($chapter_scores[$chapter_id]['mcq']['flagged']),
									$chapter_scores[$chapter_id]['score'],
									json_encode($chapter_scores[$chapter_id]['mcq']['incorrect']),
									json_encode($chapter_scores[$chapter_id]['mcq']['correct']),
									json_encode($chapter_scores[$chapter_id]['mcq']['skipped']),
									json_encode($chapter_scores[$chapter_id]['mcq']['noted']),
									json_encode($chapter_scores[$chapter_id]['mcq']['flagged'])
								);
								$result = $stmt->execute();
							}
var_dump($sid);
		}
	}
	$conn->close();


?> 
